library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ledc8x8 is
port ( -- Sem doplnte popis rozhrani obvodu.
		SMCLK : in std_logic;
		RESET: in std_logic;
		LED: out std_logic_vector(7 downto 0);
		ROW: out std_logic_vector(7 downto 0)
		
);
end ledc8x8;

architecture main of ledc8x8 is
	signal leds : std_logic_vector(7 downto 0);
	signal rows : std_logic_vector(7 downto 0);
	signal en : std_logic;
	signal counter : std_logic_vector(7 downto 0); 
	--signal delay : integer range 0 to 20000000 := 0;
	signal delay : std_logic_vector(21 downto 0);
	--signal en2 : integer range 0 to 2 := 0; hlasil warning nevim proc
	signal en2: std_logic_vector(1 downto 0); -- alternativa
	
    -- Sem doplnte definice vnitrnich signalu.

begin
	en_gen: process(SMCLK,RESET)
	begin
			if RESET = '1' then
					counter <= "00000000";
			elsif SMCLK'event and SMCLK = '1' then
					counter <= counter + 1;
			end if;
	end process en_gen;
	en <= '1' when counter = X"FF" else '0'; -- generovani smclk/256 
	
	
	rotate_row: process(SMCLK,RESET,en,rows) -- rotace radku
	begin
			if RESET = '1' then
				rows <= "10000000";-- prvni radek
			elsif (SMCLK = '1' and SMCLK'event and en = '1') then
				rows <= rows(0) & rows(7 downto 1); --posuv
			end if;
			ROW <= rows;
	end process rotate_row;
	
	gen_delay: process(SMCLK,RESET) -- generovani delay 0,25 sec = smclk 7.3728 MHz / 4 = 1843200
	begin	
			if RESET = '1' then
				delay <= "0000000000000000000000";
				en2 <= "00";
			else
				if SMCLK ='1' and SMCLK'event then
				--	if (delay = 1843200) then -- nastavuji signal en2 pro rizeni prepinani varianta integer
					if(delay = "0111000010000000000000") then
						en2 <= en2 + 1;
						delay <= "0000000000000000000000";
					else
						delay <= delay +1;
					end if;
				end if;
			end if;
	end process gen_delay;
	
	led_lights: process(rows,en2) 
		begin
			if en2 = "10" then -- prijmeni
				case rows is 
					when "10000000" => leds <= "00111110";
					when "01000000" => leds <= "01011110";
					when "00100000" => leds <= "01101110";
					when "00010000" => leds <= "01110110";
					when "00001000" => leds <= "01111010";
					when "00000100" => leds <= "01111100";
					when "00000010" => leds <= "01111110";
					when "00000001" => leds <= "11111111";
					when others     => leds <= "11111111";
				end case;
			elsif en2 = "00" then -- jmeno
				case rows is
					when "10000000" => leds <= "11000111";
					when "01000000" => leds <= "10111011";
					when "00100000" => leds <= "01111101";
					when "00010000" => leds <= "01111101";
					when "00001000" => leds <= "01110101";
					when "00000100" => leds <= "10111011";
					when "00000010" => leds <= "11000101";
					when "00000001" => leds <= "11111111";
					when others     => leds <= "11111111";
				end case;
			else
				leds<= "11111111"; -- nic
			end if;
		end process led_lights;
			ROW <= rows;
			LED <= leds;
	
	
	
    -- Sem doplnte popis obvodu. Doporuceni: pouzivejte zakladni obvodove prvky
    -- (multiplexory, registry, dekodery,...), jejich funkce popisujte pomoci
    -- procesu VHDL a propojeni techto prvku, tj. komunikaci mezi procesy,
    -- realizujte pomoci vnitrnich signalu deklarovanych vyse.

    -- DODRZUJTE ZASADY PSANI SYNTETIZOVATELNEHO VHDL KODU OBVODOVYCH PRVKU,
    -- JEZ JSOU PROBIRANY ZEJMENA NA UVODNICH CVICENI INP A SHRNUTY NA WEBU:
    -- http://merlin.fit.vutbr.cz/FITkit/docs/navody/synth_templates.html.

    -- Nezapomente take doplnit mapovani signalu rozhrani na piny FPGA
    -- v souboru ledc8x8.ucf.

end main;
