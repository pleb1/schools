//author xnguye11
#include "MK60D10.h"
#include <stdio.h>
#include <string.h>

#define BTN_SW2 0x400     // Port E, bit 10
#define SPK 0x10          // Speaker is on PTA4
#define MORSE_CODES 26
#define MAX_LEN 5
#define INVALID_CODE 1

int pressed_up = 0, pressed_down = 0, cnt = 0;
int beep_flag = 0;
//unsigned int compare = 0x200;
char input[MAX_LEN] = {0};// our input sequence of . and -
int input_valid = 1;
int input_pos = 0; // position in the input array
void input_reset();
void input_add(char c);
void SendCh(char c);
unsigned char decode_morse();
void Delay(unsigned long long int bound);

void input_reset()// reset the input array for next input
{
	for(int i = 0; i < MAX_LEN; i++)
	{
		input[i] = '\0';// reset the input
	}
	input_valid = 1;
	input_pos = 0;
}
// adding . - to the input array
void input_add(char c)
{
	if(!input_valid || input_pos > MAX_LEN) {
		input_valid = 0; // input is too long or detected invalid input
		return;
	}

	input[input_pos++] = c;
}
// short press add .
void short_press()
{
	input_add('.');
}
// long press add -
void long_press()
{
	input_add('-');
}

//processing the input_array // sending the char if array is valid
void end_input()
{
	unsigned char decoded;
	if(input_valid)// only when input is valid we print the character
	{
		decoded = decode_morse();
		if(decoded != INVALID_CODE)
		{
			SendCh(decoded);// send the char
		}
	}
	input_reset();// reset the input
}

static const char *morse[MORSE_CODES] = { // english alphabet
	".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
	"....", "..",".---", "-.-", ".-..", "--",
	"-.", "---", ".--.", "--.-", ".-.", "...", "-",
	"..-", "...-", ".--", "-..-", "-.--", "--.."
};

/* Initialize the MCU - basic clock settings, turning the watchdog off */
void MCUInit(void)  {
    MCG_C4 |= ( MCG_C4_DMX32_MASK | MCG_C4_DRST_DRS(0x01) );
    SIM_CLKDIV1 |= SIM_CLKDIV1_OUTDIV1(0x00);
    WDOG_STCTRLH &= ~WDOG_STCTRLH_WDOGEN_MASK;
}
// decoding the morse code
unsigned char decode_morse()
{
	unsigned char i;
	for (i = 0; i < MORSE_CODES; ++i) {
		if (!strcmp(input, morse[i])) {
			return i + 65; // first ascii character A = 65
		}
	}

	return INVALID_CODE;
}



// Delay
void Delay(unsigned long long int bound) {
    for(unsigned long long int i=0; i<bound; i++);
}

// Beeper
void Beep() {
    for (unsigned int q=0; q<500; q++) {
        PTA->PDOR = GPIO_PDOR_PDO(SPK);
        Delay(500);
        PTA->PDOR = GPIO_PDOR_PDO(0x0000);
        Delay(500);
    }
}
// Receive character
unsigned char ReceiveCh() {
    while( !(UART5->S1 & UART_S1_RDRF_MASK) );
    return UART5->D;
}

// Send character
void SendCh(char c) {
    while( !(UART5->S1 & UART_S1_TDRE_MASK) && !(UART5->S1 & UART_S1_TC_MASK) );
    UART5->D = c;
}

// UART INIT
void UART5Init() {
    UART5->C2  &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
    UART5->BDH =  0x00;
    UART5->BDL =  0x1A; // Baud rate 115 200 Bd, 1 stop bit
    UART5->C4  =  0x0F; // Oversampling ratio 16, match address mode disabled
    UART5->C1  =  0x00; // 8 data bitu, bez parity
    UART5->C3  =  0x00;
    UART5->MA1 =  0x00; // no match address (mode disabled in C4)
    UART5->MA2 =  0x00; // no match address (mode disabled in C4)
    UART5->S2  |= 0xC0;
    UART5->C2  |= ( UART_C2_TE_MASK | UART_C2_RE_MASK ); // Zapnout vysilac i prijimac
}

// Port INIT
void PortsInit() {

    // Enable CLOCKs for: UART5, RTC, PORT-A, PORT-B, PORT-E
    SIM->SCGC1 = SIM_SCGC1_UART5_MASK;
    SIM->SCGC5 = SIM_SCGC5_PORTA_MASK |SIM_SCGC5_PORTE_MASK | SIM_SCGC5_PORTB_MASK;

    // PORT A
    PORTA->PCR[4] = PORT_PCR_MUX(0x01);//speaker

    // PORT B
    PORTB->PCR[5] = PORT_PCR_MUX(0x01); // D9  LED

    // PORT E
    PORTE->PCR[8]  = PORT_PCR_MUX(0x03); // UART5_TX
    PORTE->PCR[9]  = PORT_PCR_MUX(0x03); // UART5_RX
    //PORTE->PCR[8] = ( 0 | PORT_PCR_MUX(0x02) ); // UART5_TX
    //PORTE->PCR[9] = ( 0 | PORT_PCR_MUX(0x02) ); // UART5_RX
    PORTE->PCR[10] = PORT_PCR_MUX(0x01); // button0 SW2

    // set ports as output
    //PTA->PDDR =  GPIO_PDDR_PDD(0x0010);
    PTA->PDDR = GPIO_PDDR_PDD(SPK);     // Speaker as output
    //PTB->PDDR =  GPIO_PDDR_PDD(0x3C);
    //PTB->PDOR |= GPIO_PDOR_PDO(0x3C); // turn all LEDs OFF
    PTA->PDOR &= GPIO_PDOR_PDO(~SPK);   // Speaker off, beep_flag is false
}

void init_hardware(void) {
	uint8_t i;

	MCUInit();		/* Zakladni inicializace vlastniho MCU - hodiny, watchdog */
	PortsInit();		/* Inicializace vstupu a vystupu potrebnych pro tuto ulohu - piny RX a TX UART5 */
	UART5Init();	/* Inicializace modulu UART5 - nastaveni prenosove rychlosti, parametru prenosu */

	// konfigurace pinu s tlacitky, porty jsou po resetu implicitne jako vstupy

	PORTE->PCR[10] = ( PORT_PCR_ISF(0x01) /* Nuluj ISF (Interrupt Status Flag) */
				| PORT_PCR_IRQC(0x0A) /* Interrupt enable on failing edge */
				| PORT_PCR_MUX(0x01) /* Pin Mux Control to GPIO */
				| PORT_PCR_PE(0x01) /* Pull resistor enable... */
				| PORT_PCR_PS(0x01)); /* ...select Pull-Up */
	//PORTB->PCR[13] |= PORT_PCR_MUX(0x01); /* Pin Mux Control - GPIO (BZZZZZZ) */
	//GPIOB_PDDR |= PIEZZO_MASK | LED_R_MASK | LED_G_MASK | LED_B_MASK;

	NVIC_ClearPendingIRQ(PORTE_IRQn ); /* Nuluj priznak preruseni od portu B */
	NVIC_EnableIRQ(PORTE_IRQn );       /* Povol preruseni od portu B */
}
// obsluhy klavesy
void PORTE_IRQHandler(void) {
	Delay(5000);
    if(PORTE->ISFR & BTN_SW2)
    {
    	while(!(GPIOE_PDIR & BTN_SW2))//pomocna promenna pro rozhodovani mezi kratky a dlouhy stisk
    	{
    		cnt++;
    	}
    	if(cnt < 1000000)// je-li cnt < 1000000 kratky stisk, 1000000 na zaklade debugu bylo zvoleno cca < 1sec
    	{
    		Beep();
    		short_press();
    		Delay(500);
    	}
    	else if(cnt > 1000000 && cnt < 2000000)
    	{
    		Beep();
    		long_press();
    		Delay(1000);
    	}
    	else
    	{
    		end_input();
    		Beep();
    		Delay(2000);
    	}
    	PORTE->ISFR |= BTN_SW2; // reset
    	cnt = 0;// reset cnt
    }
}

int main(void)
{

    /* Write your code here */
	init_hardware();
	//Beep();

    /* This for loop should be replaced. By default this loop allows a single stepping. */
	while (1);
    /* Never leave main */
    return 0;
}
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
