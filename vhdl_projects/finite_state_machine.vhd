-- fsm.vhd: Finite State Machine
-- Author(s):Nguyen Quang Trang
--
library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is (VSTUP_1,VSTUP_2,VSTUP_3,VSTUP_4,VSTUP_5A,VSTUP_6A,VSTUP_7A,VSTUP_8A,VSTUP_9A,VSTUP_10A,VSTUP_5B,VSTUP_6B,VSTUP_7B,VSTUP_8B,VSTUP_9B,VSTUP_10B,VSTUP_TEST,VSTUP_CHYBA,PRINT_CHYBA,PRINT_OK,IDLE);
   
   signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= VSTUP_1;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_1 =>
      next_state <= VSTUP_1;
      if (KEY(4) = '1') then
         next_state <= VSTUP_2; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 5) /= "0000000000") OR (KEY(3 downto 0) /= "0000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_2 =>
      next_state <= VSTUP_2;
      if (KEY(6) = '1') then
         next_state <= VSTUP_3; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 7) /= "00000000") OR (KEY(5 downto 0) /= "000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_3 =>
      next_state <= VSTUP_3;
      if (KEY(9) = '1') then
         next_state <= VSTUP_4;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 10) /= "00000") OR (KEY(8 downto 0) /= "000000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_4 =>
      next_state <= VSTUP_4;
      if (KEY(6) = '1') then
         next_state <= VSTUP_5A;
      elsif (KEY(7) = '1') then
          next_state <= VSTUP_5B; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 8) /= "0000000") OR (KEY(5 downto 0) /= "000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_5A =>
      next_state <= VSTUP_5A;
      if (KEY(5) = '1') then
         next_state <= VSTUP_6A; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 6) /= "000000000") OR (KEY(4 downto 0) /= "00000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_6A =>
      next_state <= VSTUP_6A;
      if (KEY(8) = '1') then
         next_state <= VSTUP_7A;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 9) /= "000000") OR (KEY(7 downto 0) /= "00000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_7A =>
      next_state <= VSTUP_7A;
      if (KEY(8) = '1') then
         next_state <= VSTUP_8A; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 9) /= "000000") OR (KEY(7 downto 0) /= "00000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_8A =>
      next_state <= VSTUP_8A;
      if (KEY(1) = '1') then
         next_state <= VSTUP_9A; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 2) /= "0000000000000") OR (KEY(0) /= '0')  then 
         next_state <= VSTUP_CHYBA;	 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_9A =>
      next_state <= VSTUP_9A;
      if (KEY(0) = '1') then
         next_state <= VSTUP_10A; 	 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 1) /= "00000000000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_10A =>
      next_state <= VSTUP_10A;
      if (KEY(6) = '1') then
         next_state <= VSTUP_TEST;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA;  
      elsif (KEY(14 downto 7) /= "00000000") OR (KEY(5 downto 0) /= "000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_5B =>
      next_state <= VSTUP_5B;
      if (KEY(9) = '1') then
         next_state <= VSTUP_6B; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 10) /= "00000") OR (KEY(8 downto 0) /= "000000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_6B =>
      next_state <= VSTUP_6B;
      if (KEY(5) = '1') then
         next_state <= VSTUP_7B;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 6) /= "000000000") OR (KEY(4 downto 0) /= "00000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_7B =>
      next_state <= VSTUP_7B;
      if (KEY(2) = '1') then
         next_state <= VSTUP_8B; 
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 3) /= "000000000000") OR (KEY(1 downto 0) /= "00") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_8B =>
      next_state <= VSTUP_8B;
      if (KEY(8) = '1') then
         next_state <= VSTUP_9B;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 9) /= "000000") OR (KEY(7 downto 0) /= "00000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_9B =>
      next_state <= VSTUP_9B;
      if (KEY(6) = '1') then
         next_state <= VSTUP_10B;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA;
      elsif (KEY(14 downto 7) /= "00000000") OR (KEY(5 downto 0) /= "000000") then 
         next_state <= VSTUP_CHYBA; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_10B =>
      next_state <= VSTUP_10B;
      if (KEY(4) = '1') then
         next_state <= VSTUP_TEST;
      elsif (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      elsif (KEY(14 downto 5) /= "0000000000") OR (KEY(3 downto 0) /= "0000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_TEST =>
      next_state <= VSTUP_TEST; 
      if (KEY(15) = '1') then
         next_state <= PRINT_OK; 
      elsif (KEY(14 downto 0) /= "000000000000000") then 
         next_state <= VSTUP_CHYBA;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when VSTUP_CHYBA =>
      next_state <= VSTUP_CHYBA;
      if (KEY(15) = '1') then
         next_state <= PRINT_CHYBA; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - 
   when PRINT_CHYBA =>
      next_state <= PRINT_CHYBA;
      if (CNT_OF = '1') then
         next_state <= IDLE;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - 
   when PRINT_OK =>
      next_state <= PRINT_OK;
      if (CNT_OF = '1') then
         next_state <= IDLE;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - 
   when IDLE =>
      next_state <= IDLE;
      if (KEY(15) = '1') then
         next_state <= VSTUP_1; 
      end if;
   end case;
end process next_state_logic;

-- ------------------------------------------------------

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';

   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_CHYBA =>
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
   -- - - - - - - - - - - - - - - - - - - - - - 
   when PRINT_OK =>
      FSM_MX_MEM     <= '1';
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';

   -- - - - - - - - - - - - - - - - - - - - - - 
    when IDLE =>
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - 
        when VSTUP_TEST =>
      if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - -
      when others =>
      if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   end case;
end process output_logic;

end architecture behavioral;
