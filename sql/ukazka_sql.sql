--------------------------------------------------------------------------------------------------------------------------------
--                                                  ZMAZANI TABULEK
--------------------------------------------------------------------------------------------------------------------------------
DROP TABLE Osoba CASCADE CONSTRAINTS;
DROP TABLE Zablokovany_uzivatel CASCADE CONSTRAINTS;
DROP TABLE Jidlo CASCADE CONSTRAINTS;
DROP TABLE Recept CASCADE CONSTRAINTS;
DROP TABLE Potravina CASCADE CONSTRAINTS;
DROP TABLE Recept_vyzaduje CASCADE CONSTRAINTS;
DROP TABLE Oblibeny_recept CASCADE CONSTRAINTS;
DROP TABLE Sportovni_aktivita CASCADE CONSTRAINTS;
DROP TABLE Zaznam_telesnych_parametru CASCADE CONSTRAINTS;
DROP TABLE Zaznam_sportovni_aktivity CASCADE CONSTRAINTS;
DROP TABLE Zaznam_snezeneho_jidla CASCADE CONSTRAINTS;



--------------------------------------------------------------------------------------------------------------------------------
--                                                  VYTVORENI TABULEK
--------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE Osoba (
login VARCHAR(50),
heslo VARCHAR(50) NOT NULL,
jmeno VARCHAR(100),
prijmeni VARCHAR(100),
email VARCHAR(100) NOT NULL,
pohlavi CHAR(1) CHECK (pohlavi='M' or pohlavi='Z'),
typ CHAR(1) CHECK (typ='U' or typ='S') NOT NULL,
info VARCHAR(500),
cilova_hmotnost NUMERIC(5,2),
mesto VARCHAR(100),
ulice VARCHAR(100),
psc CHAR(6) CHECK(REGEXP_LIKE(psc,'^\d{3} \d{2}$')),
telefon CHAR(16) CHECK(REGEXP_LIKE(telefon,'^\+\d{3} \d{3} \d{3} \d{3}$'))
);



CREATE TABLE Zablokovany_uzivatel (
uzivatel VARCHAR(50),
od TIMESTAMP,
do TIMESTAMP NOT NULL,
zablokovany_spravcom VARCHAR(50),
popis VARCHAR(500) NOT NULL
);

CREATE TABLE Jidlo (
id_jidla INTEGER GENERATED AS IDENTITY,
nazev VARCHAR(50) NOT NULL,
pridal VARCHAR(50) 
);


CREATE TABLE Recept (
id_jidla INTEGER,
postup VARCHAR(500) NOT NULL,
doba_pripravy INTERVAL DAY TO SECOND,
pocet_porcii INTEGER,
obtiznost VARCHAR(13) CHECK (obtiznost='snadne' or obtiznost='stredne tezke' or obtiznost='obtizne')
);


CREATE TABLE Potravina (
id_jidla INTEGER,
proteiny NUMERIC(4,2) NOT NULL,
sacharidy NUMERIC(4,2) NOT NULL,
tuky NUMERIC(4,2) NOT NULL,
vlaknina NUMERIC(4,2) NOT NULL,
kalorie INTEGER NOT NULL
);

CREATE TABLE Recept_vyzaduje (
recept INTEGER,
potravina INTEGER,
hmotnost_potraviny NUMERIC(6,2) NOT NULL
);

CREATE TABLE Oblibeny_recept (
uzivatel VARCHAR(50),
recept INTEGER
);

CREATE TABLE Sportovni_aktivita (
id_aktivity INTEGER GENERATED AS IDENTITY ,
nazev VARCHAR(50) NOT NULL,
popis VARCHAR(500) NOT NULL,
kcal_kg_min NUMERIC(4,3) NOT NULL,
pridal VARCHAR(50) 
);

CREATE TABLE Zaznam_telesnych_parametru (
datum_cas TIMESTAMP,
uzivatel VARCHAR(50),
hmotnost NUMERIC(5,2),
vyska NUMERIC(5,2),
obvod_hrudniku NUMERIC(5,2),
obvod_pasu NUMERIC(5,2),
obvod_stehna NUMERIC(5,2),
obvod_lytka NUMERIC(5,2)
);

CREATE TABLE Zaznam_sportovni_aktivity (
datum_cas TIMESTAMP,
uzivatel VARCHAR(50),
trvani INTERVAL DAY TO SECOND NOT NULL,
aktivita INTEGER NOT NULL
);

CREATE TABLE Zaznam_snezeneho_jidla (
datum_cas TIMESTAMP,
uzivatel VARCHAR(50),
hmotnost NUMERIC(6,2) NOT NULL,
jidlo INTEGER NOT NULL
);

--------------------------------------------------------------------------------------------------------------------------------
--                                          NASTAVENI PRIMARNICH A CIZICH KLICU
--------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE Osoba ADD CONSTRAINT PK_osoba PRIMARY KEY (login);

ALTER TABLE Zablokovany_uzivatel ADD CONSTRAINT FK_zablokovany_uzivatel_uzivatel FOREIGN KEY (uzivatel) REFERENCES Osoba ON DELETE CASCADE;
ALTER TABLE Zablokovany_uzivatel ADD CONSTRAINT FK_zablokovany_uzivatel_zablokovany_spravcom FOREIGN KEY (zablokovany_spravcom) REFERENCES Osoba;
ALTER TABLE Zablokovany_uzivatel ADD CONSTRAINT PK_zablokovany_uzivatel PRIMARY KEY (uzivatel, od);

ALTER TABLE Jidlo ADD CONSTRAINT FK_jidlo_pridal FOREIGN KEY (pridal) REFERENCES Osoba;
ALTER TABLE Jidlo ADD CONSTRAINT PK_jidlo PRIMARY KEY (id_jidla);

ALTER TABLE Recept ADD CONSTRAINT FK_recept_id_jidla FOREIGN KEY (id_jidla) REFERENCES Jidlo ON DELETE CASCADE;
ALTER TABLE Recept ADD CONSTRAINT PK_recept PRIMARY KEY (id_jidla);

ALTER TABLE Potravina ADD CONSTRAINT FK_potravina_id_jidla FOREIGN KEY (id_jidla) REFERENCES Jidlo ON DELETE CASCADE;
ALTER TABLE Potravina ADD CONSTRAINT PK_potravina PRIMARY KEY (id_jidla);

ALTER TABLE Recept_vyzaduje ADD CONSTRAINT FK_recept_vyzaduje_recept FOREIGN KEY (recept) REFERENCES  Recept ON DELETE CASCADE;
ALTER TABLE Recept_vyzaduje ADD CONSTRAINT FK_recept_vyzaduje_potravina FOREIGN KEY (potravina) REFERENCES Potravina ON DELETE CASCADE;
ALTER TABLE Recept_vyzaduje ADD CONSTRAINT PK_recept_vyzaduje PRIMARY KEY (recept,potravina);

ALTER TABLE Oblibeny_recept ADD CONSTRAINT FK_Oblibeny_recept_uzivatel FOREIGN KEY (uzivatel) REFERENCES Osoba ON DELETE CASCADE;
ALTER TABLE Oblibeny_recept ADD CONSTRAINT FK_Oblibeny_recept_recept FOREIGN KEY (recept) REFERENCES Recept ON DELETE CASCADE;
ALTER TABLE Oblibeny_recept ADD CONSTRAINT PK_Oblibeny_recept PRIMARY KEY (uzivatel,recept);

ALTER TABLE Sportovni_aktivita ADD CONSTRAINT FK_sportovni_aktivita_pridal FOREIGN KEY (pridal) REFERENCES Osoba;
ALTER TABLE Sportovni_aktivita ADD CONSTRAINT PK_sportovni_aktivita PRIMARY KEY (id_aktivity);

ALTER TABLE Zaznam_telesnych_parametru ADD CONSTRAINT FK_zaznam_telesnych_parametru_uzivatel FOREIGN KEY (uzivatel) REFERENCES Osoba ON DELETE CASCADE;
ALTER TABLE Zaznam_telesnych_parametru ADD CONSTRAINT PK_zaznam_telesnych_parametru PRIMARY KEY (uzivatel,datum_cas);

ALTER TABLE Zaznam_sportovni_aktivity ADD CONSTRAINT FK_zaznam_sportovni_aktivity_uzivatel FOREIGN KEY (uzivatel) REFERENCES Osoba ON DELETE CASCADE;
ALTER TABLE Zaznam_sportovni_aktivity ADD CONSTRAINT FK_zaznam_sportovni_aktivity_aktivita FOREIGN KEY (aktivita) REFERENCES Sportovni_aktivita ON DELETE CASCADE;
ALTER TABLE Zaznam_sportovni_aktivity ADD CONSTRAINT PK_zaznam_sportovni_aktivity PRIMARY KEY (uzivatel,datum_cas);

ALTER TABLE Zaznam_snezeneho_jidla ADD CONSTRAINT FK_zaznam_snezeneho_jidla_uzivatel FOREIGN KEY (uzivatel) REFERENCES Osoba ON DELETE CASCADE;
ALTER TABLE Zaznam_snezeneho_jidla ADD CONSTRAINT FK_zaznam_snezeneho_jidla_jidlo FOREIGN KEY (jidlo) REFERENCES Jidlo ON DELETE CASCADE;
ALTER TABLE Zaznam_snezeneho_jidla ADD CONSTRAINT PK_zaznam_snezeneho_jidla PRIMARY KEY (uzivatel,datum_cas);


--------------------------------------------------------------------------------------------------------------------------------
--                                                      VLOZENI DAT
--------------------------------------------------------------------------------------------------------------------------------
INSERT INTO Osoba (login,heslo,jmeno,prijmeni,email,pohlavi,typ,info,cilova_hmotnost,mesto,ulice,psc,telefon) VALUES ('pepa97','heslo01','Jozef','Maly','sdadsa','M','U','Rad tancujem.',80,NULL,NULL,NULL,NULL);
INSERT INTO Osoba (login,heslo,jmeno,prijmeni,email,pohlavi,typ,info,cilova_hmotnost,mesto,ulice,psc,telefon) VALUES ('kristynka16','heslo23','Kristina','Orecna','kristyna@email.com','Z','S','Som fitness trenerka.',NULL,'Bratislava','Tomkova 46','811 04','+421 123 456 789');
INSERT INTO Osoba (login,heslo,jmeno,prijmeni,email,pohlavi,typ,info,cilova_hmotnost,mesto,ulice,psc,telefon) VALUES ('westrock','heslo45','Pavel','Vesely','pavel@email.com','M','S','Som byvaly bodybuilder.',NULL,'Brno','Podzimni 13','614 00','+420 123 456 789');
INSERT INTO Osoba (login,heslo,jmeno,prijmeni,email,pohlavi,typ,info,cilova_hmotnost,mesto,ulice,psc,telefon) VALUES ('mil@n64','heslo67','Milan','Tomek','milan@email.com','M','U','Chcem sa stat bodybuilderom.',100,NULL,NULL,NULL,NULL);
INSERT INTO Osoba (login,heslo,jmeno,prijmeni,email,pohlavi,typ,info,cilova_hmotnost,mesto,ulice,psc,telefon) VALUES ('Ernest0','heslo89','Ernest','Vesely','ernest@email.com','M','U','Pravidelne sa zucastnujem maratonov.',65,NULL,NULL,NULL,NULL);

INSERT INTO Zablokovany_uzivatel (uzivatel,od,do,zablokovany_spravcom,popis) VALUES ('pepa97',TO_TIMESTAMP('2016-08-01 18:45:00','YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2016-08-15 18:45:00','YYYY-MM-DD HH24:MI:SS'),'kristynka16','Uvedenie potraviny so zjavne nespravnymi nutricnymi hodnotami.');
INSERT INTO Zablokovany_uzivatel (uzivatel,od,do,zablokovany_spravcom,popis) VALUES ('pepa97',TO_TIMESTAMP('2017-11-10 07:30:16','YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2017-12-10 7:30:16','YYYY-MM-DD HH24:MI:SS'),'kristynka16','Pridanie receptu zkopirovanim z inej stranky.');
INSERT INTO Zablokovany_uzivatel (uzivatel,od,do,zablokovany_spravcom,popis) VALUES ('pepa97',TO_TIMESTAMP('2018-03-28 11:10:00','YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2018-09-28 11:10:00','YYYY-MM-DD HH24:MI:SS'),'westrock','Vulgarizmy v pridanej sportovej aktivite.');
INSERT INTO Zablokovany_uzivatel (uzivatel,od,do,zablokovany_spravcom,popis) VALUES ('mil@n64',TO_TIMESTAMP('2018-04-02 15:49:16','YYYY-MM-DD HH24:MI:SS'),TO_TIMESTAMP('2018-05-02 15:49:16','YYYY-MM-DD HH24:MI:SS'),'kristynka16','Pridanie receptu zkopirovanim z inej stranky.');

INSERT INTO Jidlo (nazev,pridal) VALUES ('Jablko','kristynka16');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Hovadzie maso','mil@n64');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Ryza','westrock');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Knedlik','pepa97');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Varene zemiaky','westrock');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Mlieko','mil@n64');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Huby','Ernest0');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Kuracie maso','kristynka16');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Mrkva','kristynka16');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Jahoda','pepa97');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Citron','pepa97');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Hovadzia svieckova','Ernest0');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Kuraci steak s ryzou','kristynka16');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Jahodovy koktail','kristynka16');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Hovadzie maso so zemiakmi','westrock');
INSERT INTO Jidlo (nazev,pridal) VALUES ('Mrkvovy salat','kristynka16');

INSERT INTO Recept (id_jidla,postup,doba_pripravy,pocet_porcii,obtiznost) VALUES (12,'Uvarime maso. Spravime knedliky a omacku. Podavame na tanier.',INTERVAL '45' MINUTE,2,'obtizne');
INSERT INTO Recept (id_jidla,postup,doba_pripravy,pocet_porcii,obtiznost) VALUES (13,'Uvarime maso. Spravime ryzu. Podavame na tanier.',INTERVAL '35' MINUTE,2,'stredne tezke');
INSERT INTO Recept (id_jidla,postup,doba_pripravy,pocet_porcii,obtiznost) VALUES (14,'Jahody ocistime, nahadzeme do mixeru a rozmixujeme s mliekom.',INTERVAL '10' MINUTE,1,'snadne');
INSERT INTO Recept (id_jidla,postup,doba_pripravy,pocet_porcii,obtiznost) VALUES (15,'Uvarime maso. Ocistime zemiaky. Do omacky pridame huby. Dame to varit. Podavame na tanier.',INTERVAL '45' MINUTE,4,'obtizne');
INSERT INTO Recept (id_jidla,postup,doba_pripravy,pocet_porcii,obtiznost) VALUES (16,'Mrkvu rozsekame do misy, pridame citronovu stavu.',INTERVAL '15' MINUTE,1,'snadne');
INSERT INTO Recept (id_jidla,postup,doba_pripravy,pocet_porcii,obtiznost) VALUES (10,'test jahody.',INTERVAL '15' MINUTE,1,'snadne');

INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (1,0.37,13,0.4,3,56.8);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (2,25,0.37,8,0,170.5);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (3,2.67,28,0.28,0,129.5);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (4,7,42,2,1,212.6);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (5,1,16,0.08,2,66.9);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (6,3,5,2,0,47.3);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (7,2,3,0.5,0,26);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (8,27,0,7,0,170.1);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (9,1,7,0.22,4,35.3);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (10,0.79,6,0.37,2,34.9);
INSERT INTO Potravina (id_jidla,proteiny,sacharidy,tuky,vlaknina,kalorie) VALUES (11,1,2,0,0,20);

INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (12,2,300);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (12,4,250);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (13,3,200);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (13,8,300);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (14,6,200);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (14,10,200);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (15,2,300);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (15,5,300);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (15,7,15);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (16,9,25);
INSERT INTO Recept_vyzaduje (recept,potravina,hmotnost_potraviny) VALUES (16,11,5);

INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('pepa97',12);
INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('pepa97',13);
INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('pepa97',14);
INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('pepa97',16);
INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('mil@n64',13);
INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('mil@n64',15);
INSERT INTO Oblibeny_recept (uzivatel,recept) VALUES('Ernest0',15);

INSERT INTO Sportovni_aktivita (nazev,popis,kcal_kg_min,pridal) VALUES ('Chodza','Chodza 2 km/h po rovine.',0.029,'kristynka16');
INSERT INTO Sportovni_aktivita (nazev,popis,kcal_kg_min,pridal) VALUES ('Beh','Beh do schodov.',0.234,'kristynka16');
INSERT INTO Sportovni_aktivita (nazev,popis,kcal_kg_min,pridal) VALUES ('Beh','Beh 15 km/h po rovine.',0.265,'Ernest0');
INSERT INTO Sportovni_aktivita (nazev,popis,kcal_kg_min,pridal) VALUES ('Posilovanie','Kruhovy intenzivny trening.',0.141,'westrock');
INSERT INTO Sportovni_aktivita (nazev,popis,kcal_kg_min,pridal) VALUES ('Plavanie','Rekreacne plavanie.',0.163,'pepa97');

INSERT INTO Zaznam_telesnych_parametru (datum_cas,uzivatel,hmotnost,vyska,obvod_hrudniku,obvod_pasu,obvod_stehna,obvod_lytka) VALUES (TO_TIMESTAMP('2017-02-04 15:00:00','YYYY-MM-DD HH24:MI:SS'),'pepa97',71,185,105,90,48,39);
INSERT INTO Zaznam_telesnych_parametru (datum_cas,uzivatel,hmotnost,vyska,obvod_hrudniku,obvod_pasu,obvod_stehna,obvod_lytka) VALUES (TO_TIMESTAMP('2017-06-12 11:45:00','YYYY-MM-DD HH24:MI:SS'),'pepa97',75,186,111,92,52,40);
INSERT INTO Zaznam_telesnych_parametru (datum_cas,uzivatel,hmotnost,vyska,obvod_hrudniku,obvod_pasu,obvod_stehna,obvod_lytka) VALUES (TO_TIMESTAMP('2017-01-01 08:10:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',85,175,115,95,58,44);
INSERT INTO Zaznam_telesnych_parametru (datum_cas,uzivatel,hmotnost,vyska,obvod_hrudniku,obvod_pasu,obvod_stehna,obvod_lytka) VALUES (TO_TIMESTAMP('2018-01-03 21:20:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',97,175,138,100,69,47);
INSERT INTO Zaznam_telesnych_parametru (datum_cas,uzivatel,hmotnost,vyska,obvod_hrudniku,obvod_pasu,obvod_stehna,obvod_lytka) VALUES (TO_TIMESTAMP('2018-02-25 22:30:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',73,180,99,85,43,36);

INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2017-01-30 17:14:00','YYYY-MM-DD HH24:MI:SS'),'pepa97',INTERVAL '30' MINUTE,5);
INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2017-01-30 10:06:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',INTERVAL '1:45' HOUR TO MINUTE,4);
INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2017-01-30 14:30:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',INTERVAL '1:10' HOUR TO MINUTE,2);
INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2017-06-10 20:15:00','YYYY-MM-DD HH24:MI:SS'),'pepa97',INTERVAL '1:20' HOUR TO MINUTE,4);
INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2018-01-03 18:05:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',INTERVAL '1:40' HOUR TO MINUTE,4);
INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2018-02-20 15:32:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',INTERVAL '30' MINUTE,1);
INSERT INTO Zaznam_sportovni_aktivity (datum_cas,uzivatel,trvani,aktivita) VALUES (TO_TIMESTAMP('2018-02-24 11:44:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',INTERVAL '2:10' HOUR TO MINUTE,3);

INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2017-06-05 20:23:00','YYYY-MM-DD HH24:MI:SS'),'pepa97',40,16);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2017-06-06 08:10:00','YYYY-MM-DD HH24:MI:SS'),'pepa97',50,8);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-01-03 08:05:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',120,1);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-01-03 11:33:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',250,3);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-01-03 13:20:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',450,12);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-01-03 16:47:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',400,13);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-01-03 21:06:00','YYYY-MM-DD HH24:MI:SS'),'mil@n64',400,15);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-02-24 06:15:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',300,6);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-02-24 09:48:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',120,5);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-02-24 15:02:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',250,3);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-02-24 18:21:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',300,1);
INSERT INTO Zaznam_snezeneho_jidla (datum_cas,uzivatel,hmotnost,jidlo) VALUES (TO_TIMESTAMP('2018-02-24 21:57:00','YYYY-MM-DD HH24:MI:SS'),'Ernest0',300,6);


--------------------------------------------------------------------------------------------------------------------------------
--                                                      SELECTY
--------------------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------
--              DOTAZY SPOJENI 2 TABULEK
-----------------------------------------------------------

--zobrazi nazvy vsech oblubenych receptu uzivatela 'pepa97'
SELECT J.nazev AS oblubene_recepty
FROM Oblibeny_recept O_R JOIN Jidlo J ON(O_R.recept=J.id_jidla)
WHERE O_R.uzivatel='pepa97';

--zobrazi vsechny loginy a emaily uzivatelu, kteri byli zablokovany spravkyni systemu 'kristynka16', uzivatele se zobrazi pouze jednou (DISTINCT)
SELECT DISTINCT O.login, O.email
FROM Osoba O JOIN Zablokovany_uzivatel Z_U ON(O.login=Z_U.uzivatel)
WHERE Z_U.zablokovany_spravcom='kristynka16';

-----------------------------------------------------------
--              DOTAZ SPOJENI 3 TABULEK
-----------------------------------------------------------

--zobrazi jmeno, prezdivka a aktvita uzivatela, kterz vykonal v dne 2017-01-30
SELECT O.jmeno,O.prijmeni,S_A.nazev AS Aktivita
FROM Zaznam_sportovni_aktivity Z_S_A JOIN Osoba O ON(Z_S_A.uzivatel=O.login) JOIN Sportovni_aktivita S_A ON(Z_S_A.aktivita=S_A.id_aktivity)
WHERE TO_CHAR(cast((Z_S_A.datum_cas) AS DATE),'YYYY-MM-DD')='2017-01-30';


-----------------------------------------------------------
--              DOTAZY S KLAUZULOU GROUP BY
-----------------------------------------------------------

--zobrazi nutricne hodnoty v gramoch a kaloricke hodnoty v kcal vsetkych receptov
SELECT J.nazev AS recept, SUM(R_V.hmotnost_potraviny*P.proteiny*0.01) AS bielkoviny, SUM(R_V.hmotnost_potraviny*P.sacharidy*0.01) AS cukry,
SUM(R_V.hmotnost_potraviny*P.tuky*0.01) AS tuky, SUM(R_V.hmotnost_potraviny*P.vlaknina*0.01) AS vlaknina, SUM(R_V.hmotnost_potraviny*P.kalorie*0.01) AS kalorie
FROM Recept_vyzaduje R_V JOIN Jidlo J ON(R_V.recept=J.id_jidla) JOIN Potravina P ON(R_V.potravina=P.id_jidla)
GROUP BY J.nazev


--zobrazi rekordne najdlhsie trvanie sportovych aktivit vsetkych uzivatelov, zoradene od najdlhsieho trvania po najkratsie
SELECT O.login, MAX(Z_S_A.TRVANI) AS dlzka_aktivity
FROM Zaznam_sportovni_aktivity Z_S_A JOIN Osoba O ON(Z_S_A.uzivatel=O.login)
GROUP BY O.login
ORDER BY  dlzka_aktivity DESC;

-----------------------------------------------------------
--              DOTAZ S PREDIKATEM EXISTS
-----------------------------------------------------------

--zobrazi jmeno, prezdivka a popis o uzivatelech, kteri maji v zaznamech o sportovnich aktivitach pouze posilovani (aktivita 4)
SELECT  DISTINCT O.jmeno, O.prijmeni, O.info
FROM Osoba O JOIN Zaznam_sportovni_aktivity Z_S_A ON(Z_S_A.uzivatel=O.login)
WHERE Z_S_A.aktivita='4' AND NOT EXISTS
    (SELECT * 
     FROM Zaznam_sportovni_aktivity Z_S_A
     WHERE O.login=Z_S_A.uzivatel AND Z_S_A.aktivita<>'4');

-----------------------------------------------------------
--              DOTAZ S PREDIKATEM IN
-----------------------------------------------------------

--zobrazi nazvy jedel, ktere maji obtiznost receptu snadne
SELECT nazev
FROM Jidlo
WHERE id_jidla IN (SELECT id_jidla FROM Recept WHERE obtiznost='snadne');

-----------------------------------------------------------
--              PROCEDURY + UKAZKY
-----------------------------------------------------------
SET SERVEROUTPUT ON;

-----------------------------------------------------------
--              kontroluje spravnost emailu 
-----------------------------------------------------------

CREATE OR REPLACE PROCEDURE check_email
  IS
    CURSOR uzivatel IS SELECT * FROM osoba;
    item uzivatel%ROWTYPE;
  BEGIN
    FOR item in uzivatel
  LOOP
  IF (item.email NOT LIKE '%@%.%')
    THEN
      dbms_output.put_line('Uzivatel:' || item.jmeno || '  uvedl spatny email:' || item.email);
  END IF;
  END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20,'nastala chyba pri kontrole emailu');
  END;
   /
  
  -----------------------------------------------------------
--             vraci dodatecne informace o receptu (ocekava id_recept jako vstup)
-----------------------------------------------------------
  
  exec check_email();
  
  CREATE OR REPLACE PROCEDURE zjisti_info_recept(id_recept IN INTEGER)
  IS 
    CURSOR info IS SELECT * FROM Recept;
    item info%ROWTYPE;
    BEGIN
    FOR item in info
    LOOP
      IF(TRIM(item.id_jidla) = id_recept)
        THEN
          dbms_output.put_line('doba pripavy: ' || item.doba_pripravy || ' postup: ' || item.postup || '  pocet porci: ' || item.pocet_porcii || ' obtiznost pripravy: ' || item.obtiznost);
      END IF;
    END LOOP;
    EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-21,'nastala chyba pri zjisteni informace');
  END;
   /
exec zjisti_info_recept(12);
--------------------------------------------------------------------------------------------------------------------------------
--                                                      OPRAVNENI
--------------------------------------------------------------------------------------------------------------------------------
GRANT ALL ON Osoba TO XSABOJ00;
GRANT ALL ON Zablokovany_uzivatel TO XSABOJ00;
GRANT ALL ON Jidlo TO XSABOJ00;
GRANT ALL ON Recept TO XSABOJ00;
GRANT ALL ON Potravina TO XSABOJ00;
GRANT ALL ON Recept_vyzaduje TO XSABOJ00;
GRANT ALL ON Oblibeny_recept TO XSABOJ00;
GRANT ALL ON Zaznam_telesnych_parametru TO XSABOJ00;
GRANT ALL ON Sportovni_aktivita TO XSABOJ00;
GRANT ALL ON Zaznam_telesnych_parametru TO XSABOJ00;
GRANT ALL ON Zaznam_sportovni_aktivity TO XSABOJ00;
GRANT ALL ON Zaznam_snezeneho_jidla TO XSABOJ00;

GRANT EXECUTE ON zjisti_info_recept TO XSABOJ00;
GRANT EXECUTE ON check_email TO XSABOJ00;

--INDEX
-- pocet jednoduchych jidel pridany jednotlivymi uzivateli, spojeni dvou tabulek
EXPLAIN PLAN FOR
SELECT pridal, COUNT(*) AS Pocet_jednoduchych_jidel
FROM Jidlo JOIN Recept R USING(id_jidla) 
WHERE R.obtiznost = 'snadne'
GROUP BY (pridal);

SELECT plan_table_output FROM TABLE(dbms_xplan.display());
DROP INDEX index_obtiznosti;

CREATE INDEX index_obtiznosti ON Recept(obtiznost);

EXPLAIN PLAN FOR
SELECT pridal, COUNT(*) AS Pocet_jednoduchych_jidel
FROM Jidlo JOIN Recept R USING(id_jidla) 
WHERE R.obtiznost = 'snadne'
GROUP BY (pridal);

SELECT plan_table_output FROM TABLE(dbms_xplan.display());

-- Materializovany pohled
-- DRUHY clen musi udelat: DROP MATERIALIZED VIEW LOG ON Oblibeny_recept, DROP MATERIALIZED VIEW LOG ON Jidlo
-- CREATE MATERIALIZED VIEW LOG ON Oblibeny_recept WITH PIMARY KEY, ROWID;
-- CREATE MATERIALIZED VIEW LOG ON Jidlo WITH PIMARY KEY, ROWID;
-- GRANT ALL ON MLOG$_Oblibeny_recept to xnguye11; , GRANT ALL ON MLOG$_Jidlo to xnguye11;

CREATE MATERIALIZED VIEW pohled_osoba_oblibeny_recept
CACHE
BUILD IMMEDIATE
REFRESH FAST ON COMMIT
ENABLE QUERY REWRITE
AS SELECT jmeno, nazev, Oblibeny_recept.ROWID Oblibeny_recept_row_id, Jidlo.ROWID Jidlo_row_id FROM xsaboj00.Oblibeny_recept O_R JOIN xsaboj00.Jidlo J ON(O_R.recept=J.id_jidla);

GRANT ALL ON pohled_osoba_oblibeny_recept to xsaboj00;


