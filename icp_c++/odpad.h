// author nguyen quang trang login xnguye11
#ifndef ODPAD_H
#define ODPAD_H
#include <iostream>
#include "simlib.h"
#include <getopt.h>

using namespace std;

class Netridene: public Process{
    void Behavior() override;
};

class t1_svoz : public Process{
    void Behavior() override;
};

class n_sklad : public Process{
    void Behavior() override;
};
class t_sklad : public Process{
    void Behavior() override;
};

class t2_porucha : public Process{
    void Behavior() override;
};

class t1_porucha : public Process {
    void Behavior() override;
};

class Tridene: public Process{
    void Behavior() override;
};

class t2_svoz : public Process{
    void Behavior() override;
};

class  Odpad : public Process {
public:
    explicit Odpad(int c) : Process() {
        odpad_c = c;
    };
    void Behavior() override;
    int odpad_c;
};
#endif