//author nguyen quang trang login xnguye11
#include "odpad.h"

// 1 odpovida 100kg
Facility n_car("netridene auto");//auto pro nakladani netridenych odpadu
Facility t_car("tridene auto");//auto pro nakladani tridenych odpadu
//Histogram hist("Doba stravena v systemu", 0, 5, 7);

int bioplyn = 0;// predikat bioplyn
int spalovani = 0;// predikat spalovani
int papir = 0;//zda budeme nahrazovat plast za papir
int sklad_kom = 0;//sklad komunalnich odpadu
//int sklad_trid = 0;//skald tridenych odpadu
int event_cnt = 0;//pocet event na rok
int komunal_cnt = 0;//netridene odpady za 1rok
int trideni_cnt = 0;//tridene odpady za rok
int netridene = 0;// netridene odpady/cyklus
int odvoz = 0;//pocet odvozu za rok
double odpad_cnt = 0;// pocet odpadu na skladisti
int odpad = 130;// pocet odpadu co musime zpracovat/120 za tyden
double CO2_car = 0.0; // c02 vyprodukovane autem
double CO2_cnt = 0.0;// celkova c02
double plasty_cnt = 0;// celkovych pocet plastu na skladisti
int plasty = 0;//pocet plastu na recyklovani
int poruchy_cnt = 0;// poruchy
double netrideny_odpad = 0.77;
int skladka_povolena = 1;

// simulace nakladani odpadu
void Netridene::Behavior(){
        Seize(n_car);// simulace nakladani odpadu
        Release(n_car);
};

// simulace nakladani odpadu
void Tridene::Behavior(){
    Seize(t_car);// simulace nakladani odpadu
    if(Random() <= 0.25)// u tridenych odpadu potrebuje jen zpracovavat plasty , jine odpady jako je (papir, sklo, bio ,kov) se daji recyklovat
    {
        plasty++;
    }
    else
        ;
    Release(t_car);
};
//kontrola poruchy pri prijezdu
void t1_porucha::Behavior() {
        Seize(n_car);
        if(Random() <= 0.95)
            Release(n_car);
        else{
            poruchy_cnt++;
            Wait(Exponential(1));//auto neni ok musime cekat 1 den
            Release(n_car);
        }
};
//kontrola poruchy pri prijezdu
void t2_porucha::Behavior(){
        Seize(t_car);
        if(Random() <= 0.95)
            Release(t_car);
        else{
            poruchy_cnt++;
            Wait(Exponential(1)); //auto neni ok musime cekat 1den
            Release(t_car);
        }
};
//rozhodovani zda budeme splanovat nebo ne
void n_sklad::Behavior(){
        if (skladka_povolena) {
		//netridene++;
		if(Random() <= 0.74){
			odpad_cnt++;
		} else {
			if(spalovani){
				odpad_cnt += 0.25;
				CO2_cnt += 0.03;
			} else {
				odpad_cnt++;
			}
		}
	} else {
		odpad_cnt += 0.25;
		CO2_cnt += 0.03;
	}
};

// 
void t_sklad::Behavior(){//jen 70% plastu jede na sklad  a 30% se recykluje
        //odpad_cnt+=0.7;
        if(Random()<= 0.7)
        {
            if(!papir) // nepouzili jsme nahradu 
            {
                odpad_cnt++;
            }
            plasty_cnt++;
        }
        
};
//svoz netridenych odpadu
void t1_svoz::Behavior(){
            odvoz++;
            Seize(n_car);// simulace odjezdu 2h
            Wait(Exponential(0.083));
            Release(n_car);
            CO2_cnt += 0.013;//prictem c02 vyprodukovane pri jizdy
            CO2_car += 0.013;
            //int pom = komunal_cnt;
            //cout << netridene << "\n";
            while(netridene != 0){// simulace vylozeni odpadu na sklad
                netridene--;
                Seize(n_car);
                (new n_sklad )->Activate();// jede na netrideny sklad
                Release(n_car);
            }
};
// tridenych odpadu
void t2_svoz::Behavior(){
        odvoz++;
        Seize(t_car);// simulace odjezdu 2h
        Wait(Exponential(0.083));
        CO2_cnt += 0.013;//prictem c02 vyprodukovane pri jizdy
        CO2_car += 0.013;
        Release(t_car);
        //cout << papir << "\n";
        // cout << "helo world \n";
        while (plasty != 0)// plasty musime zpracovat
        {
            plasty--;
            Seize(n_car);
            (new t_sklad)->Activate();// jede na zpracovani
            Release(n_car);
        }
            
        //jinak u tridenych odpadu nevznikaji c02, jsou recyklovany
};

// zpracovani odpadu
void  Odpad::Behavior() {
        if(this->odpad_c > 0){ // vygeneruje 120 procesu
            this->odpad_c--;//snizime proces
            if(Random() <= netrideny_odpad){// vygenerujeme zda je to trideni nebo netrideny odpad a podle toho pokracujeme
                komunal_cnt++;
                netridene++;
                (new Netridene)->Activate();// je to netrideny odpad aktivuj nakladani
            }
            else{
                trideni_cnt++;
                (new Tridene)->Activate();//je to trideny odpad aktivuj nakladani
            }
            (new Odpad(this->odpad_c))->Activate();
        }
        else// aktivuj svoz uz neni co nalozit
        {
            (new t1_svoz)->Activate();
            (new t2_svoz)->Activate();
        }
        //netridene = 0;
        //zpracovani odpadu na sklade
};
// generovani
class Generator : public Event{
public:
        void Behavior(){
            event_cnt++;
            (new Odpad(odpad))->Activate();
            (new t1_porucha)->Activate();
            (new t2_porucha)->Activate();
            Activate(Time + 7);
        }
};

void help()
{
    cout << "pouziti ./odpad -r -o -s -p \n";
    cout << "argument -h vypise navod \n";
    cout << "argument -r doba simulace (1 = 1rok),default = 1 rok povinny parametr\n";
    cout << "argument -o objem odpadu (1 = 100 kg),default = 120 povinny parametr\n";
    cout << "argument -s pro spalovani cast odpadu\n";
    cout << "argument -p nahrazuje plasty za papir\n";
    cout << "argument -n nastavuje procentualni zastoupeni netrideneho odpadu, default = 77, povinny parametr\n";
    cout << "argument -d zakaze odvoz odpadu na skladku => netrideny odpad se vzdy spaluje\n";
}

int main(int argc, char* argv[]){
    //zpracovani argumentu
    int c , r = 1;
    while ((c = getopt_long(argc, argv, "o:r:n:dhps", nullptr, nullptr)) != -1) {
        switch (c) {
            case 'r':
		r = std::stoi(optarg);
		break;
            case 'o':
		odpad = std::stoi(optarg);
		break;
	    case 'n':
		netrideny_odpad = std::stoi(optarg);
		netrideny_odpad /= 100;	// prevod na procenta
		break;
	    case 'd':
		skladka_povolena = 0;
		break;
            case 'h':
                help();
                return 0;
            case 'p':
                papir = 1;
                break;
            case 's':
                spalovani = 1;
                break;
            default:
                fprintf(stderr, "Chyba agumentu.\n");
                return 1;
        }
    }
    Init(0,r*365);
    (new Generator())->Activate();
    Run();
    n_car.Output();//vypis obsluhy aut
    t_car.Output();//vypis obsluhy aut
    odpad_cnt /= 10;
    double celkovy_odpad = (komunal_cnt + plasty_cnt)/10;;
    //cout << "celkovy odpad: " << celkovy_odpad << endl;
    //cout << "odpad: " << odpad_cnt << endl;
    double pom = CO2_car;
    //cout << "pom:" << pom << endl;
    //cout << "celkovy event: " << event_cnt << endl;
    //cout << "celkove odvozy: " << odvoz << endl;
    //cout << "CO2 vyprodukovane pri odvozu odpadu na skladisti: " << CO2_car << "tun" << endl;
    //cout << "odpady na skladisti: " << odpad_cnt << " tun" << endl;
    //cout << celkovy_odpad << endl;
    CO2_cnt += odpad_cnt * 85 / 36;// vypocet celkove c02 za rok
    if(spalovani || papir)
    {
        pom += celkovy_odpad * 85 / 36;
        //cout << "c02 v tunach bez pouziti spalovani a nahradu papiru: " << pom << endl; 
        //cout << "emise c02 byla snizena o: " << pom - CO2_cnt << " tun" << endl;
    }
    /*cout << "plasty :" << plasty << "\n";
    cout << "papiry :" << papir << "\n";*/
    //cout << netridene << "\n";
    cout << "uhlikova stopa: " << CO2_cnt << " tun" << endl;
    //cout << "pocet poruch :" << poruchy_cnt << endl;
    
    return 0; 
}
