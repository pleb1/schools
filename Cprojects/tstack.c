// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// David Nguyen <xnguye11@stud.fit.vutbr.cz>
#include "tstack.h"
#include "error.h"
#include "lexer.h"
int* err;
int  stackInit(tStack* s)
{
    if (s == NULL) {
        *err = InternalError;
        return 0;
    }
    s->value = 7; // start with $
    s->top = -1;  //
    return 1;
}

token_t stacktop(tStack* s)
{
    return s->tok[s->top];
}

int stackEmpty(tStack* s)
{
    if (s == NULL)
        return 1;
    if (s->top < 0)
        return 1;
    else
        return 0;
}

int stackFull(const tStack* s)
{
    return ((s->top + 1) < STACK_SIZE) ? 0 : 1;
}

int stackPush(tStack* s, /* TODO: fix */ token_t tok)
{
    if (stackFull(s) != 0) {
        *err = InternalError;
        return 0;
    }

    s->tok[s->top + 1] = tok;
    s->top++;
    return 1;
}

// TODO: fix
token_t stackPop(tStack* s)
{
    if (!stackEmpty(s)) {
        s->top--;
        return s->tok[s->top + 1];
    }
    return s->tok[0];
}
int TopValue(tStack* s)
{
    return s->value;
}