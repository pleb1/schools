// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// David Nguyen <xnguye11@stud.fit.vutbr.cz>
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>

#include "error.h"
#include "lexer.h"
#include "parser.h"

int main(int argc, char** argv)
{
    lexer_state* lex = lexer_init(0);
    if (!lex)
        return InternalError;

    int err = 0;
    parse(lex, &err);

    lexer_close(lex);

    return 0;
}
