// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
#pragma once
#include <stdint.h>

typedef struct string_t string_t;

string_t* string_new(void);
void      string_free(string_t* str);
void      string_free_safe(string_t** str_ptr);
void      string_clear(string_t* str);
int       string_cat(string_t* a, const string_t* b);
int       string_append(string_t* str, const char c);
int       string_copy(const string_t* from, string_t* to);
int       string_copy_bound(const string_t* from, string_t* to, uint32_t begin, uint32_t end);
int       string_cmp(const string_t* a, const string_t* b);
int       string_cmp_cstr(const string_t* str, const char* cstr);
int       string_find(const string_t* str, const string_t* substr);
int       string_find_cstr(const string_t* str, const char* substr);
int       string_contains(const string_t* str, const char c);
uint32_t  string_len(const string_t* str);
string_t* string_substr(const string_t* str, uint32_t start, uint32_t len);
char*     cstr_of_string(const string_t* str);
string_t* string_of_cstr(const char* cstr);
string_t* string_strip(const string_t* str);
char*     string_stringify(string_t* str);
void      debug_string(const string_t* str);
