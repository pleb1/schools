// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Quang Trang Nguyen <xnguye11@stud.fit.vutbr.cz>
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
// Tomas Pribyl <xpriby17@stud.fit.vutbr.cz>
#include "codegen.h"
#include "symtable.h"
#include "error.h"
#include "lexer.h"
#include "str.h"

#include <stdio.h>

// builtin instructions
static const char* builtin_length_code = "LABEL length       # Define function\n"
                                         "CREATEFRAME        # Create TF\n"
                                         "PUSHFRAME          # Make it LF\n"
                                         "DEFVAR LF@ret      # Return value\n"
                                         "DEFVAR LF@0        # Create temp var\n"
                                         "POPS LF@0          # Get argument\n"
                                         "STRLEN LF@ret LF@0 # strlen [arg] -> ret\n"
                                         "POPFRAME           # Pop LF\n"
                                         "RETURN             # Return value\n"
                                         "\n";
// TODO: :(
static const char* builtin_ord_code = "LABEL ord\n"
                                      "CREATEFRAME\n"
                                      "PUSHFRAME\n"
                                      "DEFVAR LF@ret\n"
                                      "DEFVAR LF@0\n"
                                      "DEFVAR LF@1\n"
                                      "DEFVAR LF@tst\n"
                                      "POPS LF@1                          # [s, i] -> i\n"
                                      "POPS LF@0                          # [s]    -> s\n"
                                      "LT LF@tst LF@1 int@0\n"
                                      "JUMPIFEQ ord_bad LF@tst bool@true\n"
                                      "DEFVAR LF@sl                       # string length\n"
                                      "PUSHS LF@0\n"
                                      "CALL length\n"
                                      "MOVE LF@sl TF@ret                  # save strlen to sl\n"
                                      "LT LF@tst LF@1 LF@sl               # Test if strlen <= arg\n"
                                      "JUMPIFEQ LF@tst bool@true\n"
                                      "EQ LF@tst LF@1 LF@sl\n"
                                      "JUMPIFEQ ord_ok LF@tst bool@true\n"
                                      "MOVE LF@ret nil@nil                # If not, return nil\n"
                                      "RETURN\n"
                                      "LABEL ord_ok\n"
                                      "STRI2INT LF@ret LF@0 LF@1\n"
                                      "RETURN\n"
                                      "LABEL ord_bad\n"
                                      "MOVE LF@ret nil@nil\n"
                                      "RETURN\n"
                                      "\n";
// TODO:
static const char* builtin_substr_code = "LABEL substr\n"
                                         "CREATEFRAME\n"
                                         "DEFVAR LF@RET\n"
                                         "MOVE LF@RET string@\n"
                                         "PUSHFRAME\n"
                                         "DEFVAR LF@S\n"
                                         "DEFVAR LF@I\n"
                                         "DEFVAR LF@N\n"
                                         "DEFVAR LF@T\n"
                                         "DEFVAR LF@TE\n"
                                         "DEFVAR LF@TEM\n"
                                         /*"PUSHS string@ahojlidi" use pushs i n s before calling
                                         "PUSHS int@4"
                                         "PUSHS int@8"*/
                                         "POPS LF@N\n"
                                         "POPS LF@I\n"
                                         "POPS LF@S\n"
                                         "STRLEN LF@TE LF@S\n"
                                         "GT LF@T LF@I LF@TE\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "LT LF@T LF@N int@0\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "LT LF@T LF@I int@0\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "GT LF@T LF@I LF@TE\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "GT LF@T LF@N LF@TE\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "GT LF@T LF@N LF@TE\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "GT LF@T LF@I LF@TE\n"
                                         "JUMPIFEQ ERR LF@T bool@true\n"
                                         "LABEL TEST\n"
                                         "LT LF@T LF@I LF@N\n"
                                         "JUMPIFEQ FOR LF@T bool@true\n"
                                         "JUMP END\n"
                                         "LABEL FOR\n"
                                         "GETCHAR LF@TEM LF@S LF@I\n"
                                         "CONCAT LF@RET LF@RET LF@TEM\n"
                                         "ADD LF@I LF@I int@1\n"
                                         "JUMP TEST\n"
                                         "LABEL ERR\n"
                                         "MOVE LF@RET nil@nil\n"
                                         "CLEARS\n"
                                         "RETURN\n"
                                         "LABEL END\n"
                                         "POPFRAME\n"
                                         "WRITE LF@RET\n"
                                         "CLEARS\n"
                                         "RETURN\n";

static const char* builtin_chr_code = "LABEL chr       # Define function\n"
                                      "CREATEFRAME        # Create TF\n"
                                      "PUSHFRAME          # Make it LF\n"
                                      "DEFVAR LF@ret      # Return value\n"
                                      "DEFVAR LF@0        # Create temp var\n"
                                      "DEFVAR LF@tst\n"
                                      "POPS LF@0          # Get argument\n"

                                      "GT LF@tst LF@0 255    #2 testz na mensi a vetsi\n"
                                      "JUMPIFEQ ord_bad LF@tst bool@true\n"
                                      "LT LF@tst LF@0 0\n"
                                      "JUMPIFEQ ord_bad LF@tst bool@true\n"

                                      "INT2CHAR LF@ret LF@0 # prevod \n"
                                      "POPFRAME           # Pop LF\n"
                                      "RETURN             # Return value\n"
                                      "\n"

                                      "LABEL ord_bad   #pokud spatne cislo\n"
                                      "MOVE LF@ret nil@nil\n"
                                      "RETURN\n"
                                      "\n";

static const char* builtin_inputs_code = "LABEL inputs       # Define function\n"
                                         "CREATEFRAME        # Create TF\n"
                                         "PUSHFRAME          # Make it LF\n"
                                         "DEFVAR LF@ret\n"

                                         "READ LF@ret string  # cteni \n"
                                         "POPFRAME           # Pop LF\n"
                                         "RETURN             # Return value\n"
                                         "\n";

static const char* builtin_inputi_code = "LABEL inputi       # Define function\n"
                                         "CREATEFRAME        # Create TF\n"
                                         "PUSHFRAME          # Make it LF\n"
                                         "DEFVAR LF@ret\n"

                                         "READ LF@ret int  # cteni \n"
                                         "POPFRAME           # Pop LF\n"
                                         "RETURN             # Return value\n"
                                         "\n";
static const char* builtin_inputf_code = "LABEL inputf       # Define function\n"
                                         "CREATEFRAME        # Create TF\n"
                                         "PUSHFRAME          # Make it LF\n"
                                         "DEFVAR LF@ret\n"

                                         "READ LF@ret float  # cteni \n"
                                         "POPFRAME           # Pop LF\n"
                                         "RETURN             # Return value\n"
                                         "\n";
static const char* builtin_print_code = "LABEL print       # Define function\n"
                                        "CREATEFRAME        # Create TF\n"
                                        "PUSHFRAME          # Make it LF\n"
                                        "DEFVAR LF@0\n"

                                        "LABEL dalsi\n"
                                        "POPS LF@0          # Get argument\n"
                                        "JUMPIFEQ konec LF@0 nil #pokud dosly argumenty skocime na konec\n"
                                        "WRITE LF@0         # vypsat argument\n"
                                        "JUMP dalsi # jsou tu dalsi argumenty, tak vse opakujeme \n"

                                        "LABEL konec\n"
                                        "POPFRAME           # Pop LF\n"
                                        "RETURN        # Return value\n"
                                        "\n";

static int current_loop = 0;
static int current_branch = 0;

void gen_builtins(void)
{
    fprintf(stdout, "# Builtin functions\n");
    fprintf(stdout, "%s\n", builtin_length_code);
    fprintf(stdout, "%s\n", builtin_ord_code);
    fprintf(stdout, "%s\n", builtin_chr_code);
    fprintf(stdout, "%s\n", builtin_inputf_code);
    fprintf(stdout, "%s\n", builtin_inputi_code);
    fprintf(stdout, "%s\n", builtin_inputs_code);
    fprintf(stdout, "%s\n", builtin_substr_code);
    fprintf(stdout, "%s\n", builtin_print_code);
    fprintf(stdout, "# End of builtin functions\n\n");
}

void gen_main_begin(void)
{
    fprintf(stdout, "LABEL $main\n");
    fprintf(stdout, "CREATEFRAME\n");
    fprintf(stdout, "DEFVAR TF@ret\n");
    fprintf(stdout, "PUSHFRAME\n");
}

void gen_main_end(void)
{
    fprintf(stdout, "LABEL $main%%return\n");
    fprintf(stdout, "POPFRAME\n");
    fprintf(stdout, "CLEARS\n");
    fprintf(stdout, "MOVE GF@ret TF@ret\n");
    fprintf(stdout, "RETURN\n");
    fprintf(stdout, "WRITE GF@ret            # Print result of computation\n");
}

void gen_init(void)
{
    fprintf(stdout, ".IFJcode18\n");
    fprintf(stdout, "DEFVAR GF@%%ret\n");
    fprintf(stdout, "JUMP $main\n");
    gen_builtins();
}

void gen_save_expr_res(void)
{
    fprintf(stdout, "MOVE LF@ret LF@RES\n");
}

void gen_save_expr_res_nil(void)
{
    fprintf(stdout, "MOVE LF@ret nil@nil\n");
}

void gen_loop_head(int depth)
{
    if (!depth)
        fprintf(stdout, "LABEL $loop%%%d%%%d\n", current_loop, depth);
    else
        fprintf(stdout, "LABEL $loop%%%d%%%d\n", current_loop, depth);
}

void gen_loop_end(int depth)
{
    fprintf(stdout, "LABEL $loop_end%%%d%%%d\n", current_loop++, depth);
}

void gen_branch_if(int depth)
{
    fprintf(stdout, "JUMPIFEQ $branch_false%%%d%%%d LF@RES bool@false # If condition is false, jump to false branch\n",
            ++current_branch, depth);
}
void gen_branch_else(int depth)
{
    fprintf(stdout, "JUMP $branch_end%%%d%%%d # If we come from the [if] branch, skip the else branch\n",
            current_branch, depth);
    fprintf(stdout, "LABEL $branch_false%%%d%%%d\n", current_branch, depth);
}

void gen_branch_end(int depth)
{
    fprintf(stdout, "LABEL $branch_end%%%d%%%d\n", current_branch++, depth);
}

void gen_func_head(const char* n)
{
    fprintf(stdout, "LABEL %s             # Function '%s'\n", n, n);
    fprintf(stdout, "PUSHFRAME\n");
    fprintf(stdout, "DEFVAR LF@ret        # Define return value\n");
    fprintf(stdout, "MOVE LF@ret nil@nil  # Default return value is nil\n");
}

void gen_func_end(const char* n)
{
    fprintf(stdout, "LABEL $%s%%return\n", n); // Create return label so we can jump to it from inside the function
    // TODO: ?
    fprintf(stdout, "POPFRAME\n");
    fprintf(stdout, "RETURN               # End of function '%s'\n\n", n);
}

void gen_func_add_param(const char* n, int i)
{
    fprintf(stdout, "DEFVAR LF@%s\n", n);
    fprintf(stdout, "MOVE LF@%s LF@arg%%%d\n", n, i);
}

void gen_funcall_head(void)
{
    fprintf(stdout, "CREATEFRAME           # Create TF for arguments\n");
}

void gen_funcall(const char* n)
{
    fprintf(stdout, "CALL %s               # Call function %s\n", n, n);
    fprintf(stdout, "MOVE LF@ret TF@ret\n");
    fprintf(stdout, "POPFRAME\n");
}

void gen_funcall_add_arg(int i, token_t v)
{
    fprintf(stdout, "DEFVAR TF@arg%%%d\n", i);
    fprintf(stdout, "MOVE TF@arg%%%d ", i);
    gen_val_const(v);
    fprintf(stdout, "\n");

    fprintf(stdout, "PUSHS ");
    gen_val_const(v);
    fprintf(stdout, "\n");
}

void gen_return_val(const char* fun_name)
{
    fprintf(stdout, "MOVE LF@ret LF@RES ");
    fprintf(stdout, "\n");
    fprintf(stdout, "JUMP $%s%%return\n", fun_name);
}

void gen_val_const(token_t t)
{
    switch (t.type) {
    case Integer:
        fprintf(stdout, "int@%d", t.integer);
        break;
    case String:
        fprintf(stdout, "string@%s", cstr_of_string(t.string));
        break;
    case Real:
        fprintf(stdout, "float@%a", t.real);
        break;
    case Id: // TODO: ?
        fprintf(stdout, "LF@%s", cstr_of_string(t.string));
        break;
    case KwNil:
        fprintf(stdout, "nil@nil");
        break;
    default:
        fprintf(stderr, "codegen error\n");
        exit(InternalError);
        break;
    }
}

void gen_defvar_lf(const char* name)
{
    fprintf(stdout, "DEFVAR LF@%s\n", name);
}
