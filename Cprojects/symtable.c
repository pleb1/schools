// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// xnguye11 <xnguye11@stud.fit.vutbr.cz>
#include "symtable.h"
#include "str.h"
#include "error.h"
#include "lexer.h"
int* err;

typedef enum { TypeNIl, TypeInteger, TypeString } type;

int hashCode(char* key)
{
    int retval = 1;
    int keylen = strlen(key);
    for (int i = 0; i < keylen; i++)
        retval += key[i];
    return (retval % HTSIZE);
}

int htInit(tHTable** ptrht)
{
    *ptrht = (tHTable*)calloc(1, sizeof(tHTable[HTSIZE]));
    return true;
}

tHTItem* htSearch(tHTable* ptrht, char* key)
{
    if (ptrht == NULL) // tabulka je prazdna neni co hledat koncim
    {
        return 0;
    }
    int      i = hashCode(key);
    tHTItem* pom = (*ptrht)[i];

    while (pom) // prochazim celou tabulku
    {
        if (!strcmp(pom->key, key))
            return pom;
        pom = pom->ptrnext;
    }
    return 0; // takovy key v tabulce neni vracim 0
}

tHTItem* htInsert(tHTable* ptrht, char* key, hData data)
{
    if (!ptrht)
        return 0;
    tHTItem* pom = htSearch(ptrht, key); // hledam v tabulce jestli existuje polazka s takovym klicem
    if (pom != NULL) {
        pom->data = data;
    } else {
        pom = (tHTItem*)malloc(sizeof(tHTItem));
        if (!pom)
            return false; // nepodarilo se alokace

        int key_len = strlen(key); // delka slova
        int i = hashCode(key);     // index
        pom->key = (char*)malloc(key_len + 1);
        if (!pom->key) {
            free(pom);
        } else {
            tHTItem* first = (*ptrht)[i];
            strcpy(pom->key, key); // pridam klice
            pom->data = data;      // pridam data
            pom->ptrnext = first;  // prevazem tab
            pom->localTS = NULL;
            (*ptrht)[i] = pom;
        }
    }
    return pom;
}

int insertFce(char* fce, tHTable* table) // vlozeni fce
{
    char* pom;
    pom = (char*)malloc(sizeof(char) * (strlen(fce) + 1));
    if (pom == NULL) {
        //*err = InternalError;
        return 0;
    }
    int d = strlen(fce);
    for (int i = 0; i <= d; i++) {
        pom[i] = fce[i];
    }
    pom[d + 1] = '\0';

    hData data;
    data.first = NULL;
    data.fce = true;
    data.navesti = pom;
    data.par_cnt = 0;
    data.type = TypeNIl;
    data.define = false;
    if (!htInsert(table, pom, data)) {
        //*err = InternalError;
        return 0;
    }
    tHTItem* pom1;
    pom1 = htSearch(table, pom);
    if (pom1 == NULL) {
        //*err = InternalError; // error alloc
        return 0;
    }
    pom1->data.define = true;
    pom1->localTS = (tHTable*)malloc(sizeof(tHTable)); // vytvoreni localts
    if (pom1->localTS == NULL) {
        //*err = InternalError; // error alloc
        return 0;
    }
    if (!htInit(pom1->localTS)) {
        //*err = InternalError; // error alloc
        return 0;
    }
    return 1;
}

int insertPar(tHTItem* item, char* name) // vlozeni param
{
    params* pom = (params*)malloc(sizeof(params));
    if (!pom)
        return 0;
    pom->name = name;
    // pom->type = type;
    if (!item->data.first) {
        pom->poradi = 0;
        item->data.first = pom;
        item->data.par_cnt = 1;
    } else {
        params* pom1 = item->data.first;
        int     por = 1;
        while (pom1->next) {
            por++;
            pom1 = pom1->next;
        }
        pom1->next = pom;
        pom->poradi = por;
        item->data.par_cnt++;
    }

    return 1;
}

int insert_fce_type(int type, char* fce, tHTable* table) // vlozeni fce type
{
    tHTItem* pom = htSearch(table, fce);
    if (!pom)
        return 0;
    else {
        pom->data.type = type;
        return 1;
    }
}

int insert_par_fce(char* par, char* fcename, tHTable* table) // vlozeni param do fce
{
    tHTItem* pom = htSearch(table, fcename);
    htInsert(pom->localTS, par, (hData){ .type = KwNil, .navesti = par, 0 });

    if (!insertPar(pom, par))
        return 0; // InternalError;
    return 1;
}
int check_undefinedId(tHTItem* item, char* Idname) // check if id is defined
{
    if (!htSearch(item->localTS, Idname))
        return 0;
    return 1;
}
