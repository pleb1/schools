// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// xnguye11 <xnguye11@stud.fit.vutbr.cz>
#pragma once
#define STACK_SIZE 50

#include "lexer.h"
#include "symtable.h"

typedef struct {
    token_t tok[STACK_SIZE]; // token
    int     value;
    int     top;
} tStack;

int     stackInit(tStack* s);
int     stackEmpty(tStack* s);
int     stackPush(tStack* s, token_t tok);
token_t stackPop(tStack* s);
int     TopValue(tStack* s);
token_t stacktop(tStack*);