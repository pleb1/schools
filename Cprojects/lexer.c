// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
#include "lexer.h"
#include "str.h"

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef enum {
    StateInit,
    StateNewline,
    StateBC0,
    StateBC1,
    StateBC2,
    StateBC3,
    StateBC4,
    StateBC5,
    StateBlockComment,
    StateBCE0,
    StateBCE1,
    StateBCE2,
    StateBCE3,
    StateBCE4,
    StateBlockCommentEnd,
    StateBlockCommentEndDirect,
    StateEof,
    StateMore,
    StateMoreEq,
    StateLess,
    StateLessEq,
    StateNot,
    StateNotEq,
    StateLineComment,
    StateAssign,
    StateEq,
    StateId,
    StateStringOpen,
    StateString,
    StateInt,
    StateRealDec,
    StateRealDecSep,
    StateRealExp,
    StateRealNeg,
    StateRealNeg0,
    StateRealPos,
    StateRealPos0,
    StateLPar,
    StateRPar,
    StateComma,
    StateAdd,
    StateSub,
    StateMul,
    StateDiv,
    StateAt,
    StateAnd,
    StatePipe,
    StateOr,
    StateMutation,
    StateBool,
    StateStringEsc,
    StateStringEscHex0,
    StateStringEscHex1,
} LexState;

typedef struct lexer_state {
    uint32_t line;
    uint32_t col;
    LexState state;
    FILE*    input_file;
} lexer_state;

static const char* keywords[] = {
    [KwIf] = "if",       [KwElse] = "else", [KwThen] = "then", [KwEnd] = "end", [KwDef] = "def",
    [KwWhile] = "while", [KwDo] = "do",     [KwNot] = "not",   [KwNil] = "nil",
};

static int int_of_string(const string_t* str)
{
    return atoi(cstr_of_string(str)); // safe because [str] can only contain
                                      // digits with whitespace at last position
}

static int is_space(const int c)
{
    if (c == '\n')
        return 0;
    return isspace(c);
}

static int is_lowercase(const int c)
{
    return (c >= 'a' && c <= 'z');
}

lexer_state* lexer_init(const char* input)
{
    lexer_state* lex = (lexer_state*)malloc(sizeof(lexer_state));
    if (!lex) {
        fprintf(stderr, "[LEXER]: Cannot initialize\n");
        return NULL;
    }
    if (!input) {
        lex->input_file = stdin;
    } else if (!(lex->input_file = fopen(input, "r"))) {
        fprintf(stderr, "Cannot open file '%s'\n", input);
        return 0;
    }
    lex->line = lex->col = 0;
    lex->state = StateInit;

    return lex;
}

void lexer_close(lexer_state* lex)
{
    fclose(lex->input_file);
    free(lex);
    lex = NULL;
}

token_t get_token(lexer_state* lex)
{
    token_t   tok = { 0 };
    string_t* buf = string_new();
    int       c = 0;
    while (is_space((c = fgetc(lex->input_file)))) {
    } // Init --> Init [whitespace]
    lex->state = StateInit;
    while (1) {
        switch (lex->state) {
        case StateInit:
            if (c == '\n')
                lex->state = StateNewline;
            else if ((ftell(lex->input_file) == 1) && c == '=') // If = at beginning of file, try comment
                lex->state = StateBC0;
            else if (c == EOF)
                lex->state = StateEof;
            else if (c == '<')
                lex->state = StateLess;
            else if (c == '>')
                lex->state = StateMore;
            else if (c == '!')
                lex->state = StateNot;
            else if (c == '#')
                lex->state = StateLineComment;
            else if (c == '=')
                lex->state = StateAssign;
            else if (c == '"')
                lex->state = StateStringOpen;
            else if (c == '(')
                lex->state = StateLPar;
            else if (c == ')')
                lex->state = StateRPar;
            else if (isdigit(c))
                lex->state = StateInt;
            else if (c == ',')
                lex->state = StateComma;
            else if (c == '+')
                lex->state = StateAdd;
            else if (c == '-')
                lex->state = StateSub;
            else if (c == '*')
                lex->state = StateMul;
            else if (c == '/')
                lex->state = StateDiv;
            else if (c == '&')
                lex->state = StateAt;
            else if (c == '|')
                lex->state = StatePipe;
            else if (c == '?')
                lex->state = StateBool;
            else if (c == '_' || is_lowercase(c))
                lex->state = StateId;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c' at init\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateMutation:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpMut;
            return tok;
            break;
        case StateBool:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpBool;
            return tok;
            break;
        case StateAdd:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpAdd;
            return tok;
            break;
        case StateSub:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpSub;
            return tok;
            break;
        case StateMul:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpMul;
            return tok;
            break;
        case StateDiv:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpDiv;
            return tok;
            break;
        case StateAt:
            if (c == '&')
                lex->state = StateAnd;
            else {
                fprintf(stderr, "[LEXER] Illegal character\n");
                string_free(buf);
                return tok;
            }
            break;
        case StatePipe:
            if (c == '|')
                lex->state = StateOr;
            else {
                fprintf(stderr, "[LEXER] Illegal character\n");
                string_free(buf);
                return tok;
            }
            break;
        case StateAnd:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpAnd;
            return tok;
            break;
        case StateOr:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpOr;
            return tok;
            break;
        case StateComma:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = Comma;
            return tok;
            break;
        case StateNewline:
            if (c == '=')
                lex->state = StateBC0;
            else {
                ungetc(c, lex->input_file); // previous character was terminating -> return current character
                string_free(buf);           // cleanup
                tok.type = Eol;
                return tok;
            }
            break;
        case StateEof:
            ungetc(c, lex->input_file); // previous c was terminating -> return current c
            string_free(buf);
            tok.type = Eof;
            return tok;
            break;
        case StateLPar:
            ungetc(c, lex->input_file); // all terminating states must return last character
            string_free(buf);
            tok.type = ParenLeft;
            return tok;
            break;
        case StateRPar:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = ParenRight;
            return tok;
            break;
        case StateMore:
            if (c == '=')
                lex->state = StateMoreEq;
            else {
                ungetc(c, lex->input_file);
                string_free(buf);
                tok.type = OpMore;
                return tok;
            }
            break;
        case StateMoreEq:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpEqMore;
            return tok;
            break;
        case StateLess:
            if (c == '=')
                lex->state = StateLessEq;
            else {
                ungetc(c, lex->input_file);
                string_free(buf);
                tok.type = OpLess;
                return tok;
            }
            break;
        case StateLessEq:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpEqLess;
            return tok;
            break;
        case StateNot:
            if (c == '=')
                lex->state = StateNotEq;
            else {
                ungetc(c, lex->input_file);
                string_free(buf);
                tok.type = OpMut;
                return tok;
            }
            break;
        case StateNotEq:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpNotEq;
            return tok;
            break;
        case StateLineComment:
            if (c == '\n' || c == EOF) {
                string_free(buf);
                tok.type = Eol;
                return tok;
            }
            break;
        case StateStringOpen:
            if (c == '\\')
                lex->state = StateStringEsc;
            else if (c == '"')
                lex->state = StateString;
            else if (c <= 31) {
                fprintf(stderr, "LEXER: string can't contain non-printable ASCII, use escape sequences\n");
                return tok;
            }
            break;
        case StateStringEsc:
            if (c == 'x')
                lex->state = StateStringEscHex0;
            else if (c == 'n' || c == 't' || c == 's' || c == '"' || c == '\\')
                lex->state = StateStringOpen;
            else {
                fprintf(stderr, "LEXER: illegal escape sequence\n");
                return tok;
            }
            break;
        case StateStringEscHex0:
            if ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9'))
                lex->state = StateStringEscHex1;
            else {
                fprintf(stderr, "LEXER: illegal escape sequence - not in hex\n");
                return tok;
            }
            break;
        case StateStringEscHex1:
            if ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9'))
                lex->state = StateStringOpen;
            else {
                fprintf(stderr, "LEXER: illegal escape sequence - not in hex\n");
                return tok;
            }
            break;
        case StateString:
            ungetc(c, lex->input_file); // terminating state -> return last c
            tok.type = String;
            tok.string = string_strip(buf);
            string_free(buf);             // cleanup
            string_stringify(tok.string); // TODO: rm
            return tok;
            break;
        case StateId:
            if (c == '_' || isdigit(c) || isalpha(c))
                lex->state = StateId;
            else {
                ungetc(c, lex->input_file);
                tok.type = Id;
                for (uint32_t i = KwIf; i <= KwNil; ++i) {
                    if (!string_cmp_cstr(buf, keywords[i])) {
                        tok.type = i;
                        string_free(buf);
                        return tok;
                    }
                }
                tok.string = string_new();
                string_copy(buf, tok.string);
                string_free(buf);
                return tok;
            }
            break;
        case StateInt:
            if (isdigit(c))
                lex->state = StateInt;
            else if (c == '.')
                lex->state = StateRealDecSep;
            else if (c == 'e' || c == 'E')
                lex->state = StateRealExp;
            else if (isalpha(c)) {
                fprintf(stderr, "[LEXER] Illegal character '%c' in integer '%s'\n", c, cstr_of_string(buf));
                string_free(buf);
                return tok;
            } else if (!isdigit(c)) {
                ungetc(c, lex->input_file);
                tok.type = Integer;
                tok.integer = int_of_string(buf);
                string_free(buf);
                return tok;
            } else {
                fprintf(stderr, "[LEXER] Illegal character '%c' in integer\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealDecSep:
            if (isdigit(c))
                lex->state = StateRealDec;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c' after decimal separator\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealDec:
            if (c == 'e' || c == 'E')
                lex->state = StateRealExp;
            else if (!isdigit(c)) {
                ungetc(c, lex->input_file);
                tok.type = Real;
                tok.real = strtod(cstr_of_string(buf), NULL);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealExp:
            if (c == '+')
                lex->state = StateRealPos0;
            else if (isdigit(c))
                lex->state = StateRealPos;
            else if (c == '-')
                lex->state = StateRealNeg0;
            else {
                fprintf(stderr, "[LEXER] Illegal character in real '%c'", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealPos0:
            if (isdigit(c))
                lex->state = StateRealPos;
            else {
                fprintf(stderr, "[LEXER] Illegal character after exponent in real '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealPos:
            if (isdigit(c))
                lex->state = StateRealPos;
            else if (isalpha(c)) {
                fprintf(stderr, "[LEXER] Illegal character in real '%c'\n", c);
                string_free(buf);
                return tok;
            } else {
                ungetc(c, lex->input_file);
                tok.type = Real;
                tok.real = strtod(cstr_of_string(buf), NULL);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealNeg0:
            if (isdigit(c))
                lex->state = StateRealNeg;
            else {
                fprintf(stderr, "[LEXER] Illegal character after exponent in real '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateRealNeg:
            if (isdigit(c))
                lex->state = StateRealPos;
            else if (isalpha(c)) {
                fprintf(stderr, "[LEXER] Illegal character in real '%c'\n", c);
                string_free(buf);
                return tok;
            } else {
                ungetc(c, lex->input_file);
                tok.type = Real;
                tok.real = strtod(cstr_of_string(buf), NULL);
                string_free(buf);
                return tok;
            }
            break;
        case StateAssign:
            if (c == '=')
                lex->state = StateEq;
            else {
                ungetc(c, lex->input_file);
                string_free(buf);
                tok.type = OpAssign;
                return tok;
            }
            break;
        case StateEq:
            ungetc(c, lex->input_file);
            string_free(buf);
            tok.type = OpEq;
            return tok;
            break;
        case StateBC0:
            if (c == 'b')
                lex->state = StateBC1;
            else {
                fprintf(stderr, "[LEXER] Illegal character\n");
                string_free(buf);
                return tok;
            }
            break;
        case StateBC1:
            if (c == 'e')
                lex->state = StateBC2;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateBC2:
            if (c == 'g')
                lex->state = StateBC3;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateBC3:
            if (c == 'i')
                lex->state = StateBC4;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateBC4:
            if (c == 'n')
                lex->state = StateBC5;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateBC5:
            if (isspace(c))
                lex->state = StateBlockComment;
            else {
                fprintf(stderr, "[LEXER] Illegal character '%c'\n", c);
                string_free(buf);
                return tok;
            }
            break;
        case StateBlockComment:
            if (c == '\n')
                lex->state = StateBCE0;
            break;
        case StateBCE0:
            if (c == '=')
                lex->state = StateBCE1;
            else if (c == '\n')
                lex->state = StateBCE0;
            else
                lex->state = StateBlockComment;
            break;
        case StateBCE1:
            if (c == 'e')
                lex->state = StateBCE2;
            else if (c == '\n')
                lex->state = StateBCE0;
            else
                lex->state = StateBlockComment;
            break;
        case StateBCE2:
            if (c == 'n')
                lex->state = StateBCE3;
            else if (c == '\n')
                lex->state = StateBCE0;
            else
                lex->state = StateBlockComment;
            break;
        case StateBCE3:
            if (c == 'd')
                lex->state = StateBCE4;
            else
                lex->state = StateBlockComment;
            break;
        case StateBCE4:
            if (is_space(c))
                lex->state = StateBlockCommentEnd;
            else if (c == '\n')
                lex->state = StateBlockCommentEndDirect;
            else
                lex->state = StateBlockComment;
            break;
        case StateBlockCommentEnd:
            if (c == '\n' || c == EOF) {
                string_free(buf);
                tok.type = Eol;
                return tok;
            }
            break;
        case StateBlockCommentEndDirect:
            string_free(buf);
            tok.type = Eol;
            return tok;
            break;
        }
        string_append(buf, c);
        c = fgetc(lex->input_file);
    }
}

static const char* token_debug_str[] = { [Id] = "Id",
                                         [KwIf] = "If",
                                         [KwElse] = "Else",
                                         [KwThen] = "Then",
                                         [KwEnd] = "End",
                                         [KwDef] = "Def",
                                         [KwWhile] = "While",
                                         [KwDo] = "Do",
                                         [KwNot] = "Not",
                                         [KwNil] = "Nil",
                                         [OpAdd] = "Add",
                                         [OpSub] = "Sub",
                                         [OpMul] = "Mul",
                                         [OpDiv] = "Div",
                                         [OpEq] = "Eq",
                                         [OpNotEq] = "Neq",
                                         [OpEqMore] = "EqMore",
                                         [OpEqLess] = "EqLess",
                                         [OpLess] = "Less",
                                         [OpMore] = "More",
                                         [OpAnd] = "And",
                                         [OpOr] = "Or",
                                         [OpAssign] = "Assign",
                                         [OpMut] = "Mut",
                                         [OpBool] = "Bool",
                                         [ParenLeft] = "ParenLeft",
                                         [ParenRight] = "ParenRight",
                                         [Comma] = "Comma",
                                         [Integer] = "Integer",
                                         [String] = "String",
                                         [Real] = "Real",
                                         [Eol] = "Eol",
                                         [Eof] = "Eof" };
void               debug_token(token_t t)
{
    fprintf(stderr, "|%s|\n", token_debug_str[t.type]);
}

const char* debug_token_name(token_t t)
{
    return token_debug_str[t.type];
}