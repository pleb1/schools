// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
#pragma once

typedef enum {
    InternalError = 99,
    LexerError = 1,
    SyntaxError = 2,
    SemanticErrorDefinition = 3,
    SemanticErrorType = 4,
    SemanticErrorArguments = 5,
    SemanticError = 6,
    DivisionByZero = 9,
} ErrorCode;
