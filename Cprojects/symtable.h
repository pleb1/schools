// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// xnguye11 <xnguye11@stud.fit.vutbr.cz>
#pragma once

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define HTSIZE 30

typedef struct params {
    // int         type;   // typ parametru
    char*          name;   // ukazatel na id
    int            poradi; // poradi parametru
    struct params* next;   // dalsi parametr
} params;

typedef struct unpom { // undefined id or fce
    int           count;
    bool          fce;
    char*         fcename;
    char*         Idname;
    void*         Localts;
    struct unpom* next;
} unpom;

typedef struct hData {
    unsigned int type;    // type of id or fce
    char*        navesti; // fce name
    bool         fce;     // pokud je to funkce=true
    bool         define;  // implicit false
    int          par_cnt; // pocet param
    params*      first;   //
} hData;

typedef struct tHTItem {
    char*           key;     /* klíč = id*/
    hData           data;    /* obsah */
    struct tHTItem* ptrnext; /* ukazatel na další synonymum */
    void*           localTS; // ukazatel na localts
} tHTItem;

typedef tHTItem* tHTable[HTSIZE];

int      htInit(tHTable** ptrht);
tHTItem* htSearch(tHTable* ptrht, char* key);
tHTItem* htInsert(tHTable* ptrht, char* key, hData data);
int      insertPar(tHTItem* item, char* name);
int      searchParam(tHTItem* ptr, char* name);
int      insertFce(char* fce, tHTable* table);
int      insert_par_fce(char* par, char* fcename, tHTable* table);
int      insert_fce_type(int type, char* fce, tHTable* table);
int      check_undifinedId(tHTItem* item, char* Idname);
// tHTItem*         search_fce_param(tHTable* ptrht, char* key);
