// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// xguzik00 <xguzik00@stud.fit.vutbr.cz>
#include "parser.h"
#include "symtable.h"
#include "error.h"
#include "psa.h"
#include "codegen.h"

#include <stdio.h>

// poo
#ifdef DEBUG
#define debug fprintf
#else
#define debug (void)
#endif

#define error_msg fprintf

#define should_gen_code ((!passed && (flags & InFunction)) || (passed && !(flags & InFunction)))

#define is_term(x) (((x) == Integer) || ((x) == Real) || ((x) == String) || ((x) == Id) || ((x) == KwNil))

// linked list node
typedef struct token_item_t {
    struct token_item_t* next;
    token_t              data;
} token_item_t;

typedef enum {
    InFunction = 0x01,
    InRVal = 0x02,
} ParserFlag;

lexer_state*         lex;                    // lexer state - useless remnant of first arch sketch
static token_item_t* toklist_hd = NULL;      // head of token list
static token_item_t* toklist_current = NULL; // current position in token list
int*                 err;                    // :(
tHTable*             table_global;           // global symtable
uint8_t              flags = 0;              // flags
tHTItem*             fun_record;             // pointer to function entry in symtable, set when InFunction
int                  arg_cnt = 0;            // how many arguments we parsed, used to check if missing/too many
int                  passed = 0;             // set after first pass of parsing
int                  depth = 0;              // for numbering nested labels

// gets next token in tokenlist
token_t next_token(void)
{
    if (!toklist_hd) { // tokenlist is empty -> initialize it
        token_item_t* ti = (token_item_t*)malloc(sizeof(token_item_t));
        ti->next = NULL;
        ti->data = get_token(lex);
        toklist_hd = ti;
        return ti->data;
    } else if (!toklist_current) { // we're at the end of tokenlist -> append
        toklist_current = toklist_hd;
        return toklist_current->data;
    } else if (toklist_current->next) { // we're not at the end of list -> move to the next token
        toklist_current = toklist_current->next;
        return toklist_current->data;
    }
    // get new token and append it to the list
    token_item_t* ti = (token_item_t*)malloc(sizeof(token_item_t));
    ti->next = NULL;
    ti->data = get_token(lex);
    toklist_current->next = ti;
    toklist_current = ti;
    return ti->data;
}

// reads whole input and creates a tokenlist from it
int get_tokenlist(void)
{
    token_t t = next_token();
    while ((t = next_token()).type != Eof) {
        if (t.type == LexError)
            return 0;
    };
    return 1;
}

// populates symtable with builtin functions
int define_builtins(void)
{
    if (!htInsert(table_global, "inputs", (hData){ .type = String, .navesti = "inputs", .fce = 1, .par_cnt = 0, 0 }))
        return 0;
    if (!htInsert(table_global, "inputi", (hData){ .type = Integer, .navesti = "inputi", .fce = 1, .par_cnt = 0, 0 }))
        return 0;
    if (!htInsert(table_global, "inputf", (hData){ .type = Real, .navesti = "inputf", .fce = 1, .par_cnt = 0, 0 }))
        return 0;
    if (!htInsert(table_global, "length", (hData){ .type = Integer, .navesti = "length", .fce = 1, .par_cnt = 1, 0 }))
        return 0;
    if (!htInsert(table_global, "substr", (hData){ .type = String, .navesti = "substr", .fce = 1, .par_cnt = 3, 0 }))
        return 0;
    if (!htInsert(table_global, "ord", (hData){ .type = Integer, .navesti = "ord", .fce = 1, .par_cnt = 2, 0 }))
        return 0;
    if (!htInsert(table_global, "chr", (hData){ .type = String, .navesti = "chr", .fce = 1, .par_cnt = 1, 0 }))
        return 0;
    if (!htInsert(table_global, "print", (hData){ .navesti = "print", .fce = 1, .par_cnt = -1, 0 }))
        return 0;
    return 1;
}

int parse_Prog(void);
int parse_FunDef(void);
int parse_ParamList(tHTItem* fun);
int parse_ParamList_1(tHTItem* fun);
int parse_Args(tHTItem* fun);
int parse_Args_1(tHTItem* fun);
int parse_RVal(hData* type_out);
int parse_FunCall(hData* type_out);
int parse_Stmt(void);
int parse_Expr(hData* type_out);

int parse(lexer_state* lexer, int* error)
{
    err = error;
    lex = lexer;
    if (!get_tokenlist())
        exit(LexerError);
    toklist_current = toklist_hd;

    htInit(&table_global);  // init global symtable
    if (!define_builtins()) // populate it with builtins
        exit(InternalError);

    gen_init();
    if (parse_Prog()) // First pass - generate functions
        passed = 1;
    gen_main_begin();
    toklist_current = toklist_hd; // Reset toklist to start again
    if (parse_Prog()) {           // Second pass - generate toplevel statements
        gen_main_end();
        return 1;
    }
    return 0;
}

int parse_Prog()
{
    // Prog -> FunDef Prog
    // Prog -> def Id ( ParamList ) eol Stmt end eol Prog
    if (toklist_current->data.type == KwDef) {
        flags |= InFunction;
        if (next_token().type != Id) { // we call next_token() every time we successfully get token
            error_msg(stderr, "syntax error: expected identifier in function definition\n");
            exit(SyntaxError); // Linux is our janitor (puking smiley*10e100)
        }
        token_t fun_id = toklist_current->data;
        if (htSearch(table_global, cstr_of_string(fun_id.string)) && !passed) { // Already defined?
            error_msg(stderr, "symbol already defined\n");
            exit(SemanticErrorDefinition);
        }
        if (!passed && !insertFce(cstr_of_string(fun_id.string), table_global))
            exit(InternalError);
        fun_record = htSearch(table_global, cstr_of_string(fun_id.string));
        if (!passed) {                     // We're here for the first time, so
            fun_record->data.type = KwNil; // return type is nil by default
            htInit(fun_record->localTS);   // and we init local symtable for the func
        }
        if (next_token().type != ParenLeft) {
            error_msg(stderr, "syntax error: expected '(' in function definition\n");
            exit(SyntaxError);
        }
        if (should_gen_code)
            gen_func_head(cstr_of_string(fun_id.string));
        next_token();
        if (!parse_ParamList(fun_record)) {
            error_msg(stderr, "syntax error: expected parameters in function definition\n");
            exit(SyntaxError);
        }
        if ((toklist_current->data.type != ParenRight) || next_token().type != Eol) {
            error_msg(stderr, "syntax error: expected '(' after parameters\n");
            exit(SyntaxError);
        }
        next_token();
        if (!parse_Stmt()) {
            error_msg(stderr, "syntax error: expected statement in function body\n");
            exit(SyntaxError);
        }
        if (toklist_current->data.type != KwEnd || next_token().type != Eol) {
            error_msg(stderr, "syntax error: end to terminate function definition\n");
            exit(SyntaxError);
        }
        debug(stderr, "function definition\n");
        if (should_gen_code)
            gen_func_end(cstr_of_string(fun_id.string));
        flags &= ~InFunction; // no longer in function definition -> unset flag
        fun_record = NULL;    // and reset helper ptr
        next_token();
        return parse_Prog();
    }
    // Prog -> Eol Prog
    else if (toklist_current->data.type == Eol) {
        next_token();
        return parse_Prog();
    }
    // Prog -> Eof
    else if (toklist_current->data.type == Eof) {
        return 1;
    }
    // Prog -> Stmt Prog
    else if (parse_Stmt())
        return parse_Prog();
    return 0;
}

int parse_Stmt()
{
    token_item_t* backtrack = toklist_current;
    // Stmt -> if Expr then Eol Stmt else Eol Stmt end Eol Stmt
    if (toklist_current->data.type == KwIf) {
        next_token();
        if (!parse_Expr(NULL)) {
            error_msg(stderr, "syntax error: expected expression in branch condition\n");
            exit(SyntaxError);
        }
        if ((toklist_current->data.type != KwThen) || (next_token().type != Eol)) {
            error_msg(stderr, "syntax error: expected then after if condition\n");
            exit(SyntaxError);
        }
        if (should_gen_code)
            gen_branch_if(++depth); // Set depth for inner labels
        next_token();
        if (!parse_Stmt()) {
            error_msg(stderr, "syntax error: expected statement in branch\n");
            exit(SyntaxError);
        }
        if ((toklist_current->data.type != KwElse) || (next_token().type != Eol)) {
            error_msg(stderr, "syntax error: expected else\n");
            exit(SyntaxError);
        }
        if (should_gen_code)
            gen_branch_else(depth);
        next_token();
        if (!parse_Stmt()) {
            error_msg(stderr, "syntax error: expected statement in else branch\n");
            exit(SyntaxError);
        }
        if ((toklist_current->data.type != KwEnd) || (next_token().type != Eol)) {
            error_msg(stderr, "syntax error: expected terminator after branch\n");
            exit(SyntaxError);
        }
        if (should_gen_code)
            gen_branch_end(depth--); // Reset depth
        next_token();
        return parse_Stmt();
    }
    // Stmt -> while Expr do Eol Stmt end Eol Stmt
    else if (toklist_current->data.type == KwWhile) {
        next_token();
        if (!parse_Expr(NULL)) {
            error_msg(stderr, "syntax error: expected expression in while condition\n");
            exit(SyntaxError);
        }
        if ((toklist_current->data.type != KwDo) || (next_token().type != Eol)) {
            error_msg(stderr, "syntax error: expected do after while condition\n");
            exit(SyntaxError);
        }
        gen_loop_head(depth++); // Increase depth
        next_token();
        if (!parse_Stmt()) {
            error_msg(stderr, "syntax error: expected statement in while\n");
            exit(SyntaxError);
        }
        if ((toklist_current->data.type != KwEnd) || (next_token().type != Eol)) {
            error_msg(stderr, "syntax error: expected loop terminator\n");
            exit(SyntaxError);
        }
        debug(stderr, "while loop parsed\n");
        depth--;      // Exiting loop -> depth decreased
        next_token(); // TODO:?
        return parse_Stmt();
    }
    // Stmt -> Id = RVal Eol Stmt
    // Stmt -> FunCall Eol Stmt
    // Stmt -> Expr Eol Stmt
    else if (toklist_current->data.type == Id) {
        tHTItem* id;
        if (flags & InFunction) {                                                             // In function definition?
            id = htSearch(fun_record->localTS, cstr_of_string(toklist_current->data.string)); // Look in local table
            if (!id)                                                                          //
                id = htSearch(table_global, cstr_of_string(toklist_current->data.string));    // then global
            if (!id)                                                                          // then insert to local
                id = htInsert(fun_record->localTS, cstr_of_string(toklist_current->data.string),
                              (hData){ .type = KwNil, .navesti = cstr_of_string(toklist_current->data.string) });
            if (!id)
                exit(InternalError);
        } else { // Otherwise search global and insert if not found
            if (!(id = htSearch(table_global, cstr_of_string(toklist_current->data.string))))
                id = htInsert(table_global, cstr_of_string(toklist_current->data.string),
                              (hData){ .type = KwNil, .navesti = cstr_of_string(toklist_current->data.string), 0 });
        }
        token_item_t* save = toklist_current;
        // Stmt -> Id = RVal Eol Stmt
        if (next_token().type == OpAssign) {
            gen_defvar_lf(id->key);
            // Semantic check: can't assign to function
            tHTItem* lval = htSearch(table_global, cstr_of_string(save->data.string));
            if (lval && lval->data.fce) {
                error_msg(stderr, "semantic error: function can't be lval\n");
                exit(SemanticError);
            }
            next_token();
            if (!parse_RVal(&id->data)) {
                error_msg(stderr, "syntax error: expected rvalue after '='\n");
                exit(SyntaxError);
            }
            if (toklist_current->data.type != Eol) {
                error_msg(stderr, "syntax error: expected newline after assignment\n");
                exit(SyntaxError);
            }
            next_token();
            debug(stderr, "assignment\n");
            return parse_Stmt();
        }
        // Stmt -> Expr Eol Stmt | FIRST() = Id
        toklist_current = save;
        hData dummy = { .type = KwNil, 0 }; // Save type of expression
        if (parse_Expr(&dummy)) {
            if (toklist_current->data.type != Eol) {
                error_msg(stderr, "syntax error: expected newline after statement\n");
                exit(SyntaxError);
            }
            if (save->data.type == Id && id->data.type == KwNil) { // Undefined variable or nil
                fprintf(stderr, "semantic error: trying to return undefined variable '%s'\n",
                        cstr_of_string(save->data.string));
                exit(SemanticErrorDefinition);
            }
            next_token();
            if ((flags & InFunction) && fun_record->data.type == KwNil)        // first time returning from function
                fun_record->data.type = id->data.type;                         // so we set it appropriately
            else if ((flags & InFunction) && fun_record->data.type != KwNil) { // returning wrong type
                if (fun_record->data.type != id->data.type) {
                    error_msg(stderr, "semantic error: function '%s' can't have multiple return types\n",
                              fun_record->key);
                    exit(SemanticErrorType);
                }
            }
            debug(stderr, "expression statement\n");
            return parse_Stmt();
        }
        // Stmt -> FunCall Eol Stmt
        toklist_current = save;
        if (parse_FunCall(&id->data)) {
            // Stmt -> FunCall Eol Stmt
            if (toklist_current->data.type != Eol) {
                error_msg(stderr, "syntax error: expected newline after function call\n");
                exit(SyntaxError);
            }
            next_token();
            return parse_Stmt();
        }
    }
    // Stmt -> Expr Eol Stmt | FIRST() =/= Id
    else if (toklist_current->data.type == Integer || toklist_current->data.type == Real ||
             toklist_current->data.type == String || toklist_current->data.type == KwNil) {
        hData dummy = { .type = KwNil, 0 };
        if (parse_Expr(&dummy) && toklist_current->data.type != Eol) {
            error_msg(stderr, "syntax error: expected newline after statement\n");
            exit(SyntaxError);
        }
        if ((flags & InFunction) && fun_record->data.type == KwNil) // Function type undefined - first time returning
            fun_record->data.type = dummy.type;
        else if ((flags & InFunction) &&
                 fun_record->data.type != KwNil) { // Function type known - check if we're not trying to retype it
            if (fun_record->data.type != dummy.type) {
                error_msg(stderr, "semantic error: function '%s' can't have multiple return types\n", fun_record->key);
                exit(SemanticErrorType);
            }
        }
        next_token();
        if ((should_gen_code) && (flags & InFunction))
            gen_return_val(fun_record->key);
        else if (should_gen_code)
            gen_return_val("main");
        debug(stderr, "expression statement\n");
        return parse_Stmt();
    }
    // Stmt -> \epsilon
    toklist_current = backtrack;
    return 1;
}

int parse_RVal(hData* type_out)
{
    token_item_t* backtrack = toklist_current;
    flags |= InRVal;
    // RVal -> Expr
    if (parse_Expr(type_out)) {
        flags &= ~InRVal;
        type_out->define = 1; // Mark as defined
        return 1;
    }
    toklist_current = backtrack;
    // RVal -> FunCall
    int r = parse_FunCall(type_out);
    flags &= ~InRVal;
    if (r) // Mark as defined
        type_out->define = 1;
    return r;
}

int parse_FunCall(hData* type_out)
{
    token_item_t* backtrack = toklist_current;
    // FunCall -> Id ( Args )
    if (toklist_current->data.type == Id) {
        arg_cnt = 0;
        tHTItem* fun = htSearch(table_global, cstr_of_string(toklist_current->data.string));
        if (!fun) { // Undefined function or symbol
            // TODO: psa? ; Is this a function or a variable? let's find out
            tHTItem* i = htSearch(table_global, cstr_of_string(toklist_current->data.string));
            if (i && !i->data.fce) { // it's in table and not a function -> variable
                error_msg(stderr, "symbol %s not a function\n", cstr_of_string(toklist_current->data.string));
                exit(SyntaxError);
                // return 0;
            }
            if (flags & InFunction) {
                // local scope
                i = htSearch(fun_record->localTS, cstr_of_string(toklist_current->data.string));
                if (i && !i->data.fce) { // found and it's not a function
                    error_msg(stderr, "symbol %s not a function\n", cstr_of_string(toklist_current->data.string));
                    exit(SyntaxError);
                    // return 0;
                }
            }
            error_msg(stderr, "undefined symbol: %s\n", cstr_of_string(toklist_current->data.string));
            exit(SemanticErrorDefinition);
        }
        // RVal -> Id Modifier ( Args )
        if (next_token().type == OpMut || toklist_current->data.type == OpBool) {
            debug(stderr, "modified ");
            if (toklist_current->data.type == OpBool)
                type_out->type = OpBool;
        } else
            toklist_current = backtrack;
        if (!fun->data.fce) { // Trying to call variable
            error_msg(stderr, "trying to call variable: %s\n", cstr_of_string(toklist_current->data.string));
            exit(SemanticError);
        }
        if (should_gen_code) // If we're either in function
                             // definition, or in toplevel
            gen_funcall_head();
        // FunCall -> Id ( Args )
        if (next_token().type == ParenLeft) {
            next_token();
            if (!parse_Args(fun)) {
                error_msg(stderr, "syntax error: expected arguments in function call\n");
                exit(SyntaxError);
            }
            if (toklist_current->data.type != ParenRight) {
                error_msg(stderr, "syntax error: expected ')' in function call\n");
                exit(SyntaxError);
            }
            if (arg_cnt < fun->data.par_cnt) {
                error_msg(stderr, "%s is missing argument(s): expected %d, got %d\n", fun->key, fun->data.par_cnt,
                          arg_cnt);
                exit(SemanticErrorArguments);
            }
            if (arg_cnt > fun->data.par_cnt && fun->data.par_cnt != -1) { // TODO: -1 -> variable params
                error_msg(stderr, "%s was passed too many argument(s): expected %d, got %d\n", fun->key,
                          fun->data.par_cnt, arg_cnt);
                exit(SemanticErrorArguments);
            }
            debug(stderr, "funcall(): %s\n", fun->key);
            if (should_gen_code)
                gen_funcall(fun->key);
            next_token(); // TODO:?
            error_msg(stderr, "Setting type to %d\n", fun->data.type);
            type_out->type = fun->data.type; // Set type for lval
            return 1;
        }
        toklist_current = backtrack;
        // RVal -> Id Args
        if (next_token().type == OpMut || toklist_current->data.type == OpBool) {
            debug(stderr, "modified ");
        } else
            toklist_current = backtrack;
        // FunCall -> Id Args
        debug(stderr, "funcall: %s\n", cstr_of_string(backtrack->data.string));
        next_token();
        int r = parse_Args(fun);
        if (arg_cnt < fun->data.par_cnt) {
            error_msg(stderr, "%s is missing argument(s): expected %d, got %d\n", fun->key, fun->data.par_cnt, arg_cnt);
            exit(SemanticErrorArguments);
        }
        if (arg_cnt > fun->data.par_cnt && fun->data.par_cnt != -1) { // TODO: -1 -> variable params
            error_msg(stderr, "%s was passed too many argument(s): expected %d, got %d\n", fun->key, fun->data.par_cnt,
                      arg_cnt);
            exit(SemanticErrorArguments);
        }
        debug(stderr, "returning token: %s", debug_token_name(toklist_current->data));
        if (r) {
            type_out->type = fun->data.type; // Set type for lval
            if (should_gen_code)
                gen_funcall(fun->key);
        }
        return r;
    }
    return 0;
}

int parse_ParamList(tHTItem* fun)
{
    // ParamList -> Id ParamList'
    if (toklist_current->data.type == Id) {
        token_t p = toklist_current->data;
        // Check if parameter is unique
        if (!passed && htSearch(fun_record->localTS, cstr_of_string(p.string))) {
            error_msg(stderr, "parameters must be unique\n");
            exit(SemanticErrorDefinition);
        }
        // Insert to local symtable
        if (!passed)
            insert_par_fce(cstr_of_string(p.string), fun->key, table_global);
        if (should_gen_code)
            gen_func_add_param(cstr_of_string(toklist_current->data.string), fun_record->data.par_cnt);
        next_token();
        return parse_ParamList_1(fun);
    }
    // ParamList -> \epsilon
    return 1;
}

int parse_ParamList_1(tHTItem* fun)
{
    // ParamList' -> , Id ParamList'
    if (toklist_current->data.type == Comma) {
        if (next_token().type != Id) {
            error_msg(stderr, "syntax error: expected identifier as parameter, got: %s\n",
                      debug_token_name(toklist_current->data));
            exit(SyntaxError);
        }
        token_t p = toklist_current->data;
        // Check if parameter is unique
        if (!passed && htSearch(fun_record->localTS, cstr_of_string(p.string))) {
            error_msg(stderr, "parameters must be unique\n");
            exit(SemanticErrorDefinition);
        }
        // Insert to local symtable
        if (!passed)
            insert_par_fce(cstr_of_string(p.string), fun->key, table_global);
        // Increment parameter count
        if (should_gen_code)
            gen_func_add_param(cstr_of_string(toklist_current->data.string), fun_record->data.par_cnt);
        next_token();
        return parse_ParamList_1(fun);
    }
    // ParamList' -> \epsilon
    return 1;
}

int parse_Args(tHTItem* fun)
{
    // Args -> Term Args'
    if (toklist_current->data.type == Id) {
        // Check if passing defined symbol
        char*    arg = cstr_of_string(toklist_current->data.string);
        tHTItem* arg_data = NULL;
        if (flags & InFunction) {
            if (!(arg_data = htSearch(fun_record->localTS, arg))) {
                if (!(arg_data = htSearch(table_global, arg))) {
                    error_msg(stderr, "passing undefined argument: %s\n", arg);
                    exit(SemanticErrorDefinition);
                }
            }
        } else if (!(arg_data = htSearch(table_global, arg))) {
            error_msg(stderr, "passing undefined argument: %s\n", arg);
            exit(SemanticErrorDefinition);
        }

        if (should_gen_code)
            gen_funcall_add_arg(++arg_cnt, toklist_current->data);
        else
            arg_cnt++;
        next_token();
        return parse_Args_1(fun);
    } else if (is_term(toklist_current->data.type)) {
        if (should_gen_code)
            gen_funcall_add_arg(++arg_cnt, toklist_current->data);
        else
            arg_cnt++;
        next_token();
        return parse_Args_1(fun);
    }
    // Args -> \epsilon
    return 1;
}

int parse_Args_1(tHTItem* fun)
{
    // Args' -> , Id Args'
    if (toklist_current->data.type == Comma) {
        next_token();
        if (toklist_current->data.type == Id) { // Check if passing defined symbol
            char* arg = cstr_of_string(toklist_current->data.string);
            if (flags & InFunction) {
                if (!htSearch(fun_record->localTS, arg) && !htSearch(table_global, arg)) {
                    error_msg(stderr, "passing undefined argument\n");
                    exit(SemanticErrorDefinition);
                }
            } else if (!htSearch(table_global, arg)) {
                error_msg(stderr, "passing undefined argument\n");
                exit(SemanticErrorDefinition);
            }
        } else if (!is_term(toklist_current->data.type)) {
            error_msg(stderr, "syntax error: argument must be terminal\n");
            exit(SyntaxError);
        }
        if (should_gen_code)
            gen_funcall_add_arg(arg_cnt++, toklist_current->data);
        else
            arg_cnt++;
        next_token();
        return parse_Args_1(fun);
    }
    // Args' -> \espilon
    return 1;
}

int parse_Expr(hData* type_out)
{
    token_item_t* backtrack = toklist_current;
    // FIRST() = Id? Check if it's variable
    if (toklist_current->data.type == Id) {
        tHTItem* i;
        if ((i = htSearch(table_global, cstr_of_string(toklist_current->data.string))) && (i->data.fce)) {
            // debug(stderr, "expr got function\n");
            return 0;
        }
    }
    // Expr -> Nil
    if (toklist_current->data.type == KwNil) {
        gen_save_expr_res_nil();
        next_token();
        return 1;
    }
    // Expr -> Factor Expr'
    if (flags & InFunction) {
        if (parse_psa(toklist_current->data, type_out, table_global, fun_record->localTS, should_gen_code)) {
            if (should_gen_code) // Bind result of expression
                gen_save_expr_res();
            return 1;
        }
    } else if (parse_psa(toklist_current->data, type_out, table_global, NULL, should_gen_code)) {
        if (should_gen_code)
            gen_save_expr_res();
        return 1;
    }
    toklist_current = backtrack;
    return 0;
}

#undef error
#undef debug
