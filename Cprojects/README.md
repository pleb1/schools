# ifj18
[![pipeline status](https://gitlab.com/fnctr/ifj18/badges/master/pipeline.svg)](https://gitlab.com/fnctr/ifj18/commits/master)


Use [**libcheck**](https://github.com/libcheck/check) and [**gcov**](https://gcc.gnu.org/onlinedocs/gcc/Invoking-Gcov.html#Invoking-Gcov) for unit tests+coverage,
write documentation here using [**GitLab Markdow**](https://docs.gitlab.com/ee/user/markdown.html)

## TODO: psa
    - `a = nil`
      `a + 3`
      musi byt chyba typu, ale psa nehodi chybu typu

## How to use git
`git clone https://gitlab.com/fnctr/ifj18.git` to download repo and `cd ifj18`
- create personal working branch with `git checkout X` where X is name of your branch
- when you're done working `git checkout master`
- `git pull origin master` (update master)
- `git merge X` (merge your changes into master)
- `git push origin master` (push updates to master)
- when you want to work on your branch again, `git checkout X` (but don't forget to update)

## Lexer

```mermaid
    graph LR
        Init((Init)) -->|whitespace| Init
        Init -->|"\n"|Newline((Newline))
        Init -->|"_ (a-z)"|Id((Id))
        Init -->|"0-9"|Int
        Init -->|"''"|StringOpen
        Init -->|"#"|LineComment((LineComment))
        Init -->|"="|Assign((Assign))
        Init -->|"!"|Mutation
        Init -->|"<"|Less((Less))
        Init -->|">"|More((More))
        Init -->|"("|LPar((LPar))
        Init -->|")"|RPar((RPar))
        Init -->|"EOF"|Eof((Eof))
        Init -->|","|Comma((Comma))
        Init -->|"+"|Add((Add))
        Init -->|"-"|Sub((Sub))
        Init -->|"*"|Mul((Mul))
        Init -->|"/"|Div((Div))
        Init -->|"&"|At
        Init -->|"|"|Pipe
        Init -->|"?"|BoolFun((BoolFun))
        
        At-->|"&"|And((And))
        Pipe -->|"|"|Or((Or))
        
        
        More -->|"="|MoreEq((MoreEq))
        Less -->|"="|LessEq((LessEq))
        Mutation -->|"="|NotEq((NotEq))
        
        LineComment-->|"not \n"|LineComment
        
        Assign-->|"="|Eq((Eq))
        
        Newline-->|"="|BC0
        BC0-->|"b"|BC1
        BC1-->|"e"|BC2
        BC2-->|"g"|BC3
        BC3-->|"i"|BC4
        BC4-->|"n"|BC5
        BC5-->|"whitespace"|BlockComment((BlockComment))
        BlockComment-->|'not \n'|BlockComment
        BlockComment-->|"\n"|BCE0
        BCE0-->|"="|BCE1
        BCE0-->|"\n"|BCE0
        BCE0-->BlockComment
        BCE1-->|"e"|BCE2
        BCE1-->|"\n"|BCE0
        BCE1-->BlockComment
        BCE2-->|"n"|BCE3
        BCE2-->|"\n"|BCE0
        BCE2-->BlockComment
        BCE3-->|"d"|BCE4
        BCE3-->|"\n"|BCE0
        BCE4-->|"whitespace"|BlockCommentEnd((BlockCommentEnd))
        BCE4-->|"\n"|BlockCommentEndDirect((BlockCommentEndDirect))
        BCE4-->BlockComment
        BlockCommentEnd-->|"not \n"|BlockCommentEnd
        
        Id -->|"_ (a-Z) (0-9)"|Id
        
        StringOpen -->|"\"|StringEsc
        StringOpen -->|"''"|String((String))
        StringOpen -->StringOpen
        StringEsc -->|"n,t,s,\,''"|StringOpen
        StringEsc -->|"x"|StringEscHex0
        StringEscHex0 -->|"(0-9)+(a-f)"|StringEscHex1
        StringEscHex1 -->|"(0-9)+(a-f)"|StringOpen
        
        Int -->|"0-9"|Int((Int))
        Int -->|"."|RealDecSep
        Int -->|"e"|RealExp
        
        RealDecSep -->|"0-9"|RealDec((RealDec))
        RealDec -->|"e E"|RealExp
        RealExp -->|"[+]"|RealExpPos0
        RealExp -->|"-"|RealExpNeg0
        RealExpPos0 -->|"0-9"|RealExpPos((RealExpPos))
        RealExpPos -->|"0-9"|RealExpPos
        RealExpNeg0 -->|"0-9"|RealExpNeg((RealExpNeg))
        RealExpNeg -->|"0-9"|RealExpNeg
```

## Parser
LL grammar, parsed by recursive descent up to `Expr`

### Grammar
- 1: `Prog` $`\to`$ `FunDef` `Prog`
- 2: `Prog` $`\to`$ `Stmt` `Prog`
- 3: `Prog` $`\to`$ `eol` `Prog`
- 4: `Prog` $`\to`$ `eof`
- 5: `FunDef` $`\to`$ `def` `id` `(` `ParamList` `)` `eol` `Stmt` `end` `eol`
- 6: `Stmt` $`\to`$ `if` `Expr` `then` `eol` `Stmt` `else` `eol` `Stmt` `end` `eol` `Stmt`
- 7: `Stmt` $`\to`$ `while` `Expr` `do` `eol` `Stmt` `end` `eol` `Stmt`
- 8: `Stmt` $`\to`$ `id` `=` `RVal` `eol` `Stmt`
- 9: `Stmt` $`\to`$ `Expr` `eol` `Stmt`
- 10: `Stmt` $`\to`$ `FunCall` `eol` `Stmt`
- 11: `Stmt` $`\to`$ $`\epsilon`$
- 12: `RVal` $`\to`$ `FunCall`
- 13: `RVal` $`\to`$ `Expr`
- 14: `FunCall` $`\to`$ `id` `Modifier` `(` `Args` `)`
- 15: `FunCall` $`\to`$ `id` `Modifier` `Args`
- 16: `Modifier` $`\to`$ `!` | `?`
- 17: `Modifier` $`\to`$ $`\epsilon`$
- 18: `ParamList` $`\to`$ `id` `ParamList'`
- 19: `ParamList` $`\to`$ $`\epsilon`$
- 20: `ParamList'` $`\to`$ `,` `id` `ParamList'`
- 21: `ParamList'` $`\to`$ $`\epsilon`$
- 22: `Args` $`\to`$ `Term` `Args'`
- 23: `Args` $`\to`$ $`\epsilon`$
- 24: `Args'` $`\to`$ `,` `Term` `Args'`
- 25: `Args'` $`\to`$ $`\epsilon`$
- 26: `Expr` $`\to`$ `Factor` `Expr'`
- 27: `Expr'` $`\to`$ `Op` `Factor` `Expr'`
- 28: `Expr'` $`\to`$ $`\epsilon`$
- 29: `Factor` $`\to`$ `(` `Expr` `)`
- 30: `Factor` $`\to`$ `Term`
- 31: `Term` $`\to`$ `id` | `int` | `real` | `string` | `nil`
- 32: `Op` $`\to`$ `+` | `-` | `*` | `/` | `<` | `>` | `<=` | `>=` | `==` | `!=` | `&&` | `||`

LL-table

|             | `def` | `eol` | `eof` | `id`  | `Term` | `Op` | `if` | `while` | `!` `?`| `(`| `,`| `$` |
|:------:     | :---: | :-:   | :-:   | :-:   | :-:    | :-:  | :-:  | :-:     | :-:    |:-: |:-: | :-: | 
| `Prog`      | 1     |     3 | 4     |     2 | 2      |      | 2    |  2      |        |    |    |     |
| `FunDef`    | 5     |       |       |       |        |      |      |         |        |    |    |     |
| `Stmt`      |       |       |       | 8,10,9| 9      |      | 6    |   7     |        |    |    | 11  |
| `RVal`      |       |       |       | 12,13 | 13     |      |      |         |        |    |    |     |
| `Modifier`  |       |       |       |       |        |      |      |         |16      |    |    |17   |
| `FunCall`   |       |       |       | 14,15 |        |      |      |         |        |    |    |     |
| `ParamList` |       |       |       | 18    |        |      |      |         |        |    |    |19   | 
| `ParamList'`|       |       |       |       |        |      |      |         |        |    | 20 |21   |
| `Args`      |       |       |       | 22    | 22     |      |      |         |        |    |    |23   |
| `Args'`     |       |       |       |       |        |      |      |         |        |    | 24 |25   |
| `Expr`      |       |       |       | 26    | 26     |      |      |         |        |    |    | 26  |
| `Expr'`     |       |       |       |       |        | 27   |      |         |        |    |    | 28  |
| `Factor`    |       |       |       |       | 30     |      |      |         |        | 29 |    |     |
| `Term`      |       |       |       | 31    | 31     |      |      |         |        |    |    |     |


### str.h
Nil - terminated C string (`char*` or `const char*`) wrapper interface, defining opaque structure `string_t` and operations on it.

- functions that can fail `return 0` on failure

- `const` qualifier serves as a semantic hint; such arguments aren't being modified by the function

- every string should be defined as `string_t *s = string_new()`

- `string_free_safe(&s)` will ensure `!s` while `string_free(s)` will not, so caller has to ensure it is no longer used

- `string_clear(s)` only sets first character to nil, it leaves the rest unmodified so be aware of this behaviour when calling `cstr_of_string(s)`

- do not use `string_of_cstr()` without binding it to a variable, otherwise you will leak memory

- `string_cmp()` and `string_cmp_cstr()` have return value analogous to `strcmp()` from `string.h`

- `string_find()` uses [shift-or](http://www-igm.univ-mlv.fr/~lecroq/string/node6.html) algorithm, which is $`\mathcal{O}(m+n)`$ but can only search substring of length at max 31


### lexer.h
Lexer interface, defines a structure `token_t` that carries result of lexing and an opaque structure `lexer_state` and operations on it.

- `lexer_state* lexer_init(const char*)` initializes a lexer with argument being path to the input file
    > returns nil on failure

    > if argument is nil, input is taken from `stdin`

- `lexer_close(lexer_state*)` cleans up the resources used by lexer

- `token_t get_token(lexer_state*)` returns next token from lexer passed to it
    > sets `token_t.type = LexError` on failure

    > sets `token_t.string` when `token_t.type` is `Id, Keyword, String` so don't forget to clean up
