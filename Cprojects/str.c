// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
#include "str.h"

#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "error.h"

static const uint32_t default_length_step = 2;

typedef struct string_t {
    char*    data;
    uint32_t len;
    uint32_t alloc;
} string_t;

void debug_string(const string_t* str)
{
    fprintf(stderr, "[DEBUG]: string_t [%p]\n\t\t[data]='%s'\n", (void*)str, str->data);
}

string_t* string_new(void)
{
    string_t* str;
    if (!(str = (string_t*)malloc(sizeof(string_t))))
        return NULL;
    if (!(str->data = (char*)malloc(default_length_step)))
        return NULL;
    str->len = 0;
    str->data[str->len] = '\0';
    str->alloc = default_length_step;

    return str;
}

void string_free(string_t* str)
{
    if (!str)
        return;
    free(str->data);
    str->data = NULL;
    str->len = 0;
    str->alloc = 0;
    free(str);
    str = NULL;
}

void string_free_safe(string_t** str_ptr)
{
    if (!str_ptr)
        return;
    if (!*str_ptr)
        return;
    string_free(*str_ptr);
    *str_ptr = NULL;
}

void string_clear(string_t* str)
{
    str->data[0] = '\0';
    str->len = 0;
}

int string_append(string_t* str, const char c)
{
    if (str->len + 1 >= str->alloc) { // alloc block too small -> realloc
        if (!(str->data = (char*)realloc(str->data, str->len + default_length_step)))
            return 0;
        str->alloc += default_length_step;
    }
    str->data[str->len++] = c;
    str->data[str->len] = '\0';

    return 1;
}

int string_cat(string_t* a, const string_t* b)
{
    uint32_t l = a->len + b->len;
    if (a->alloc < l) {
        if (!(a->data = (char*)realloc(a->data, l)))
            return 0;
        a->alloc = l;
    }
    for (uint32_t i = 0; i < b->len; ++i)
        a->data[a->len++] = b->data[i];
    a->data[a->len] = '\0';

    return 1;
}

int string_copy(const string_t* from, string_t* to)
{
    if (to->alloc <= from->len) {
        free(to->data);
        if (!(to->data = (char*)calloc(1, from->len)))
            return 0;
        to->alloc = from->len;
        to->len = from->len;
    }
    memcpy(to->data, from->data, from->len);

    return 1;
}
int string_copy_bound(const string_t* from, string_t* to, uint32_t begin, uint32_t end)
{
    if (to->alloc < end - begin) {
        if (!(to->data = (char*)realloc(to->data, end - begin + 2))) // 0 index + nil
            return 0;
        to->alloc = end - begin + 2; // allocated for nil
        to->len = end - begin + 1;   // length -> nil not counted
    }
    if (end - begin)
        memcpy(to->data, from->data + begin, end - begin);
    else
        memcpy(to->data, from->data + begin, 1);
    to->data[to->len] = '\0'; // nil termination
    return 1;
}

string_t* string_strip(const string_t* str)
{
    string_t* r = string_new();
    if (str->len == 2) // Empty string after stripping
        return r;

    r->data = (char*)realloc(r->data, str->len - 2 + 1); // Strip 2 symbols, ad nil-terminator
    r->alloc = str->len - 2 + 1;
    r->len = str->len - 2;
    if (!r->data)
        return NULL;
    memcpy(r->data, str->data + 1, str->len - 2);
    r->data[r->len] = '\0';
    return r;
}

int string_cmp(const string_t* a, const string_t* b)
{
    char* pa = a->data;
    char* pb = b->data;
    while (*pa == *pb) {
        if (!*pa || !*pb)
            break;
        pa++;
        pb++;
    }
    if (*pa == '\0' && *pb == '\0')
        return 0;
    if (*pa == '\0')
        return -1;
    return 1;
}

int string_cmp_cstr(const string_t* str, const char* cstr)
{
    char* ps = str->data;
    char* pc = (char*)cstr;
    while (*ps == *pc) {
        if (!*ps || !*pc)
            break;
        ps++;
        pc++;
    }
    if (*ps == '\0' && *pc == '\0')
        return 0;
    if (*ps == '\0')
        return -1;
    return 1;
}

// does [str] contain character [c]?
int string_contains(const string_t* str, const char c)
{
    for (uint32_t i = 0; i < str->len; ++i)
        if (str->data[i] == c)
            return 1;
    return 0;
}

// find substring (of max length 31), using shift-or algorithm [http://www-igm.univ-mlv.fr/~lecroq/string/node6.html]
int string_find(const string_t* str, const string_t* substr)
{
    if (substr->data[0] == '\0' || substr->len > 31) {
        fprintf(stderr, "string_find: Trying to find empty substring or substring is too long\n");
        return 0;
    }

    uint32_t bitfield = ~1;
    uint32_t mask[CHAR_MAX + 1];

    for (uint32_t i = 0; i <= CHAR_MAX; ++i)
        mask[i] = ~0;
    for (uint32_t i = 0; i < substr->len; ++i)
        mask[(int)substr->data[i]] &= ~(1UL << i);
    for (uint32_t i = 0; str->data[i] != '\0'; ++i) {
        bitfield |= mask[(int)str->data[i]];
        bitfield <<= 1;

        if (!(bitfield & (1UL << substr->len)))
            return 1;
    }

    return 0;
}

int string_find_cstr(const string_t* str, const char* cstr)
{
    string_t* substr = string_of_cstr(cstr);
    int       ret = string_find(str, substr);
    string_free(substr);
    return ret;
}

string_t* string_substr(const string_t* str, uint32_t start, uint32_t len)
{
    if (start > str->len)
        return string_new();

    string_t* ret = string_new();
    for (char *s = str->data; *s && start < len; ++s, ++len)
        string_append(ret, *s);

    return ret;
}

char* cstr_of_string(const string_t* str)
{
    return str->data;
}

string_t* string_of_cstr(const char* cstr)
{
    string_t* str = string_new();
    char*     pcstr = (char*)cstr;
    while (*pcstr) {
        string_append(str, *pcstr++);
    }

    return str;
}

uint32_t string_len(const string_t* str)
{
    return str->len;
}

// convert string to IFJcode18-compatible string
char* string_stringify(string_t* str)
{
    string_t* s = string_new();
    for (uint32_t i = 0; i < str->len; ++i) {
        // IFJcode18 doesn't have nil-terminated strings
        if (str->data[i] == '\0')
            break;
        // Convert escape sequences to IFJcode18
        else if (str->data[i] == '\\') {
            //             \ x y z
            char esc[] = "\0\0\0\0";
            // First try simple \n, \s, \t, \\, \"
            if ((i < str->len - 1) && (str->data[i + 1] != 'x')) {
                switch (str->data[i + 1]) {
                case 'n':
                    sprintf(esc, "\\%03d", (unsigned char)'\n');
                    string_append(s, esc[0]);
                    string_append(s, esc[1]);
                    string_append(s, esc[2]);
                    string_append(s, esc[3]);
                    break;
                case 's':
                    sprintf(esc, "\\%03d", (unsigned char)' ');
                    string_append(s, esc[0]);
                    string_append(s, esc[1]);
                    string_append(s, esc[2]);
                    string_append(s, esc[3]);
                    break;
                case 't':
                    sprintf(esc, "\\%03d", (unsigned char)'\t');
                    string_append(s, esc[0]);
                    string_append(s, esc[1]);
                    string_append(s, esc[2]);
                    string_append(s, esc[3]);
                    break;
                case '\\':
                    sprintf(esc, "\\%03d", (unsigned char)'\\');
                    string_append(s, esc[0]);
                    string_append(s, esc[1]);
                    string_append(s, esc[2]);
                    string_append(s, esc[3]);
                    break;
                case '\"':
                    sprintf(esc, "\\%03d", (unsigned char)'\"');
                    string_append(s, esc[0]);
                    string_append(s, esc[1]);
                    string_append(s, esc[2]);
                    string_append(s, esc[3]);
                    break;
                }
                i++;
                continue;
            }
            // Next try hex sequence
            if ((i < str->len - 3) && (str->data[i + 1] == 'x')) {
                char hex[] = { str->data[i + 2], str->data[i + 3], '\0' }; // extract hex part for conversion
                int  c = (int)strtol(&hex[0], NULL, 16);                   // to decadic
                sprintf(esc, "\\%03d", c);
                string_append(s, esc[0]);
                string_append(s, esc[1]);
                string_append(s, esc[2]);
                string_append(s, esc[3]);
                i += 3;
            }
        }
        // Convert whitespace and special characters required by IFJcode18 to escape seq
        else if (isspace(str->data[i]) || str->data[i] == '#' || str->data[i] == '\\') {
            //             \ x y z
            char esc[] = "\0\0\0\0";
            sprintf(esc, "\\%03d", (unsigned char)str->data[i]);
            string_append(s, esc[0]);
            string_append(s, esc[1]);
            string_append(s, esc[2]);
            string_append(s, esc[3]);
        } else
            string_append(s, str->data[i]);
    }
    string_copy(s, str);
    return cstr_of_string(str);
}