// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Radovan Guzik <xnguye11@stud.fit.vutbr.cz>
#pragma once
#include "str.h"

typedef enum {
    LexError = 0,
    Id,

    KwIf,
    KwElse,
    KwThen,
    KwEnd,
    KwDef,
    KwWhile,
    KwDo,
    KwNot,
    KwNil,

    OpAdd,
    OpSub,
    OpDiv,
    OpMul,
    OpEq,
    OpNotEq,
    OpEqLess,
    OpEqMore,
    OpLess,
    OpMore,
    OpAnd,
    OpOr,
    OpAssign,
    OpMut,
    OpBool,

    ParenLeft,
    ParenRight,
    Comma,

    Integer,
    Real,
    String,

    Eol,
    Eof,
    TokenType_COUNT
} TokenType;

typedef struct {
    TokenType type;
    union {
        int       integer;
        double    real;
        string_t* string;
    };
} token_t;

typedef struct lexer_state lexer_state;

lexer_state* lexer_init(const char* path);
void         lexer_close(lexer_state* lex);
token_t      get_token(lexer_state* lex);
void         debug_token(token_t t);
const char*  debug_token_name(token_t t);
