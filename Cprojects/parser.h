// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
#pragma once
#include "lexer.h"

int     parse(lexer_state* lex, int* error);
token_t next_token(void);