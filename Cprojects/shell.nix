{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation rec {
  name = "ifj18";

  buildInputs = [
    pkgs.gcc pkgs.gdb pkgs.valgrind
    pkgs.clang-tools pkgs.check
  ];
}
