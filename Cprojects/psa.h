// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// xnguye11 <xnguye11@stud.fit.vutbr.cz>
#include "parser.h"
#include "symtable.h"
#pragma once

int parse_psa(token_t tok, hData* type, tHTable* global, tHTable* local, int gen);
