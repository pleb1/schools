// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// xnguye11 <xnguye11@stud.fit.vutbr.cz>
#include "tstack.h"
#include "error.h"
#include "lexer.h"
#include "parser.h"
#include <stdio.h>
#include "psa.h"
#include <stdlib.h>

token_t token;
int*    err;
// clang-format off
// tabulka precedencni tabulky
int generate(tStack *tem, tStack *op, hData *type, tHTable* global, tHTable* local);
token_t op1, op2, op3, oper1, oper2;
int tc = 0, oc = 0, left = 0, rigth = 0; 
static char prec_tab[][8] = {
/*|             | * / | + - | < rel |== !=  | ( | ) |id | $ |       */
/*| * / |*/     {'>'  ,'>'  ,'>'    ,'>'    ,'<','>','<','>'},
/*| + - |*/     {'<'  ,'>'  ,'>'    ,'>'    ,'<','>','<','>'},
/*| < rel |*/   {'<'  ,'<'  ,' '    ,'>'    ,'<','>','<','>'},
/*| == !=|*/    {'<'  ,'<'  ,'<'    ,' '    ,'<','>','<','>'},
/*| (  |*/      {'<'  ,'<'  ,'<'    ,'<'    ,'<','=','<','>'},
/*| )  |*/      {'>'  ,'>'  ,'>'    ,'>'    ,' ','>',' ','>'},
/*| id |*/      {'>'  ,'>'  ,'>'    ,'>'    ,' ','>',' ','>'},
/*| $  |*/      {'<'  ,'<'  ,'<'    ,'<'    ,'<',' ','<','-'},
};
// clang-format on
int    flag = 0;
int    tstart = 0;
int    ostart = 0;
double givedouble(int a)
{
    return (double)a;
}

int paren(tStack* tem, tStack* op);

int num_of_prec_table(token_t t)
{
    switch (t.type) {
    case OpSub:
    case OpAdd:
        return 1;
        break;
    case OpDiv:
    case OpMul:
        return 0;
        break;
    case OpMore:
    case OpEqLess:
    case OpEqMore:
    case OpLess:
        return 2;
        break;
    case OpNotEq:
    case OpEq:
        return 3;
        break;
    case ParenLeft:
        return 4;
        break;
    case ParenRight:
        return 5;
        break;
    case Id:
    case Integer:
    case Real:
    case String:
    case KwNil:
        return 6;
        break;
    case Eof:
    case Eol:
    case KwThen:
    case KwDo:
        return 7;
        break;
    // chyba SA, nic jineho nemuze byt ve vyrazu
    default:
        fprintf(stderr, "SyntaxError: illegal character");
        exit(SyntaxError);
        return -1;
        break;
    }
}

// Expr → Factor Expr' | nil
// Expr' → Op Factor Expr' | ϵ\epsilonϵ
// Factor → ( Expr ) | Term
// Term → Id | Int | Real | String
// Op → + | - | * | / | < | > | <= | >= | == | != | && | ||
int parse_psa(token_t tok, hData* type, tHTable* global, tHTable* local, int gen)
{
    tc = 0, oc = 0;
    FILE*    file = stdout; // fopen("code.txt","w");
    tHTItem *item1, *item2;
    int      d = 0;
    // gen = 1;
    if (type == NULL)
        flag = 0;
    else
        flag = 1;
    token_t pom, pop, top, rel = { 0 };
    token_t op1, op2, op3, z1, z2, tpom, opom;
    int     i, t, no = 0, cmul = 0, rc = 0, cadd = 0, l = 1;
    tStack* s = (tStack*)malloc(sizeof(tStack));
    tStack* op = (tStack*)malloc(sizeof(tStack));
    tStack* tem = (tStack*)malloc(sizeof(tStack));
    if (s == NULL || op == NULL || tem == NULL) {
        exit(InternalError);
    }
    char znak;
    if (!stackInit(s) || !stackInit(op) || !stackInit(tem)) {
        exit(InternalError);
    }

    pom = tok;
    // item1 = htSearch(global,cstr_of_string(pom.string));
    if (pom.type == Eol || pom.type == Eof)
        return 0;
    while (1) {
        i = num_of_prec_table(pom);
        if (i == -1) {
            fprintf(stderr, "SyntaxError: illegal character");
            exit(SyntaxError);
        }
        if (stackEmpty(s))
            t = TopValue(s);
        else {
            top = stacktop(s);
            t = num_of_prec_table(top);
        }
        znak = prec_tab[t][i];

        if (znak == '-') {
            if (rc > 1) {
                fprintf(stderr, "SyntaxError: too many rel op");
                exit(SyntaxError);
            }
            if (gen == 0) {
                if (flag == 1) {
                    if (type->type == KwNil && type->define == false) {
                        while (!stackEmpty(tem)) {
                            pom = stackPop(tem);
                            if (pom.type == Real)
                                d = 1;
                            if (pom.type == String)
                                d = 2;
                        }
                        if (d == 1)
                            type->type = Real;
                        if (d == 2)
                            type->type = String;
                        if (d == 0)
                            type->type = Integer;
                    }
                }
            } else {
                generate(tem, op, type, global, local);
            }
            // fclose(file);
            return 1;
        } else if (znak == '<') // push token
        {
            if (!stackPush(s, pom))
                exit(InternalError);
            if (num_of_prec_table(pom) == 6) {
                if (pom.type == Id) { // test if id exists
                    if (local != NULL) {
                        if (!htSearch(local, cstr_of_string(pom.string)) &&
                            !htSearch(global, cstr_of_string(pom.string))) {
                            fprintf(stderr, "SemanticError: undefined ID");
                            exit(SemanticErrorDefinition);
                        } else {
                            if (tc > 1) {
                                item1 = htSearch(local, cstr_of_string(pom.string));
                                item2 = htSearch(global, cstr_of_string(pom.string));
                                if (item1 && item1->data.fce) {
                                    fprintf(stderr, "SyntaxError: funcall can't be in expression\n");
                                    exit(SyntaxError);
                                } else if (item2 && item2->data.fce) {
                                    fprintf(stderr, "SyntaxError: funcall can't be in expression\n");
                                    exit(SyntaxError);
                                }
                            }
                        }
                    } else {
                        if (!htSearch(global, cstr_of_string(pom.string))) {
                            fprintf(stderr, "SemanticError: undefined ID");
                            exit(SemanticErrorDefinition);
                        } else {
                            if (tc > 1) {
                                item2 = htSearch(global, cstr_of_string(pom.string));
                                if (item2->data.fce == true) { // trying to do int +-*/ func error
                                    fprintf(stderr, "SyntaxError: trying to do term + func");
                                    exit(SyntaxError);
                                }
                            }
                        }
                    }
                }
                if (!stackPush(tem, pom))
                    exit(InternalError);
                tc++;
                pom = next_token();                                             // after id operation or error
                if (num_of_prec_table(pom) == 6 && num_of_prec_table(pom) == 4) // id( or id id
                {
                    fprintf(stderr, "SyntaxError: expecting op after term");
                    exit(SyntaxError);
                }
            } else if (num_of_prec_table(pom) < 4) {
                if (!stackPush(op, pom))
                    exit(InternalError);
                oc++;
                pom = next_token();                                             // after op term or error
                if (num_of_prec_table(pom) != 6 && num_of_prec_table(pom) != 4) // + id or (
                {
                    fprintf(stderr, "SyntaxError: expecting Term or ( after op");
                    exit(SyntaxError);
                }
            } else {
                left++;
                tc++;
                if (!stackPush(tem, pom))
                    exit(InternalError);
                pom = next_token();
                if (num_of_prec_table(pom) != 6 && num_of_prec_table(pom) != 4) // ( id
                {
                    fprintf(stderr, "SyntaxError: expecting Term  or ( after (\n");
                    exit(SyntaxError);
                }
            }
        } else if (znak == '>') {
            {
                pop = stackPop(s);
                if (pop.type == Integer || String || Real) {
                    if (flag == 1) {
                        if (type->type == KwNil && type->define == false) {
                            if (pop.type == Integer)
                                type->type = Integer;
                            if (pop.type == String)
                                type->type = String;
                            if (pop.type == Real)
                                type->type = Real;
                        }
                    }
                }
                // if (pop.type == Id || pop.type == Integer || pop.type == Real || pop.type == String) {
                if (num_of_prec_table(pom) < 4) {
                    if (num_of_prec_table(pom) == 3 || num_of_prec_table(pom) == 2)
                        rc++;
                    oc++;
                    if (!stackPush(op, pom))
                        exit(InternalError);
                    pom = next_token();
                    if (num_of_prec_table(pom) != 6 && num_of_prec_table(pom) != 4) {
                        fprintf(stderr, "SyntaxError: Expecting term or ( after op");
                        exit(SyntaxError);
                    }
                }
                // TODO: fix
                if (pom.type == KwDo || pom.type == KwThen || pom.type == Eol || pom.type == Eof) // po obdrzeni tokenu
                {
                    if (left != rigth) { // test (E)
                        fprintf(stderr, "SyntaxError: Wrong number of Parens");
                        exit(SyntaxError);
                    }
                    if (rc > 1) {
                        fprintf(stderr, "SyntaxError: too many rel op");
                        exit(SyntaxError);
                    }
                    if (gen == 0) { // gen = 0 prvni pruchod se nic negeneruje
                        if (flag == 1) {
                            if (type->type == KwNil) {
                                while (!stackEmpty(tem)) {
                                    pom = stackPop(tem);
                                    if (pom.type == Real)
                                        d = 1;
                                    if (pom.type == String)
                                        d = 2;
                                }
                                if (d == 1)
                                    type->type = Real;
                                if (d == 2)
                                    type->type = String;
                                if (d == 0)
                                    type->type = Integer;
                            }
                        }
                    } else {
                        generate(tem, op, type, global, local);
                    }
                    return 1;
                }

                if (pom.type == ParenRight) {
                    rigth++;
                    tc++;
                    if (!stackPush(tem, pom))
                        exit(InternalError);
                    pom = next_token(); // after ( id or ) else error
                    if (num_of_prec_table(pom) == 6 || num_of_prec_table(pom) == 4) {
                        fprintf(stderr, "SyntaxError: expected op after )");
                        exit(SyntaxError);
                    }
                }
            }
        } else if (znak == '=') {
            tc++;
            if (!stackPush(tem, pom))
                exit(InternalError);
            rigth++;
            pom = next_token();
            if (pom.type == Id) {
                if (local != NULL) {
                    if (!htSearch(local, cstr_of_string(pom.string)) && !htSearch(global, cstr_of_string(pom.string))) {
                        fprintf(stderr, "SemanticError: undefined ID");
                        exit(SemanticErrorDefinition);
                    }
                } else {
                    if (!htSearch(global, cstr_of_string(pom.string))) {
                        fprintf(stderr, "SemanticError: undefined ID");
                        exit(SemanticErrorDefinition);
                    }
                }
            }
            if (num_of_prec_table(pom) == 6 || num_of_prec_table(pom) == 4) {
                fprintf(stderr, "SyntaxError: expected op after )");
                exit(SyntaxError);
            }
        } else {
            fprintf(stderr, "SyntaxError: illegal syntax");
            exit(SyntaxError);
        }
    }
}

int getop(token_t tok) // func for getting operation
{
    int a = 0;
    switch (tok.type) {
    case OpAdd:
        a = 0;
        break;
    case OpSub:
        a = 1;
        break;
    case OpMul:
        a = 2;
        break;
    case OpDiv:
        a = 3;
        break;
    case OpEq:
    case OpNotEq:
        a = 4;
        break;
    case OpLess:
    case OpEqLess:
        a = 5;
        break;
    case OpMore:
    case OpEqMore:
        a = 6;
        break;
    default:
        break;
    }
    return a;
}
// generate code
int generate(tStack* tem, tStack* op, hData* type, tHTable* global, tHTable* local)
{
    FILE*    file = stdout; // fopen("code.txt", "w");
    tHTItem* pom1;
    fprintf(file, "%s", "DEFVAR LF@RES\n");
    fprintf(file, "%s", "DEFVAR LF@TMP\n");
    fprintf(file, "%s", "DEFVAR LF@POM\n");
    token_t op1, op2, op3, z1, z2, rel = { 0 }, pom;
    int     g = 0, cmul = 0, cadd = 0, start = 0, l = 0, tt = 0;
    bool    d = false;
    double  d1, d2, d3;
    if (oc == 0) {
        op1 = stackPop(tem);
        if (op1.type == Integer) {
            fprintf(file, "MOVE LF@RES int@%d\n", op1.integer);
            if (flag == 1) {
                if (type->type == KwNil)
                    type->type = Integer;
            }
        }
        if (op1.type == Id) {
            fprintf(file, "MOVE LF@RES LF@%s\n", cstr_of_string(op1.string));
            pom1 = htSearch(local, cstr_of_string(op1.string));
            if (pom1 == NULL)
                pom1 = htSearch(global, cstr_of_string(op1.string));
            if (flag == 1) {
                if (type->type == KwNil) {
                    if (pom1->data.type == Integer)
                        type->type = Integer;
                    if (pom1->data.type == String)
                        type->type == String;
                    if (pom1->data.type == Real)
                        type->type == Real;
                }
            }
        }
        if (op1.type == String) {
            fprintf(file, "MOVE LF@RES string@%s\n", cstr_of_string(op1.string));
            if (flag == 1) {
                if (type->type == KwNil)
                    type->type == String;
            }
        }
        if (op1.type == Real) {
            fprintf(file, "MOVE LF@RES int@%lf\n", op1.real);
            if (flag == 1) {
                if (type->type == KwNil)
                    type->type == Real;
            }
        }
    }
    if (oc == 1) { // term op term
        op1 = stackPop(tem);
        while (num_of_prec_table(op1) != 6) {
            op1 = stackPop(tem);
        }
        op2 = stackPop(tem);
        while (num_of_prec_table(op2) != 6) {
            op2 = stackPop(tem);
        }
        z1 = stackPop(op);
        // string + int or real // err
        if ((op1.type == String && (op2.type == Integer || op2.type == Real)) ||
            ((op1.type == Integer || op1.type == Real) && op2.type == String)) {
            fprintf(stderr, "SemanticError: error type %s", debug_token_name(op1));
            exit(SemanticErrorType);
        } else if ((op1.type == Real || op2.type == Real) && (op1.type != Id && op2.type != Id)) {
            d = true;                  // real + real or real+ int
            if (op1.type == Integer) { // convert int to real
                d1 = givedouble(op1.integer);
                g = getop(z1);
                if (g < 4) {
                    if (op1.type == KwNil || op2.type == KwNil) // cant do int +*- nil
                    {
                        fprintf(stderr, "SemanticErrorType: nil");
                        exit(SemanticErrorType);
                    }
                    if (g == 0)
                        fprintf(file, "ADD LF@RES float@%lf float%lf\n", op2.real, d1);
                    if (g == 1)
                        fprintf(file, "SUB LF@RES float@%lf float%lf\n", op2.real, d1);
                    if (g == 2)
                        fprintf(file, "MUL LF@RES float@%lf float%lf\n", op2.real, d1);
                    if (g == 3)
                        fprintf(file, "DIV LF@RES float@%lf float%lf\n", op2.real, d1);
                }
                if (g == 4)
                    fprintf(file, "EQ LF@RES float%lf float%lf\n", op2.real, d1);
                if (g == 5)
                    fprintf(file, "LT LF@RES float@%lf float%lf\n", op2.real, d1);
                if (g == 6)
                    fprintf(file, "GT LF@RES float@%lf float%lf\n", op2.real, d1);
                return 1;
            } else {
                d1 = givedouble(op2.integer);
                g = getop(z1);
                if (g < 3) {
                    if (op1.type == KwNil || op2.type == KwNil) {
                        fprintf(stderr, "SemanticErrorType: nil");
                        exit(SemanticErrorType);
                    }
                    if (g == 0)
                        fprintf(file, "ADD LF@RES float@%lf float%lf\n", d1, op1.real);
                    if (g == 1)
                        fprintf(file, "SUB LF@RES float@%lf float%lf\n", d1, op1.real);
                    if (g == 3)
                        fprintf(file, "DIV LF@RES float@%lf float%lf\n", d1, op1.real);
                    if (g == 2)
                        fprintf(file, "MUL LF@RES float@%lf float%lf\n", d1, op1.real);
                    return 1;
                } else {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES float@%lf float%lf\n", d1, op1.real);
                    if (g == 5)
                        fprintf(file, "LT LF@RES float@%lf float%lf\n", d1, op2.real);
                    if (g == 6)
                        fprintf(file, "GT LF@RES float%lf float%lf\n", d1, op2.real);
                    return 1;
                }
            }
        } else if (op1.type == Id || op2.type == Id) { // op1 or op 2 is id
            if (op1.type == Id) {                      // op is id  test type
                if (op2.type == Integer) {
                    pom1 = htSearch(local, cstr_of_string(op1.string));
                    if (pom1 == NULL) {
                        pom1 = htSearch(global, cstr_of_string(op1.string));
                    }
                    if (pom1 != NULL) {
                        if (pom1->data.type == String || (pom1->data.type == KwNil && pom1->data.define == true)) {
                            fprintf(stderr, "SemanticErrorType: string");
                            exit(SemanticErrorType);
                        }
                    }
                    g = getop(z1);
                    if (g < 4) {
                        if (g == 0)
                            fprintf(file, "ADD LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                        if (g == 1)
                            fprintf(file, "SUB LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                        if (g == 3)
                            fprintf(file, "DIV LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                        if (g == 2)
                            fprintf(file, "MUL LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                    }
                    if (g == 4)
                        fprintf(file, "EQ LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                    if (g == 5)
                        fprintf(file, "LT LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                    if (g == 6)
                        fprintf(file, "GT LF@RES int@%d LF@%s\n", op2.integer, cstr_of_string(op1.string));
                    return 1;
                } else if (op2.type == String) { // op2 is string need to test type of id
                    pom1 = htSearch(local, cstr_of_string(op1.string));
                    if (pom1 == NULL) {
                        pom1 = htSearch(global, cstr_of_string(op1.string));
                    }
                    if (pom1 != NULL) {
                        if (pom1->data.type != String) {
                            fprintf(stderr, "SemanticErrorType");
                            exit(SemanticErrorType);
                        }
                        g = getop(z1);
                        if (g > 3) {
                            if (pom1->data.type != String)
                                fprintf(file, "%s", "MOVE LF@RES bool@%false\n");
                            else {
                                if (g == 4)
                                    fprintf(file, "EQ LF@RES LF@%s string@%s", cstr_of_string(op1.string),
                                            cstr_of_string(op2.string));
                                if (g == 5)
                                    fprintf(file, "LT LF@RES LF@%s string@%s", cstr_of_string(op1.string),
                                            cstr_of_string(op2.string));
                                if (g == 6)
                                    fprintf(file, "GT LF@RES LF@%s string@%s", cstr_of_string(op1.string),
                                            cstr_of_string(op2.string));
                            }
                        } else if (g == 0) { // if not string + string return error
                            if (pom1->data.type != String) {
                                fprintf(stderr, "SemanticErrorType");
                                exit(SemanticErrorType);
                            }
                            fprintf(file, "CONCAT LF@RES LF@%s strin@%s", cstr_of_string(op1.string),
                                    cstr_of_string(op2.string));
                        }
                    }

                } else { // op2 is real
                    d = true;
                    g = getop(z1);
                    if (g < 4) {
                        if (op1.type == KwNil || op2.type == KwNil) {
                            fprintf(stderr, "SemanticErrorType: nil or string");
                            exit(SemanticErrorType);
                        }
                        if (g == 0)
                            fprintf(file, "ADD LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                        if (g == 1)
                            fprintf(file, "SUB LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                        if (g == 3)
                            fprintf(file, "DIV LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                        if (g == 2)
                            fprintf(file, "MUL LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                    }
                    if (g == 4)
                        fprintf(file, "EQ LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                    if (g == 5)
                        fprintf(file, "LT LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                    if (g == 6)
                        fprintf(file, "GT LF@RES float@%lf LF@%s\n", op2.real, cstr_of_string(op1.string));
                    return 1;
                }

            } else {                       // op 2 is id
                if (op1.type == Integer) { // integer // test op1 real string or id or nil
                    pom1 = htSearch(local, cstr_of_string(op2.string));
                    if (pom1 == NULL) {
                        pom1 = htSearch(global, cstr_of_string(op2.string));
                    }
                    if (pom1 != NULL) {
                        if (pom1->data.type == String || (pom1->data.type == KwNil && pom1->data.define == true)) {
                            fprintf(stderr, "SemanticErrorType: string");
                            exit(SemanticErrorType);
                        }
                    }
                    g = getop(z1);
                    if (g < 4) {
                        if (op1.type == KwNil || op2.type == KwNil) {
                            fprintf(stderr, "SemanticErrorType: nil");
                            exit(SemanticErrorType);
                        }
                        if (g == 0)
                            fprintf(file, "ADD LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                        if (g == 1)
                            fprintf(file, "SUB LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                        if (g == 3)
                            fprintf(file, "DIV LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                        if (g == 2)
                            fprintf(file, "MUL LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                    }
                    if (g == 4)
                        fprintf(file, "EQ LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                    if (g == 5)
                        fprintf(file, "LT LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                    if (g == 6)
                        fprintf(file, "GT LF@RES LF@%s int@%d\n", cstr_of_string(op2.string), op1.integer);
                    if (pom1->data.define == false) {
                        pom1->data.type = Integer;
                        pom1->data.define = true;
                    }

                } else if (op1.type == String) { // string
                    pom1 = htSearch(local, cstr_of_string(op2.string));
                    if (pom1 == NULL) {
                        pom1 = htSearch(global, cstr_of_string(op2.string));
                    }
                    if (pom1 != NULL) {
                        if (pom1->data.type != String) {
                            fprintf(stderr, "SemanticErrorType: string");
                            exit(SemanticErrorType);
                        }
                        g = getop(z1);
                        if (g > 3) {
                            if (pom1->data.type != String)
                                fprintf(file, "%s", "MOVE LF@RES bool@%false\n");
                            else {
                                if (g == 4)
                                    fprintf(file, "EQ LF@RES LF@%s string@%s", cstr_of_string(op2.string),
                                            cstr_of_string(op1.string));
                                if (g == 5)
                                    fprintf(file, "LT LF@RES LF@%s string@%s", cstr_of_string(op2.string),
                                            cstr_of_string(op1.string));
                                if (g == 6)
                                    fprintf(file, "GT LF@RES LF@%s string@%s", cstr_of_string(op2.string),
                                            cstr_of_string(op1.string));
                            }
                        } else if (g == 0) {
                            if (pom1->data.type != String) {
                                fprintf(stderr, "SemanticErrorType");
                                exit(SemanticErrorType);
                            }
                            fprintf(file, "CONCAT LF@RES LF@%s strin@%s", cstr_of_string(op2.string),
                                    cstr_of_string(op1.string));
                        }
                    } else {
                        exit(SemanticErrorDefinition);
                    }
                } else { // real
                    pom1 = htSearch(local, cstr_of_string(op2.string));
                    if (op1.type == KwNil || op2.type == KwNil) {
                        fprintf(stderr, "SemanticErrorType: nil");
                        exit(SemanticErrorType);
                    }
                    d = true;
                    g = getop(z1);
                    if (g < 4) {
                        if (g == 0)
                            fprintf(file, "ADD LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                        if (g == 1)
                            fprintf(file, "SUB LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                        if (g == 3)
                            fprintf(file, "DIV LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                        if (g == 2)
                            fprintf(file, "MUL LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                    }
                    if (g == 4)
                        fprintf(file, "EQ LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                    if (g == 5)
                        fprintf(file, "LT LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                    if (g == 6)
                        fprintf(file, "GT LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), op1.real);
                    if (pom1->data.define == false) {
                        pom1->data.type = Real;
                        pom1->data.define = true;
                    }
                }
                return 1;
            }
        } else if (op1.type == String || op2.type == String) { // both op is string can do concat or rel op else error
            g = getop(z1);
            if (g == 0)
                fprintf(file, "CONCAT LF@RES string@%s string@%s", cstr_of_string(op1.string),
                        cstr_of_string(op2.string));
            else if (g == 4)
                fprintf(file, "EQ LF@RES string@%s string@%s", cstr_of_string(op1.string), cstr_of_string(op2.string));
            else if (g == 5)
                fprintf(file, "EQ LF@RES string@%s string@%s", cstr_of_string(op1.string), cstr_of_string(op2.string));
            else if (g == 6)
                fprintf(file, "EQ LF@RES string@%s string@%s", cstr_of_string(op1.string), cstr_of_string(op2.string));
            else {
                fprintf(stderr, "SemanticErrorType: string * string , string - string , string / string");
                exit(SemanticErrorType);
            }
            return 1;
        } else { // op1 and op2 is int or coulbe be nil
            g = getop(z1);
            if (g < 4) {
                if (op1.type == KwNil || op2.type == KwNil) {
                    fprintf(stderr, "SemanticErrorType: nil");
                    exit(SemanticErrorType);
                }
                if (g == 0)
                    fprintf(file, "ADD LF@RES int@%d int@%d", op2.integer, op1.integer);
                if (g == 1)
                    fprintf(file, "SUB LF@RES int@%d int@%d", op2.integer, op1.integer);
                if (g == 2)
                    fprintf(file, "MUL LF@RES int@%d int@%d", op2.integer, op1.integer);
                if (g == 3)
                    fprintf(file, "IDIV LF@RES int@%d int@%d", op2.integer, op1.integer);
                return 1;
            } else {
                if (g == 4)
                    fprintf(file, "EQ LF@RES int@%d int@%d", op2.integer, op1.integer);
                if (g == 5)
                    fprintf(file, "LT LF@RES int@%d int@%d", op2.integer, op1.integer);
                if (g == 6)
                    fprintf(file, "GT LF@RES int@%d int@%d", op2.integer, op1.integer);
                return 1;
            }
        }

    } else {                      //  3 or more terms need to reduce
        while (!stackEmpty(op)) { /*
                                     if(stackEmpty(tem))// 5*6 + 7 * 6
                                     {
                                         g = getop(z1);
                                         if(g == 0)
                                             fprintf(file,"ADD LF@RES LF@TMP LF@RES\n");
                                         if(g == 1)
                                             fprintf(file,"SUB LF@RES LF@TMP LF@RES\n");
                                         if(g == 4)
                                             fprintf(file,"EQ LF@RES LF@TMP LF@RES\n");
                                         if(g == 5)
                                             fprintf(file,"LT LF@RES LF@TMP LF@RES\n");
                                         if(g == 6)
                                             fprintf(file,"GT LF@RES LF@TMP LF@RES\n");
                                         return 1;
                                     }*/
            op1 = stackPop(tem);
            if (op1.type == ParenRight)
                paren(tem, op);
            z1 = stackPop(op);
            if (stackEmpty(tem)) // end last tem
            {
                if (op1.type == KwNil || op1.type == String) {
                    fprintf(stderr, "SemanticErrorType: string or nil");
                    exit(SemanticErrorType);
                }
                if (!stackEmpty(op)) // 7-89*7+8*9
                {
                    z2 = stackPop(op);
                    if (z1.type == OpAdd)
                        fprintf(file, "%s", "ADD LF@RES LF@TMP LF@RES\n");
                    else
                        fprintf(file, "%s", "SUB LF@RES LF@TMP LF@RES\n");
                    if (op1.type == Integer) {
                        if (tt == 1) {
                            if (z2.type == OpAdd)
                                fprintf(file, "ADD LF@RES LF@RES int@%d\n", op1.integer);
                            else
                                fprintf(file, "SUB LF@RES LF@RES int@%d\n", op1.integer);
                        } else {
                            if (z2.type == OpAdd)
                                fprintf(file, "ADD LF@RES int@%d LF@RES\n", op1.integer);
                            else
                                fprintf(file, "SUB LF@RES int@%d LF@RES\n", op1.integer);
                        }
                    }
                    if (op1.type == Id) {
                        pom1 = htSearch(local, cstr_of_string(op1.string));
                        if (pom1 == NULL)
                            pom1 = htSearch(global, cstr_of_string(op1.string));
                        if (pom1 != NULL) {
                            if (pom1->data.type == String || (pom1->data.define == true && pom1->data.type == KwNil)) {
                                fprintf(stderr, "SemanticErrorType:string");
                                exit(SemanticErrorType);
                            } else {
                                if (tt == 1) {
                                    if (z2.type == OpAdd)
                                        fprintf(file, "ADD LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                                    else
                                        fprintf(file, "SUB LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                                } else {
                                    if (z2.type == OpAdd)
                                        fprintf(file, "ADD LF@RES lf@%s LF@RES\n", cstr_of_string(op1.string));
                                    else
                                        fprintf(file, "SUB LF@RES lf@%s LF@RES\n", cstr_of_string(op1.string));
                                }
                            }
                        } else {
                            exit(SemanticErrorDefinition);
                        }
                    }
                    if (op1.type == Real) {
                        if (tt == 1) {
                            if (z2.type == OpAdd)
                                fprintf(file, "ADD LF@RES LF@RES float@%lf\n", op1.real);
                            else
                                fprintf(file, "SUB LF@RES LF@RES float@%lf\n", op1.real);
                        } else {
                            if (z2.type == OpAdd)
                                fprintf(file, "ADD LF@RES float@%lf LF@RES\n", op1.real);
                            else
                                fprintf(file, "SUB LF@RES float@%lf LF@RES\n", op1.real);
                        }
                    }
                    return 1;
                } else {
                    if (op1.type == String) {
                        fprintf(stderr, "SemanticError: error type %s '%s'\n", debug_token_name(op1),
                                cstr_of_string(op1.string));
                        exit(SemanticErrorType);
                    }
                    if (op1.type == Integer) {
                        g = getop(z1);
                        if (tt == 1) {
                            if (g == 0)
                                fprintf(file, "ADD LF@RES LF@RES int@%d\n", op1.integer);
                            if (g == 1)
                                fprintf(file, "SUB LF@RES LF@RES int@%d\n", op1.integer);
                            if (g == 2)
                                fprintf(file, "MUL LF@RES LF@RES int@%d\n", op1.integer);
                            if (g == 3)
                                fprintf(file, "IDIV LF@RES LF@RES int@%d\n", op1.integer);
                            if (g == 4)
                                fprintf(file, "EQ LF@RES LF@RES int@%d\n", op1.integer);
                            if (g == 5)
                                fprintf(file, "LT LF@RES LF@RES int@%d\n", op1.integer);
                            if (g == 6)
                                fprintf(file, "GT LF@RES LF@RES int@%d\n", op1.integer);
                        } else {
                            if (g == 0)
                                fprintf(file, "ADD LF@RES int@%d LF@RES\n", op1.integer);
                            if (g == 1)
                                fprintf(file, "SUB LF@RES int@%d LF@RES\n", op1.integer);
                            if (g == 2)
                                fprintf(file, "MUL LF@RES int@%d LF@RES\n", op1.integer);
                            if (g == 3)
                                fprintf(file, "IDIV LF@RES int@%d LF@RES\n", op1.integer);
                            if (g == 4)
                                fprintf(file, "EQ LF@RES int@%d LF@RES\n", op1.integer);
                            if (g == 5)
                                fprintf(file, "LT LF@RES int@%d LF@RES\n", op1.integer);
                            if (g == 6)
                                fprintf(file, "GT LF@RES int@%d LF@RES\n", op1.integer);
                        }
                    }
                    if (op1.type == Real) {
                        d = true;
                        if (tt == 1) {
                            if (g == 0)
                                fprintf(file, "ADD LF@RES LF@RES float@%lf\n", op1.real);
                            if (g == 1)
                                fprintf(file, "SUB LF@RES LF@RES float@%lf\n", op1.real);
                            if (g == 2)
                                fprintf(file, "MUL LF@RES LF@RES float@%lf\n", op1.real);
                            if (g == 3)
                                fprintf(file, "DIV LF@RES LF@RES float@%lf\n", op1.real);
                            if (g == 4)
                                fprintf(file, "EQ LF@RES LF@RES float@%lf\n", op1.real);
                            if (g == 5)
                                fprintf(file, "LT LF@RES LF@RES float@%lf\n", op1.real);
                            if (g == 6)
                                fprintf(file, "GT LF@RES LF@RES float@%lf\n", op1.real);
                        } else {
                            if (g == 0)
                                fprintf(file, "ADD LF@RES float@%lf LF@RES\n", op1.real);
                            if (g == 1)
                                fprintf(file, "SUB LF@RES float@%lf LF@RES\n", op1.real);
                            if (g == 2)
                                fprintf(file, "MUL LF@RES float@%lf LF@RES\n", op1.real);
                            if (g == 3)
                                fprintf(file, "DIV LF@RES float@%lf LF@RES\n", op1.real);
                            if (g == 4)
                                fprintf(file, "EQ LF@RES float@%lf LF@RES\n", op1.real);
                            if (g == 5)
                                fprintf(file, "LT LF@RES float@%lf LF@RES\n", op1.real);
                            if (g == 6)
                                fprintf(file, "GT LF@RES float@%lf LF@RES \n", op1.real);
                        }
                    }
                    if (op1.type == Id) {
                        if (g < 4) {
                            pom1 = htSearch(local, cstr_of_string(op1.string));
                            if (pom1 == NULL)
                                pom1 = htSearch(global, cstr_of_string(op1.string));
                            if (pom1 != NULL) {
                                if (pom1->data.type == String ||
                                    (pom1->data.define == true && pom1->data.type == KwNil)) {
                                    fprintf(stderr, "SemanticErrorType:string");
                                    exit(SemanticErrorType);
                                }
                            }
                        }
                        if (tt == 1) {
                            if (g == 0)
                                fprintf(file, "ADD LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            if (g == 1)
                                fprintf(file, "SUB LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            if (g == 2)
                                fprintf(file, "MUL LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            if (g == 3)
                                fprintf(file, "IDIV LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            if (g == 4)
                                fprintf(file, "EQ LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            if (g == 5)
                                fprintf(file, "LT LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            if (g == 6)
                                fprintf(file, "GT LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                        } else {
                            if (g == 0)
                                fprintf(file, "ADD LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            if (g == 1)
                                fprintf(file, "SUB LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            if (g == 2)
                                fprintf(file, "MUL LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            if (g == 3)
                                fprintf(file, "IDIV LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            if (g == 4)
                                fprintf(file, "EQ LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            if (g == 5)
                                fprintf(file, "LT LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            if (g == 6)
                                fprintf(file, "GT LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                        }
                    }
                    return 1;
                }

            } else {
                z2 = stackPop(op);
                op2 = stackPop(tem);
                if (op1.type == String || op2.type == String) {
                    fprintf(stderr, "SemanticError: error type %s '%s'\n", debug_token_name(op1),
                            cstr_of_string(op1.string));
                    fprintf(stderr, "op1 type %d, op2 type %d\n", op1.type, op2.type);
                    exit(SemanticErrorType);
                }
                if ((num_of_prec_table(z1) == 2 || num_of_prec_table(z1) == 3) ||
                    (num_of_prec_table(z2) == 2 || num_of_prec_table(z2) == 3)) {
                    if (num_of_prec_table(z1) > 1) {
                        l = 1;
                        op3 = stackPop(tem);
                        rel = z1;
                        if (op3.type == Id || op2.type == Id) {
                            if (op2.type == Id || op3.type == Real) {
                                if (op2.type == Integer)
                                    d2 = givedouble(op2.integer);
                                else
                                    d2 = op2.real;
                                if (op3.type == Integer)
                                    d3 = givedouble(op3.integer);
                                else
                                    d3 = op3.real;
                                if (op2.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                    }
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@TEST float@%lf LF@%s\n", d3, cstr_of_string(op2.string));
                                    if (z2.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST float@%lf LF@%s\n", d3, cstr_of_string(op2.string));
                                    if (z2.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST float@%lf LF@%s\n", d3, cstr_of_string(op2.string));
                                    if (z2.type == OpSub)
                                        fprintf(file, "SUB LF@TEST float@%lf LF@%s\n", d3, cstr_of_string(op2.string));

                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op3.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op3.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@TEST LF@%s float@%lf\n", cstr_of_string(op3.string), d2);
                                    if (z2.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST LF@%s float@%lf\n", cstr_of_string(op3.string), d2);
                                    if (z2.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST LF@%s float@%lf\n", cstr_of_string(op3.string), d2);
                                    if (z2.type == OpSub)
                                        fprintf(file, "SUB LF@TEST LF@%s float@%lf\n", cstr_of_string(op3.string), d2);
                                }
                            } else {
                                if (op3.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op3.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op3.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Integer;
                                        pom1->data.define = true;
                                    }
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@TEST LF@%s int@%d\n", cstr_of_string(op3.string),
                                                op2.integer);
                                    if (z2.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST LF@%s int@%d\n", cstr_of_string(op3.string),
                                                op2.integer);
                                    if (z2.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST LF@%s int@%d\n", cstr_of_string(op3.string),
                                                op2.integer);
                                    if (z2.type == OpSub)
                                        fprintf(file, "SUB LF@TEST LF@%s int@%d\n", cstr_of_string(op3.string),
                                                op2.integer);

                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Integer;
                                        pom1->data.define = true;
                                    }
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@TEST int@%d LF@%s\n", op3.integer,
                                                cstr_of_string(op2.string));
                                    if (z2.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST int@%d LF@%s\n", op3.integer,
                                                cstr_of_string(op2.string));
                                    if (z2.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST int@%d LF@%s\n", op3.integer,
                                                cstr_of_string(op2.string));
                                    if (z2.type == OpSub)
                                        fprintf(file, "SUB LF@TEST int@%d LF@%s\n", op3.integer,
                                                cstr_of_string(op2.string));
                                }
                            }
                        } else {
                            if (op2.type == Real || op3.type == Real) {
                                if (op2.type == Integer)
                                    d2 = givedouble(op2.integer);
                                else
                                    d2 = op2.real;
                                if (op3.type == Integer)
                                    d3 = givedouble(op3.integer);
                                else
                                    d3 = op3.real;
                                if (z2.type == OpAdd)
                                    fprintf(file, "ADD LF@TEST float@%lf float@%lf\n", d3, d2);
                                if (z2.type == OpSub)
                                    fprintf(file, "SUB LF@TEST float@%lf float@%lf\n", d3, d2);
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@TEST float@%lf float@%lf\n", d3, d2);
                                if (z2.type == OpDiv)
                                    fprintf(file, "DIV LF@TEST float@%lf float@%lf\n", d3, d2);
                            } else {
                                if (z2.type == OpAdd)
                                    fprintf(file, "ADD LF@TEST int@%d int@%d\n", op3.integer, op2.integer);
                                if (z2.type == OpSub)
                                    fprintf(file, "SUB LF@TEST int@%d int@%d\n", op3.integer, op2.integer);
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@TEST int@%d int@%d\n", op3.integer, op2.integer);
                                if (z2.type == OpDiv)
                                    fprintf(file, "IDIV LF@TEST int@%d int@%d\n", op3.integer, op2.integer);
                            }
                        }
                        stackPush(tem, op1);
                    } else {
                        rel = z2;
                        if (op1.type == Id || op2.type == Id) {
                            if (op1.type == Real || op2.type == Real) {
                                if (op1.type == Integer)
                                    d1 = givedouble(op1.integer);
                                else
                                    d1 = op1.real;
                                if (op2.type == Integer)
                                    d2 = givedouble(op2.integer);
                                else
                                    d2 = op2.real;
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@TEST float@%lf LF@%s\n", d2, cstr_of_string(op1.string));
                                    if (z1.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST float@%lf LF@%s\n", d2, cstr_of_string(op1.string));
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST float@%lf LF@%s\n", d2, cstr_of_string(op1.string));
                                    if (z1.type == OpSub)
                                        fprintf(file, "SUB LF@TEST float@%lf LF@%s\n", d2, cstr_of_string(op1.string));

                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@TEST LF@%s float@%lf\n", cstr_of_string(op2.string), d1);
                                    if (z1.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST LF@%s float@%lf\n", cstr_of_string(op2.string), d1);
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST LF@%s float@%lf\n", cstr_of_string(op2.string), d1);
                                    if (z1.type == OpSub)
                                        fprintf(file, "SUB LF@TEST LF@%s float@%lf\n", cstr_of_string(op2.string), d1);
                                }
                            } else {
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Integer;
                                        pom1->data.define = true;
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@TEST int@%d LF@%s\n", op2.integer,
                                                cstr_of_string(op1.string));
                                    if (z1.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST int@%d LF@%s\n", op2.integer,
                                                cstr_of_string(op1.string));
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST int@%d LF@%s\n", op2.integer,
                                                cstr_of_string(op1.string));
                                    if (z1.type == OpSub)
                                        fprintf(file, "SUB LF@TEST int@%d LF@%s\n", op2.integer,
                                                cstr_of_string(op1.string));

                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Integer;
                                        pom1->data.define = true;
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@TEST LF@%s int@%d\n", cstr_of_string(op2.string),
                                                op1.integer);
                                    if (z1.type == OpDiv)
                                        fprintf(file, "DIV LF@TEST LF@%s int@%d\n", cstr_of_string(op2.string),
                                                op1.integer);
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@TEST LF@%s int@%d\n", cstr_of_string(op2.string),
                                                op1.integer);
                                    if (z1.type == OpSub)
                                        fprintf(file, "SUB LF@TEST LF@%s int@%d\n", cstr_of_string(op2.string),
                                                op1.integer);
                                }
                            }
                        } else {
                            if (op1.type == Real || op2.type == Real) {
                                if (op1.type == Integer)
                                    d1 = givedouble(op1.integer);
                                else
                                    d1 = op1.real;
                                if (op2.type == Integer)
                                    d2 = givedouble(op2.integer);
                                else
                                    d2 = op2.real;
                                if (z1.type == OpAdd)
                                    fprintf(file, "ADD LF@TEST float@%lf float@%lf\n", d2, d1);
                                if (z1.type == OpSub)
                                    fprintf(file, "SUB LF@TEST float@%lf float@%lf\n", d2, d1);
                                if (z1.type == OpMul)
                                    fprintf(file, "MUL LF@TEST float@%lf float@%lf\n", d2, d1);
                                if (z1.type == OpDiv)
                                    fprintf(file, "DIV LF@TEST float@%lf float@%lf\n", d2, d1);
                            } else {
                                if (z1.type == OpAdd)
                                    fprintf(file, "ADD LF@TEST int@%d int@%d\n", op2.integer, op1.integer);
                                if (z1.type == OpSub)
                                    fprintf(file, "SUB LF@TEST int@%d int@%d\n", op2.integer, op1.integer);
                                if (z1.type == OpMul)
                                    fprintf(file, "MUL LF@TEST int@%d int@%d\n", op2.integer, op1.integer);
                                if (z1.type == OpDiv)
                                    fprintf(file, "IDIV LF@TEST int@%d int@%d\n", op2.integer, op1.integer);
                            }
                        }
                    }
                } else if (num_of_prec_table(z1) > num_of_prec_table(z2)) // z2 = mul/div z1 = + -
                {
                    if (op2.type == String || op1.type == String) {
                        fprintf(stderr, "SemanticErrorType:string");
                        exit(SemanticErrorType);
                    }
                    if (op1.type == KwNil || op2.type == KwNil) { // trying to do term */ nil
                        fprintf(stderr, "SemanticErrorType:nil");
                        exit(SemanticErrorType);
                    }
                    // cmul = 1;
                    if (start == 0) {
                        op3 = stackPop(tem);
                        if (op3.type == String) {
                            fprintf(stderr, "SemanticErrorType:");
                            exit(SemanticErrorType);
                        }
                        if (op2.type == Real || op3.type == Real) { // testing type of each op(int , real string or id)
                            d = true;
                            if (op2.type == Integer)
                                d2 = givedouble(op2.integer);
                            else
                                d2 = op2.real;
                            if (op3.type == Integer)
                                d3 = givedouble(op3.integer);
                            else
                                d3 = op3.real;
                            if (op2.type == Id || op3.type == Id) {
                                if (op2.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@RES float@%lf LF@%s\n", d3, cstr_of_string(op2.string));
                                    else
                                        fprintf(file, "DIV LF@RES float@%lf LF@%s\n", d3, cstr_of_string(op2.string));
                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op3.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op3.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@RES LF@%s float@%lf\n", cstr_of_string(op3.string), d2);
                                    else
                                        fprintf(file, "DIV LF@RES LF@%s float@%lf\n", cstr_of_string(op3.string), d2);
                                }
                            } else {
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@RES float@%lf float@%lf\n", d2, d3);
                                else
                                    fprintf(file, "DIV LF@RES float@%lf float@%lf\n", d2, d3);
                            }
                        } else {
                            if (op2.type == Id || op3.type == Id) {
                                if (op2.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (op3.type == Real) {
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL float@%lf LF@%s\n", op3.real,
                                                    cstr_of_string(op2.string));
                                        else
                                            fprintf(file, "IDIV float@%lf LF@%s\n", op3.real,
                                                    cstr_of_string(op2.string));
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                    } else {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL int@%d LF@%s\n", op3.integer,
                                                    cstr_of_string(op2.string));
                                        else
                                            fprintf(file, "IDIV int@%d LF@%s\n", op3.integer,
                                                    cstr_of_string(op2.string));
                                    }

                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op3.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op3.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (op2.type == Real) {
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL LF@%s float@%lf\n", cstr_of_string(op3.string),
                                                    op2.real);
                                        else
                                            fprintf(file, "IDIV LF@%s float@%lf\n", cstr_of_string(op3.string),
                                                    op2.real);
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                    } else {
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL LF@%s int@%d\n", cstr_of_string(op3.string),
                                                    op2.integer);
                                        else
                                            fprintf(file, "IDIV LF@%s int@%d\n", cstr_of_string(op3.string),
                                                    op2.integer);
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                    }
                                }

                            } else {
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@RES int@%d int@%d\n", op3.integer, op2.integer);
                                else
                                    fprintf(file, "IDIV LF@RES int@%d int@%d\n", op3.integer, op2.integer);
                            }
                            stackPush(tem, op1);
                        }
                        tt = 1;
                        start = 1;
                        stackPush(op, z1);
                    } else {
                        if (cmul != 1 && tt == 1) {
                            if (op2.type == Integer) {
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@RES LF@RES int@%d\n", op2.integer);
                                else
                                    fprintf(file, "IDIV LF@RES LF@RES int@%d\n", op2.integer);
                            }
                            if (op2.type == Real) {
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@RES LF@RES float@%lf\n", op2.real);
                                else
                                    fprintf(file, "DIV LF@RES LF@RES float@%lf\n", op2.real);
                            }
                            if (op2.type == Id) {
                                pom1 = htSearch(local, cstr_of_string(op2.string));
                                if (pom1 == NULL)
                                    pom1 = htSearch(global, cstr_of_string(op2.string));
                                if (pom1 != NULL) {
                                    if (pom1->data.type == String ||
                                        (pom1->data.define == true && pom1->data.type == KwNil)) {
                                        fprintf(stderr, "SemanticErrorType:string");
                                        exit(SemanticErrorType);
                                    }
                                }
                                if (z2.type == OpMul)
                                    fprintf(file, "MUL LF@RES LF@RES LF@%s\n", cstr_of_string(op2.string));
                                else
                                    fprintf(file, "DIV LF@RES LF@RES LF@%s\n", cstr_of_string(op2.string));
                            }
                            stackPush(tem, op1);
                            stackPush(op, z1);
                            cmul = 0;
                        } else {
                            if (op1.type == Id || op2.type == Id) {
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (op2.type == Real) {
                                        d = true;
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL LF@TMP float@%lf LF@%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "DIV LF@TMP float@%lf LF@%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                    } else {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL LF@TMP int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "DIV LF@TMP int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                    }
                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (op1.type == Integer) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL LF@TMP LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                        else
                                            fprintf(file, "IDIV LF@TMP LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                    } else {
                                        d = true;
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                        if (z2.type == OpMul)
                                            fprintf(file, "MUL LF@TMP LF@%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                        else
                                            fprintf(file, "IDIV LF@TMP LF@%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                    }
                                }
                            } else {
                                if (op1.type == Real || op1.type == Real) {
                                    d = true;
                                    if (op1.type == Integer)
                                        d1 = givedouble(op1.integer);
                                    else
                                        d1 = op1.real;
                                    if (op2.type == Integer)
                                        d2 = givedouble(op2.integer);
                                    else
                                        d2 = op2.real;
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@TMP float@%lf float@%lf\n", d2, d1);
                                    else
                                        fprintf(file, "DIV LF@TMP float@%lf float@%lf\n", d2, d1);
                                } else {
                                    if (z2.type == OpMul)
                                        fprintf(file, "MUL LF@TMP int@%d int@%d\n", op2.integer, op1.integer);
                                    else
                                        fprintf(file, "IDIV LF@TMP int@%d int@%d\n", op2.integer, op1.integer);
                                }
                            }
                            if (stackEmpty(op)) {
                                if (z1.type == OpAdd)
                                    fprintf(file, "%s", "ADD LF@RES LF@TMP LF@RES\n");
                                else
                                    fprintf(file, "%s", "SUB LF@RES LF@TMP LF@RES\n");
                                return 1;
                            } else {
                                pom = stackPop(op);
                                while (num_of_prec_table(pom) < 1) { // doing term*term*term+ term*term
                                    op1 = stackPop(tem);
                                    if (op1.type == Id) {
                                        pom1 = htSearch(local, cstr_of_string(op1.string));
                                        if (pom1 == NULL)
                                            pom1 = htSearch(global, cstr_of_string(op1.string));
                                        if (pom1 != NULL) {
                                            if (pom1->data.type == String ||
                                                (pom1->data.define == true && pom1->data.type == KwNil)) {
                                                fprintf(stderr, "SemanticErrorType:string");
                                                exit(SemanticErrorType);
                                            }
                                        }
                                        if (pom.type == OpMul)
                                            fprintf(file, "MUL LF@TMP LF@TMP LF@%s", cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "IDIV LF@TMP LF@TMP LF@%s", cstr_of_string(op1.string));
                                    }
                                    if (op1.type == Real) {
                                        if (pom.type == OpMul)
                                            fprintf(file, "MUL LF@TMP LF@TMP float@%lf\n", op1.real);
                                        else
                                            fprintf(file, "DIV LF@TMP LF@TMP float@%lf\n", op1.real);
                                    }
                                    if (op1.type == Integer) {
                                        if (pom.type == OpMul)
                                            fprintf(file, "MUL LF@TMP LF@TMP int@%d\n", op1.integer);
                                        else
                                            fprintf(file, "IDIV LF@TMP LF@TMP int@%d\n", op1.integer);
                                    }
                                    if (stackEmpty(op)) {
                                        // if(stackEmpty(tem))
                                        if (z1.type == OpAdd)
                                            fprintf(file, "%s", "ADD LF@RES LF@TMP LF@RES\n");
                                        else
                                            fprintf(file, "%s", "SUB LF@RES LF@TMP LF@RES\n");
                                        return 1;
                                    }
                                    pom = stackPop(op);
                                }
                                stackPush(op, pom);
                                stackPush(op, z1);
                            }
                            cmul = 1;
                            cadd = 0;
                        }
                    }

                } else if (num_of_prec_table(z2) > num_of_prec_table(z1)) // z1 = op mul z2 = add sub
                {
                    if (op2.type == String || op1.type == String) {
                        fprintf(stderr, "SemanticErrorType:string");
                        exit(SemanticErrorType);
                    }
                    if (op1.type == KwNil || op2.type == KwNil) {
                        fprintf(stderr, "SemanticErrorType:nil");
                        exit(SemanticErrorType);
                    }
                    if (start == 0) {
                        if (op1.type == Real || op2.type == Real) { // testing type
                            d = true;
                            if (op1.type == Integer)
                                d1 = givedouble(op1.integer);
                            else
                                d1 = op2.real;
                            if (op2.type == Integer)
                                d2 = givedouble(op2.integer);
                            else
                                d2 = op2.real;
                            if (op1.type == Id || op2.type == Id) {
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES float@%lf LF@%s\n", d2, cstr_of_string(op1.string));
                                    else
                                        fprintf(file, "IDIV LF@RES float@%lf LF@%s\n", d2, cstr_of_string(op1.string));
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), d1);
                                    else
                                        fprintf(file, "IDIV LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string), d1);
                                    if (pom1->data.define == false) {
                                        pom1->data.type = Real;
                                        pom1->data.define = true;
                                    }
                                }
                            } else {
                                if (z1.type == OpMul)
                                    fprintf(file, "MUL LF@RES float@%lf float@%lf\n", d2, d1);
                                else
                                    fprintf(file, "DIV LF@RES float@%lf float@%lf\n", d2, d1);
                            }

                        } else {
                            if (op1.type == Id || op2.type == Id) {
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (op2.type == Real) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES float@%lf LF@%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "IDIV LF@RES float@%lf LF@%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                    } else {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "IDIV LF@RES int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                    }
                                } else {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (op1.type == Real) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                        else
                                            fprintf(file, "DIV LF@RES LF@%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                    } else {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                        else
                                            fprintf(file, "DIV LF@RES LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                    }
                                }
                            } else {
                                if (z1.type == OpMul)
                                    fprintf(file, "MUL LF@RES int@%d int@%d\n", op2.integer, op1.integer);
                                else
                                    fprintf(file, "IDIV LF@RES int@%d int@%d\n", op2.integer, op1.integer);
                            }
                        }
                        cmul = 1;
                        // stackPush(op,z2);
                    } else {
                        if (op1.type == Id) {
                            pom1 = htSearch(local, cstr_of_string(op1.string));
                            if (pom1 == NULL)
                                pom1 = htSearch(global, cstr_of_string(op1.string));
                            if (pom1 != NULL) {
                                if (pom1->data.type == String ||
                                    (pom1->data.define == true && pom1->data.type == KwNil)) {
                                    fprintf(stderr, "SemanticErrorType:string");
                                    exit(SemanticErrorType);
                                }
                            }
                            if (z1.type == OpMul)
                                fprintf(file, "MUL LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                            else
                                fprintf(file, "DIV LF@RES LF@%s LF@RES\n", cstr_of_string(op1.string));
                        }
                        if (op1.type == Real) {
                            if (z1.type == OpMul)
                                fprintf(file, "MUL LF@RES float@%lf LF@RES\n", op1.real);
                            else
                                fprintf(file, "DIV LF@RES float@%lf LF@RES\n", op1.real);
                        }
                        if (op1.type == Integer) {
                            if (z1.type == OpMul)
                                fprintf(file, "MUL LF@RES int@%d LF@RES\n", op1.integer);
                            else
                                fprintf(file, "IDIV LF@RES int@%d LF@RES\n", op1.integer);
                        }
                        stackPush(tem, op2);
                    }
                    stackPush(op, z2);
                    start = 1;
                } else // ++ or **
                {
                    if (op1.type == KwNil || op2.type == KwNil) {
                        fprintf(stderr, "SemanticErrorType:nil");
                        exit(SemanticErrorType);
                    }
                    if (z1.type == OpMul || z1.type == OpDiv) // * / nebo * *
                    {
                        if (op2.type == String || op1.type == String) {
                            fprintf(stderr, "SemanticErrorType:string");
                            exit(SemanticErrorType);
                        }
                        cmul = 1;
                        cadd = 0;
                        if (start == 0) {
                            if (op1.type == Id || op2.type == Id) { // testing type
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                }
                                if (op2.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                }
                                if (op1.type == Id && op2.type == Id) {
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES LF@%s LF@%s\n", cstr_of_string(op2.string),
                                                cstr_of_string(op1.string));
                                    else
                                        fprintf(file, "DIV LF@RES LF@%s LF@%s\n", cstr_of_string(op2.string),
                                                cstr_of_string(op1.string));
                                } else if (op1.type == Real || op2.type == Real) {
                                    if (op1.type == Id) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES float@%lf LF%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "DIV LF@RES float@%lf LF%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                    } else {
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES LF%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                        else
                                            fprintf(file, "DIV LF@RES LF%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                    }
                                } else {
                                    if (op1.type == Id) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "IDIV LF@RES int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                    } else {
                                        if (z1.type == OpMul)
                                            fprintf(file, "MUL LF@RES LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                        else
                                            fprintf(file, "IDIV LF@RES LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                    }
                                }
                            } else {
                                if (op1.type == Real || op2.type == Real) { // testing type
                                    if (op1.type == Integer)
                                        d1 = givedouble(op1.integer);
                                    else
                                        d1 = op1.real;
                                    if (op2.type == Integer)
                                        d2 = givedouble(op2.integer);
                                    else
                                        d2 = op2.real;
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES float@%lf float@%lf\n", d2, d1);
                                    else
                                        fprintf(file, "DIV LF@RES float@%lf float@%lf\n", d2, d1);
                                } else {
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES int@%d int@%d\n", op2.integer, op1.integer);
                                    else
                                        fprintf(file, "IDIV LF@RES int@%d int@%d\n", op2.integer, op1.integer);
                                }
                            }
                            start = 1;
                        } else {
                            if (cmul == 1) { // doing sequence of mul 5*7*8*9*** only use 1op
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                                    else
                                        fprintf(file, "IDIV LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                                } else if (op1.type == Real) {
                                    d = true;
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES LF@RES float@%lf\n", op1.real);
                                    else
                                        fprintf(file, "DIV LF@RES LF@RES float@%lf\n", op1.real);
                                } else {
                                    if (z1.type == OpMul)
                                        fprintf(file, "MUL LF@RES LF@RES int@%d\n", op1.integer);
                                    else
                                        fprintf(file, "IDIV LF@RES LF@RES int@%d\n", op1.integer);
                                }
                                stackPush(tem, op2); // returning op2 to stack
                                cadd = 0;
                            }
                        }
                        stackPush(op, z2); // returning z2 to stack
                    } else                 // + - nebo + + nebo - -
                    {
                        if (op1.type == KwNil || op2.type == KwNil) {
                            fprintf(stderr, "SemanticErrorType:nil");
                            exit(SemanticErrorType);
                        }
                        cadd = 1;
                        cmul = 0;
                        if (start == 0) {
                            if (op1.type == Id || op2.type == Id) { // type testing
                                if (op1.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op1.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op1.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                }
                                if (op2.type == Id) {
                                    pom1 = htSearch(local, cstr_of_string(op2.string));
                                    if (pom1 == NULL)
                                        pom1 = htSearch(global, cstr_of_string(op2.string));
                                    if (pom1 != NULL) {
                                        if (pom1->data.type == String ||
                                            (pom1->data.define == true && pom1->data.type == KwNil)) {
                                            fprintf(stderr, "SemanticErrorType:string");
                                            exit(SemanticErrorType);
                                        }
                                    }
                                }
                                if (op1.type == Id && op2.type == Id) {
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@RES LF@%s LF@%s\n", cstr_of_string(op2.string),
                                                cstr_of_string(op1.string));
                                    else
                                        fprintf(file, "SUB LF@RES LF@%s LF@%s\n", cstr_of_string(op2.string),
                                                cstr_of_string(op1.string));
                                } else if (op1.type == Real || op2.type == Real) {
                                    if (op1.type == Id) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Real;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpAdd)
                                            fprintf(file, "ADD LF@RES float@%lf LF%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "SUB LF@RES float@%lf LF%s\n", op2.real,
                                                    cstr_of_string(op1.string));
                                    } else {
                                        if (z1.type == OpAdd)
                                            fprintf(file, "ADD LF@RES LF%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                        else
                                            fprintf(file, "SUB LF@RES LF%s float@%lf\n", cstr_of_string(op2.string),
                                                    op1.real);
                                    }
                                } else {
                                    if (op1.type == Id) {
                                        if (pom1->data.define == false) {
                                            pom1->data.type = Integer;
                                            pom1->data.define = true;
                                        }
                                        if (z1.type == OpAdd)
                                            fprintf(file, "ADD LF@RES int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                        else
                                            fprintf(file, "SUB LF@RES int@%d LF@%s\n", op2.integer,
                                                    cstr_of_string(op1.string));
                                    } else {
                                        if (z1.type == OpAdd)
                                            fprintf(file, "ADD LF@RES LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                        else
                                            fprintf(file, "SUB LF@RES LF@%s int@%d\n", cstr_of_string(op2.string),
                                                    op1.integer);
                                    }
                                }
                            } else {
                                if (op1.type == Real || op2.type == Real) {
                                    if (op1.type == Integer)
                                        d1 = givedouble(op1.integer);
                                    else
                                        d1 = op1.real;
                                    if (op2.type == Integer)
                                        d2 = givedouble(op2.integer);
                                    else
                                        d2 = op2.real;
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@RES float@%lf float@%lf\n", d2, d1);
                                    else
                                        fprintf(file, "ADD LF@RES float@%lf float@%lf\n", d2, d1);
                                } else {
                                    if (z1.type == OpAdd)
                                        fprintf(file, "ADD LF@RES int@%d int@%d\n", op2.integer, op1.integer);
                                    else
                                        fprintf(file, "SUB LF@RES int@%d int@%d\n", op2.integer, op1.integer);
                                }
                            }
                            start = 1;
                        } else {
                            // sequence of add or sub 5-8-7-9-...
                            if (op1.type == String) {
                                fprintf(stderr, "SemanticErrorType: type");
                                exit(SemanticErrorType);
                            }
                            if (op1.type == Id) {
                                pom1 = htSearch(local, cstr_of_string(op1.string));
                                if (pom1 == NULL)
                                    pom1 = htSearch(global, cstr_of_string(op1.string));
                                if (pom1 != NULL) {
                                    if (pom1->data.type == String ||
                                        (pom1->data.define == true && pom1->data.type == KwNil)) {
                                        fprintf(stderr, "SemanticErrorType:string");
                                        exit(SemanticErrorType);
                                    }
                                }
                                if (z1.type == OpAdd)
                                    fprintf(file, "ADD LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                                else
                                    fprintf(file, "ADD LF@RES LF@RES LF@%s\n", cstr_of_string(op1.string));
                            } else if (op1.type == Real) {
                                d = true;
                                if (z1.type == OpAdd)
                                    fprintf(file, "ADD LF@RES LF@RES float@%lf\n", op1.real);
                                else
                                    fprintf(file, "SUB LF@RES LF@RES float@%lf\n", op1.real);
                            } else {
                                if (z1.type == OpAdd)
                                    fprintf(file, "ADD LF@RES LF@RES int@%d\n", op1.integer);
                                else
                                    fprintf(file, "SUB LF@RES LF@RES int@%d\n", op1.integer);
                            }
                            stackPush(tem, op2); // returning op2 to stack
                        }
                        stackPush(op, z2);
                        // return z2 to stack
                    }
                }
            }
        }
        if (!stackEmpty(tem)) // term rel op term + term etc
        {
            op1 = stackPop(tem);
            if (op1.type == String) {
                fprintf(stderr, "SemanTicErrorType: string");
                exit(SemanticErrorType);
            }
            if (op1.type == KwNil) {
                fprintf(stderr, "SemanticErrorType:nil");
                exit(SemanticErrorType);
            }
            g = getop(rel);
            if (l == 1) {
                if (op1.type == String) {
                    fprintf(file, "%s", "MOVE LF@RES bool@false\n");
                }
                if (op1.type == Real) {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES LF@TEST float@%lf\n", op1.real);
                    if (g == 5)
                        fprintf(file, "LT LF@RES LF@TEST float@%lf\n", op1.real);
                    if (g == 6)
                        fprintf(file, "GT LF@RES LF@TEST float@%lf\n", op1.real);
                } else if (op1.type == Id) {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES LF@TEST LF@%s\n", cstr_of_string(op1.string));
                    if (g == 5)
                        fprintf(file, "LT LF@RES LF@TEST LF@%s\n", cstr_of_string(op1.string));
                    if (g == 6)
                        fprintf(file, "GT LF@RES LF@TEST LF@%s\n", cstr_of_string(op1.string));
                } else {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES LF@TEST int@%d\n", op1.integer);
                    if (g == 5)
                        fprintf(file, "LT LF@RES LF@TEST int@%d\n", op1.integer);
                    if (g == 6)
                        fprintf(file, "GT LF@RES LF@TEST int@%d\n", op1.integer);
                }
            } else {
                if (op1.type == String) {
                    fprintf(file, "%s", "MOVE LF@RES bool@false\n");
                }
                if (op1.type == Real) {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES float@%lf LF@TEST\n", op1.real);
                    if (g == 5)
                        fprintf(file, "LT LF@RES float@%lf LF@TEST\n", op1.real);
                    if (g == 6)
                        fprintf(file, "GT LF@RES float@%lf LF@TEST\n", op1.real);
                } else if (op1.type == Id) {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES LF@%s LF@TEST\n", cstr_of_string(op1.string));
                    if (g == 5)
                        fprintf(file, "LT LF@RES LF@%s LF@TEST\n", cstr_of_string(op1.string));
                    if (g == 6)
                        fprintf(file, "GT LF@RES LF@%s LF@TEST\n", cstr_of_string(op1.string));
                } else {
                    if (g == 4)
                        fprintf(file, "EQ LF@RES int@%d LF@TEST\n", op1.integer);
                    if (g == 5)
                        fprintf(file, "LT LF@RES int@%d LF@TEST\n", op1.integer);
                    if (g == 6)
                        fprintf(file, "GT LF@RES int@%d LF@TEST\n", op1.integer);
                }
            }
        }
    }
    return 1;
}
int paren(tStack* tem, tStack* op)
{
    token_t tok, z;
    return 1;
}
