// Projekt IFJ 2018 - Prekladac podmnoziny jazyka Ruby do ciloveho jazyka IFJcode18
// David Nguyen <xnguye11@stud.fit.vutbr.cz>
// Radovan Guzik <xguzik00@stud.fit.vutbr.cz>
// Tomas Pribyl <xpriby17@stud.fit.vutbr.cz>
#pragma once
#include "lexer.h"

void gen_init(void);
void gen_loop_head(int depth);
void gen_loop_end(int depth);
void gen_branch_if(int depth);
void gen_branch_else(int depth);
void gen_branch_end(int depth);
void gen_save_expr_res(void);
void gen_save_expr_res_nil(void);
void gen_main_begin(void);
void gen_main_end(void);
void gen_return_val(const char* fun_name);
void gen_var_def(const char* n);
void gen_assgn(const char* lhs, const char* rhs);
void gen_func_head(const char* n);
void gen_func_add_param(const char* n, int i);
void gen_func_end(const char* n);
void gen_funcall_head(void);
void gen_funcall(const char* n);
void gen_funcall_add_arg(int i, token_t v);
void gen_val_const(token_t t);
void gen_defvar_lf(const char* name);