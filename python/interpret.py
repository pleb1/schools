#coding=utf-8
#interpret for a hypothetical language
import sys,getopt,re
import xml.etree.ElementTree as XML
from pathlib import Path


instrs = ["CREATEFRAME", "PUSHFRAME", "POPFRAME", "RETURN", "BREAK", "DEFVAR", "POPS", "MOVE", "INT2CHAR", "STRLEN", "TYPE", "NOT", "ADD", "SUB", "MUL", "IDIV", "LT", "GT", "EQ", "AND", "OR", "STRI2INT",  "CONCAT", "GETCHAR", "SETCHAR", "READ", "CALL", "LABEL", "JUMP", "JUMPIFEQ", "JUMPIFNEQ", "PUSHS", "WRITE", "DPRINT"]
labels = {}
GF = {"empty": "empty"} # global frame
LF = ['empty'] # local frame
TF = {"NULL": "NULL"} # temporary frame
stack = []
sourcefile=''
infile=''

def parseXML(xml):
    #print("xml")
    if xml.tag == 'program' and xml.attrib == {'language': 'IPPcode19'}:#check xml root and atribut root must be program
        for child in xml:
            #print("xml child")
            if child.tag == 'instruction':#children must be <instruction></instruction>
                opcode = child.get('opcode')
                opcode = opcode.upper()
                if opcode in instrs:# check if opcode is valid
                    if opcode == "LABEL" and child.get('order') is not None:#if opcode is label check if name of label exists
                        if child.find('arg1') is None:#empty label 
                            return False
                        if child.find('arg1').text in labels.keys():#label does not exist
                            exit(52)
                        labels[child.find('arg1').text] = child.get('order')
                else:#invalid opcode
                    #print("xml fale")
                    #print("xml error 32")
                    exit(32)
                for arg in child:#check child's params tag
                    if arg.tag not in ["arg1", "arg2", "arg3"]:
                        return False
                continue
            return False
        return True
    return False

def getVar(arg):# check if the var is in LF,GF OR TF
    global GF, LF, TF# 3 types of var
    #print(arg.text)
    if arg.text[0:2] == "LF":
        if 'empty' in LF:# var does not exist
            exit(55)
        if arg.text[3:] in LF[0]:#var exists return the var,
            return LF[0].get(arg.text[3:])

    if arg.text[0:2] == "TF":
        if "NULL" in TF:#var does not exist
            exit(55)
        if arg.text[3:] in TF:#var exists return the var
            return TF.get(arg.text[3:])
    if arg.text[0:2] == "GF":
        if "empty" in GF:#var does not exist
            exit(55)
        if arg.text[3:] in GF:#var exists return the var
            #print(arg.text[3:])
            #print(GF)
            return GF.get(arg.text[3:])
    exit(54)#var must be LF OR TF OR GF 


def getSymb(arg):#symbol can be var or literal
    if arg.get('type') == 'var':
        return getVar(arg)
    else:
        return arg.get('type') + '@' + isempty(arg.text)#literal




def setVar(name, value):#PUSH var to LF,GF OR TF
    global GF, LF, TF
    if name.text[0:2] == "GF":#first GF
        if 'empty' in GF:#init state
            del GF['empty']
            GF.update({name.text[3:]: value})
            return
        elif value == 'NULL':#Defvar new GF
            if 'empty' in GF:
                del GF['empty']#remove init state
            GF.update({name.text[3:]: value})# update to our value
            return
        elif name.text[3:] in GF:#GF alredy exist need to change value operation MOVE
            GF.update({name.text[3:]: value})
            return
    
    if name.text[0:2] == "LF":
        if 'empty' in LF:#nonexisting return error
            exit(55)
        if value == 'NULL':#defvar
            if 'empty' in LF[0]:
                del LF[0]['empty']
            LF[0].update({name.text[3:]: value})
            return
        elif 'empty' in LF:#lf not exist
            exit(55)
        elif name.text[3:] in LF[0]:#operation move
            LF[0].update({name.text[3:]: value})
            return

    if name.text[0:2] == "TF":#same like above
        if "NULL" in TF:
            exit(55)
        if value == 'NULL':
            TF.update({name.text[3:]: value})
            return
        elif 'empty' in TF:#not error at the beginning TF is not defined
            del TF['empty']
            TF.update({name.text[3:]: value})
            return
        elif name.text[3:] in TF:#replacing existed tf with new value
            TF.update({name.text[3:]: value})
            return

    exit(54)


def isempty(s):
    if s is not None:
        return s
    return ''


def parseParams(child, typelist):# check if param is valid param needs to match their type depending on the opcode
    i=0
    #value = {}
    for arg in child:
       # print(arg.tag)
       # print('arg'+str(i+1))
        if arg.tag != 'arg' + str(i+1):# we start from 0 and arg is from arg1 arg2 arg3,xml format 
            exit(31)
        if typelist[i] == 'symb':
            if arg.get('type') in ['bool', 'int', 'string']:
                if arg.get('type') == 'int':#checking if interger is valid
                    if not re.match("[-+]{0,1}[0-9]+", isempty(arg.text)):
                        #value.update({arg.get('type'): isempty(arg.text)})
                    #else:
                        exit(32)
                elif arg.get('type') == 'string':#checking if string is valid
                    if not re.match('([^\t\n \\\\]*(\\\\[0-9]{3})*)*', isempty(arg.text), 0):
                        #value.update({arg.get('type'): isempty(arg.text)})
                    #else:
                        exit(32)
                else:#checking if bool is valid
                    if not re.match("(true|false)", isempty(arg.text)):
                        #value.update({arg.get('type'): isempty(arg.text)})
                   # else:
                        exit(32)
            else:
                typelist[i] = 'var'

        if typelist[i] == 'var':#check if var is valid 
            if(arg.get('type') != 'var'):
                exit(32)
            if not re.match("(GF|LF|TF)@[a-zA-Z_\-$&%*]+[a-zA-Z_\-$&%*0-9]*", isempty(arg.text)):
                exit(32)

        elif typelist[i] == 'label':#check if label is valid
            if(arg.get('type') != 'label'):
                exit(32)
            if not re.match("[a-zA-Z_\-$&%*]+[a-zA-Z_\-$&%*0-9]*", isempty(arg.text)):
                exit(32)

        elif typelist[i] == 'type':#check if type is valid
            if(arg.get('type') != 'type'):
                exit(32)
            if not re.match("(bool|int|string)", isempty(arg.text)):
                exit(32)

        i += 1# next param
    return True


def appendToFrame(frame, value):
    if 'NULL' in frame:# frame is not defined error
        exit(55)
    if 'empty' in frame:#remove first empty slot
        del frame['empty']
    frame.update(value)#push the value
    return





def main():
    global TF, GF, LF, label, stack,infile,sourcefile
    labelList = [] 
    instrDone = 0 
    i = 0
    if len(sys.argv[1:]) >= 1: #check params 
        try:
            options, args = getopt.getopt(sys.argv[1:], None, ['source=','input=','help'])# all options
        except getopt.GetoptError as error:
            print(error)
            exit(10)
        for opt, arg in options:
            if opt == '--source':
                if Path(arg).exists():
                    sourcefile = arg
                else:#file not exist
                    exit(11)
            if opt == '--input':
                if Path(arg).exists():
                    infile = arg
                else:
                    exit(11)
            if opt == '--help':
                if(len(sys.argv[1:]) > 1):
                    exit(10)
                print("usage: python3.6 --source=file --input=file --help")
                exit(0)

    if(sourcefile == ''):# --source is missing 
        #print("im here")
        xml = XML.parse(sys.stdin).getroot() 
    #print(sourcefile)
    else:
        if not sourcefile:
            exit(11)
        xml = XML.parse(sourcefile).getroot()
    if not parseXML(xml):#checking xml format
        exit(31)

    
    while i < len(xml):#parsing xml values
        instrDone += 1#number of instructions done
        child = xml[i]
        instr = child.get('opcode')#instruction
        instr = instr.upper()#instruction case insensitive
        #print(instr)
        if int(xml[i].get('order')) != i+1:#checking order format our i is from 0 order from 1
            exit(31)
        if instr == "ADD":
            x = getSymb(child[1])#get first symbol
            y = getSymb(child[2])#get second symbol
            parseParams(child, ['var', 'symb', 'symb'])#check params type
            if x[0:3] != 'int' or y[0:3] != 'int':#wrong ops type
                exit(53)
            setVar(child[0], 'int@' + str(int(x[4:]) + int(y[4:])))#set the var value
        elif instr == "SUB":
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:3] != 'int' or y[0:3] != 'int':#wrong ops type
                exit(53)
            setVar(child[0], 'int@' + str(int(x[4:]) - int(y[4:])))
        elif instr == "IDIV":
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])#check params type
            if x[0:3] != 'int' or y[0:3] != 'int':#wrong ops type
                exit(53)
            if int(y[4:]) == 0:#deleni 0
                exit(57)
            setVar(child[0], 'int@' + str(int(x[4:]) // int(y[4:])))#set the var value
        elif instr == "MUL":
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])#check params type
            if x[0:3] != 'int' or y[0:3] != 'int':#wrong ops type
                exit(53)
            setVar(child[0], 'int@' + str(int(x[4:]) * int(y[4:])))#set the var value
        
        elif instr == "AND": # x == true and y == true = true else false, expecting both type = bool
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:4] == 'bool' and y[0:4] == 'bool':
                if x[5:] == 'true' and y[5:] == 'true':
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            else:
                exit(53)
        elif instr == "OR":# x is true or y is true, expecting both type = bool
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:4] == 'bool' and y[0:4] == 'bool':
                if x[5:] == 'true' or y[5:] == 'true':
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            else:
                exit(53)

        elif instr == "NOT":# true -> false , false -> true
            x = getSymb(child[1])
            parseParams(child, ['var', 'symb'])
            if x[0:4] != 'bool':
                exit(53)
            if x[5:] == 'true':
                setVar(child[0], 'bool@false')
            else:
                setVar(child[0], 'bool@true')
       
        elif instr == "LT":# x < y = true else false, same type of ops or error
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:3] == 'int' and y[0:3] == 'int':
                if int(x[4:]) < int(y[4:]):
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@flase')
            
            elif x[0:6] == 'string' and y[0:6] == 'string':
                if x < y:
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            
            elif x[0:4] == 'bool' and y[0:4] == 'bool':
                if x < y:
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            
            else:#invalid type
                exit(53)
        elif instr == "GT":# x > y = true else false, same type of ops or error
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:3] == 'int' and y[0:3] == 'int':
                if int(x[4:]) > int(y[4:]):
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')

            elif x[0:6] == 'string' and y[0:6] == 'string':
                if x > y:
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')

            elif x[0:4] == 'bool' and y[0:4] == 'bool':
                if x > y:
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            
            else:#invalid type
                exit(53)
        elif instr == "EQ":#x == y = true else false
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:3] == 'int' and y[0:3] == 'int':
                if int(x[4:]) == int(y[4:]):
                    setVar(child[0], 'bool@true')#set value to true
                else:
                    setVar(child[0], 'bool@false')#set value to false
            elif x[0:6] == 'string' and y[0:6] == 'string':
                if x == y:
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            elif x[0:4] == 'bool' and y[0:4] == 'bool':
                if x == y:
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')
            else:
                exit(53)

        elif instr == "CREATEFRAME":
            del TF
            TF = {"empty": "empty"}# init new frame

        elif instr == "PUSHFRAME":
            if "NULL" in TF:# tf doest not exist
                exit(55)
            if 'empty' in LF:# push tf to lf
                LF.remove('empty')
            LF.append(TF)
            del TF# remove pushed tf
            TF = {"NULL": "NULL"}
        elif instr == "POPFRAME":
            if 'empty' in LF:#nothing to pop
                exit(55)
            TF = LF.pop()
            if not LF:# last LF
                LF.append('empty')
        elif instr == "RETURN":
            i = labelList.pop(0)
            continue
        elif instr == "BREAK":#write to stderr instr, instr done + frame
            sys.stderr.write('Instruction: ' + str(i+1) + ' Instructions done: ' + str(instrDone) + '\nGF-LF-TF:')
            sys.stderr.write(str(GF))
            sys.stderr.write(str(LF))
            sys.stderr.write(str(TF))
        elif instr == "DEFVAR":
            #print("defvar")
            parseParams(child, ["var"])
            setVar(child[0], 'NULL')#init the var to empty

        elif instr == "PUSHS":
            #print("PUSHS")
            parseParams(child, ['symb'])
            stack.append(getSymb(child[0]))#push the symbol to the top of stack

        elif instr == "POPS":
            #print("pops")
            if not stack:# stack empty
                exit(56)
            parseParams(child, ["var"])#check if var is valid
            setVar(child[0], stack.pop())#pop 

        elif instr == "MOVE":
            x = getSymb(child[1])#value to move
            parseParams(child, ["var", "symb"])
            if 'NULL' not in x:
                setVar(child[0], getSymb(child[1]))
            else:#val does not exist
                exit(54)

        elif instr == "INT2CHAR":
            x = getSymb(child[1])
            parseParams(child, ['var', 'symb'])
            #print(x)
            #y = x[4:]
            #print(y)
            if 'NULL' in x:#value is null
                exit(54)
            if x[0:3] == 'int':
                #print("zde")
                try:
                    setVar(child[0], chr(int(x[4:])))
                except ValueError:#cant convert int to char
                    exit(58)
            else:#wrong type
                exit(53)
        elif instr == "STRI2INT":#(var,sym,sym)
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])#checkparams
            if x[0:6] != 'string' or y[0:3] != 'int':#wrong type
                exit(53)
            if int(y[4:]) > len(x[7:]):#number is bigger than len of string
                exit(58)
            setVar(child[0], 'int@' + str(ord(x[int(y[4:])+7: int(y[4:])+8])))#x = string thats why start from +7, y is the index(int) that's why start from 4, 7+index in y gives index in string 
        
        elif instr == "STRLEN":#(var,sym)
            x = getSymb(child[1])
            parseParams(child, ['var', 'symb'])
            if 'NULL' in x:#x does not exist
                exit(54)
            if x[0:6] == 'string':#return the str len string@ => x[7:]
                setVar(child[0], 'int@' + str(len(x[7:])))
            else:
                exit(53)
        elif instr == "GETCHAR":#(var,sym,sym)
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:6] != 'string' or y[0:3] != 'int':#sym must be int or string
                exit(53)
            if int(y[4:]) > len(x[7:]):#index is bigger than len
                exit(58)
            setVar(child[0], 'string@' + x[int(y[4:])+7: int(y[4:]) + 8])
            
        elif instr == "SETCHAR":#(var,sym,sym)
            x = getSymb(child[1])
            y = getSymb(child[2])
            z = getSymb(child[3])
            parseParams(child, ['var', 'symb', 'symb'])
            if y[0:3] != 'int' or z[0:6] != 'string':#sym must be int or string
                exit(53)
            if int(y[4:]) > len(x[7:]) or len(z[7:]) < 1:#index is less than 1 or > max len
                exit(58)
            setVar(child[0], 'string@' + x[7:int(y[4:])+7] + z[7:8] + x[int(y[4:])+8:])
        
        elif instr == "CONCAT":#(var,sym,sym)
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['var', 'symb', 'symb'])
            if x[0:6] == 'string' and y[0:6] == 'string':#must be strings
                setVar(child[0], x+y[7:])
            else:#wront type
                exit(53)

        elif instr == "TYPE":#(var,sym)
            x = getSymb(child[1])
            parseParams(child, ['var', 'symb'])
            if 'NULL' in x:#var is nul = empty string
                setVar(child[0], 'string@')
            else:
                setVar(child[0], 'string@' + x[0:x.find('@')])

        elif instr == "READ":#(var,type)
            if infile == '':
                x = input()
            else:
                f = open(infile,"r")
                if f.mode == 'r':
                    x = f.read()
                else:
                    exit(11)
            parseParams(child, ['var', 'type'])
            if child[1].text == 'int':#read int
                try:
                    setVar(child[0], 'int@' + str(int(x)))
                except ValueError:
                    setVar(child[0], 'int@0')#cant read

            elif child[1].text == 'string':#read string
                if re.match('([^\t\n \\\\]*(\\\\[0-9]{3})*)*', str(x)):#check if string is valid
                    setVar(child[0], 'string@' + str(x))
                else:
                    setVar(child[0], 'string@')#cant read empty string

            elif child[1].text == 'bool':#read bool 
                if x.lower() == 'true':
                    setVar(child[0], 'bool@true')
                else:
                    setVar(child[0], 'bool@false')

        elif instr == "CALL":#(label)
            parseParams(child, ['label'])
            labelList.append(i+1)#save next instruction for later(after return)
            i = int(labels[child[0].text])#set next instruction to the label
            continue
        elif instr == "LABEL":
            x=1
            #print(instr)
        elif instr == "JUMP":#(label)
            parseParams(child, ['label'])
            i = int(labels[child[0].text])#set next instruction to the label
            continue
        elif instr == "JUMPIFEQ":#(label,sym,sym)
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['label', 'symb', 'symb'])
            if x[0:3] == 'int' and y[0:3] == 'int':
                if x == y:
                    i = int(labels[child[0].text])#set next instruction to the label
                    continue
            if x[0:6] == 'string' and y[0:6] == 'string':
                if x == y:
                    i = int(labels[child[0].text])
                    continue
            if x[0:4] == 'bool' and y[0:4] == 'bool':
                if x == y:
                    i = int(labels[child[0].text])
                    continue
            else:#wrong types
                exit(53)

        elif instr == "JUMPIFNEQ":#(label,sym,sym)
            x = getSymb(child[1])
            y = getSymb(child[2])
            parseParams(child, ['label', 'symb', 'symb'])
            if x[0:3] == 'int' and y[0:3] == 'int':
                if x != y:
                    i = int(labels[child[0].text])#set next instruction to the label
                    continue
            if x[0:6] == 'string' and y[0:6] == 'string':
                if x != y:
                    i = int(labels[child[0].text])
                    continue
            if x[0:4] == 'bool' and y[0:4] == 'bool':
                if x != y:
                    i = int(labels[child[0].text])
                    continue
            else:
                exit(53)

        elif instr == "WRITE":#(sym)
            printstr = []
            str1 = '\\'
            x = getSymb(child[0])
            parseParams(child, 'symb')
            p = x.find('@')
            #print("write")
            #print(x[x.find('@')+1:],end='')
            y = x.find('\\')
            if y != -1: # redukce escape problem is hleadni \,kdy nefunguje kdyz bereme string od znaku @, proto nechavam string v puvodnimu stavu a vzdy ostradnim escape + pomocny list pro vypsani
                y = x.find('\\')
                #print(p) y je pozice \ p je pozice @ odstraneni escape pomoci(pripadne jiz string pred escape sekvenci) slicing
                #print(y)
                while y != -1:
                    if y == p+1:
                        pstr = x[y+1:y+4]
                        #print(pstr)
                        printstr.append(chr(int(pstr)))
                        x = x[0:p+1]+x[y+4:]
                        y = x.find('\\')
                    else:
                        pstr = x[y+1:y+4]
                        #print(pstr)
                        printstr.append(x[p+1:y])
                        printstr.append(chr(int(pstr)))
                        x = x[0:p+1]+x[y+4:]
                        y = x.find('\\')
                        #print(x)
                if len(x) != 7:
                    printstr.append(x[p+1:])
                for f in printstr:
                    print(f,end='')
            else:
                print(x[p+1:],end = '')
            #print(x)

        elif instr == "DPRINT":#(sym)
            x = getSymb(child[0])#get symbol
            parseParams(child, ['symb'])#check symbol
            sys.stderr.write(x[x.find('@')+1:])#write symbol value to stderr
            sys.stderr.write('\n')
        else:#cannot happen but w/e
            #print("invalid instruction")
            exit(32)
        i += 1


if __name__ == '__main__':
    main()
