import pandas as pd
import numpy as np
from unidecode import unidecode
from openpyxl import load_workbook
from matplotlib import pyplot as plt

def clean(): # clean all the useless sheets
    del_sheets = [
        'Úvod',
        'Poznatky',
        'Šablóna',
        'Šablona new',
        'Starší',
        'Členové',
        'Starsi',
        'Sheet34',
    ]
    workbook = load_workbook('raw.xlsx')
    for sheets in del_sheets:
        if sheets in workbook.sheetnames:
            workbook.remove(workbook[sheets])
    workbook.save(filename='raw.xlsx')

def get_committee(pd: pd.DataFrame, committee: list)-> pd.DataFrame: # get your committee
    return pd[pd['teacher'].isin(committee)]


def get_teacher(data: any)->str: # finding the teacher from details returns unknown if not found any returns teacher names
    seznam_teacher = ['barina','bartik','bartlik','beran','bidlo','burget','burget lukas','burget radek','burgetova','cadik',
     'cernocky','ceska','chudy','drabek','drahansky','dytrych','fuchs','fucik','fusek','gregr','grezl','hanacek','herout','hlinena',
     'holik','honzik','hradis','hruby','hruska','janousek','jaros','kekely','koci','kolar','korenek','korenekco','kotasek',
     'kovar','krena','kreslikova','krivka','krupkova','len','lengal','malinka','martinek','matousek','novak','ocenasek','orsag',
     'pepe','peringer','polcak','rogalewicz','rogalewitz','rozman','ruzicka','rychly','rysavy','satek','sekanina','smrcka',
     'smrz','spanel','strnadel','sveda','szoke','trchalik','vasicek','vesely','vojnar','zachariasova','zboril','zemcik', 'zendulka',
     ]
    teacher = ""
    if pd.isna(data):
        return "unkown"
    char_remove = '[]{}(),.-:' # removing all none desired characters in our details
    if(data != str):
        data = str(data)
    data = unidecode(data).lower()
    for x in char_remove:
        data = data.replace(x,'')
    data = data.split()
    for x in data:
        if '/' in x: # we got (potentialy) multiple teachers asking the question
            x = x.split('/')
            for g in x:
                if g in seznam_teacher:
                    teacher += f' {g}'
            if teacher != "":# found the teachers ending
                break
        else:
            if x in seznam_teacher:
                teacher += x
                break# found the teacher ending
    if teacher == "":
        teacher = "unknown"
    return teacher

def filter_bad_grade(grade: any) -> bool: # filters out invalid grades
    if pd.isna(grade):
        return False
    if grade == 'X':
        return False

    grades = ['A', 'B', 'C', 'D', 'E', 'F']
    grade = grade.strip()
    if grade not in grades:
        return False
    return True


def filter_bad_question(question: any) -> bool: # filter outs invalid questions
    valid_questions = list(range(0, 42))
    if question in valid_questions:
        return True
    return False

def get_bp_grade(data:str)->str:#gets bp grade
    grade = 'X'
    if pd.isna(data):
        return grade
    if '/' in data:
        if(filter_bad_grade(data.split('/')[0])):
            return data.split('/')[0]
        else:
            return grade
    return grade

def get_szz_grade(data:str)->str: # get szz grade
    grade = 'X'
    if pd.isna(data):
        return grade
    if '/' in data:
        if(filter_bad_grade(data.split('/')[1])):
            return data.split('/')[1]
        else:
            return grade
    return grade

def parse_raw():# parsing the raw excel file, adding useful infos to the excel file for next usage
    data = pd.read_excel('raw.xlsx')
    cols = [x for x in data.columns if "Unnamed" not in x]
    datapom = data[cols]
    datapom.columns = ['name','question','details','grade','year']
    datapom
    datapom['bp_grade'] = datapom.grade.map(get_bp_grade)
    datapom['szz_grade'] = datapom.grade.map(get_szz_grade)
    datapom['teacher'] = datapom['details'].map(get_teacher)
    datapom.to_excel('cleaned.xlsx')

def plot_questions(data: pd.DataFrame): # plot questions for your committee
    data = data.dropna()
    question = data.question
    bins = [i for i in range(1,42)]
    plt.hist(question,bins = bins)
    #plt.xticks(bins)
    plt.xlabel('Otazky')
    plt.ylabel('Pocet otazek')
    plt.title('cetnost padnutych otazek')
    #figure(figsize=(8, 6), dpi=800)
    plt.savefig('question.png',dpi = 300)
    plt.show()

def plot_succes_ratio(data: pd.DataFrame): # plot success ratio for your commmittee
    data = data.dropna()
    grades = data.groupby('question')['szz_grade'].sum()
    question = data['question'].tolist()
    question = list(set(question))
    ratio = {x:y for x,y in zip(question,grades)}
    pass_ratio = {}
    for x,y in ratio.items():
        cnt = rat = pom = 0
        for c in y:
            #print(c)
            if c == 'F':# count fails
                cnt += 1
            rat += 1
        pom = rat - cnt
        pass_ratio[int(x)] = int(pom / rat * 100)
    x_axis_red = []
    y_axis_red = []
    x_axis_yellow = []
    y_axis_yellow = []
    x_axis_green = []
    y_axis_green = []
    for x,y in pass_ratio.items():
        if y < 30:
            x_axis_red.append(x)
            y_axis_red.append(y)
        elif y > 30 and y < 70:
            x_axis_yellow.append(x)
            y_axis_yellow.append(y)
        else:  
            x_axis_green.append(x)
            y_axis_green.append(y)
    #x_axis = np.array(x_axis)
    #y_axis = np.array(y_axis)
    #mask1 = y_axis < 30
    #mask2 = y_axis > 30 & y_axis < 70
    #mask3 = y_axis > 70
    plt.bar(x_axis_red,y_axis_red, color = 'red')
    plt.bar(x_axis_yellow,y_axis_yellow, color = 'yellow')
    plt.bar(x_axis_green,y_axis_green, color = 'green')
    plt.title('question pass ratio')
    plt.ylabel('percentage')
    plt.xlabel('questions')
    plt.savefig('question_pass_ratio.png',dpi = 300)
    plt.show()

#clean()
#parse_raw()
data = pd.read_excel('cleaned.xlsx')
committee = ['vasicek', 'vesely', 'bidlo', 'rysavy', 'strnadel']
committee_only = get_committee(data,committee)
committee_only
g = [[x,y] for (x,y) in zip(committee_only.question,committee_only.details)]
g
with open('mycommitee.txt','w', encoding = 'utf-8') as fd: # filters out all questions for your committee
    for x in g:
        for y in x:
            fd.write(f' {str(y)}')
        fd.write('\n')
plot_questions(committee_only)
plot_succes_ratio(committee_only)

