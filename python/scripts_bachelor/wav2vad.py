#!/usr/bin/env python
import numpy as np
import sys, os
import errno
sys.path.append('/homes/kazi/iplchot/pytel')
import pytel.features as features
#import pytel.htk as htk
import pytel.utils as utils
import pytel.gmm as gmm
import scipy.io.wavfile
import StringIO
import gzip

#import pytel.kaldi_io
from scipy.special import logsumexp

seg_list    = sys.argv[1]
out_vad_dir = sys.argv[2]
in_wav_dir  = sys.argv[3]


def compute_vad(s, win_length=200, win_overlap=120, n_realignment=5, threshold=0.3):
    # power signal for energy computation
    s = s**2
    # frame signal with overlap
    F = features.framing(s, win_length, win_length - win_overlap) 
    # sum frames to get energy
    E = F.sum(axis=1).astype(np.float64)
    # E = np.sqrt(E)
    # E = np.log(E)

    # normalize the energy
    E -= E.mean()
    try:
        E /= E.std()
    # initialization
        mm = np.array((-1.00, 0.00, 1.00))[:, np.newaxis]
        ee = np.array(( 1.00, 1.00, 1.00))[:, np.newaxis]
        ww = np.array(( 0.33, 0.33, 0.33))

        GMM = gmm.gmm_eval_prep(ww, mm, ee)

        E = E[:,np.newaxis]

        for i in xrange(n_realignment):
        # collect GMM statistics
            llh, N, F, S = gmm.gmm_eval(E, GMM, return_accums=2)

        # update model
            ww, mm, ee   = gmm.gmm_update(N, F, S)
        # wrap model
            GMM = gmm.gmm_eval_prep(ww, mm, ee)

    # evaluate the gmm llhs
        llhs = gmm.gmm_llhs(E, GMM)

        llh  = logsumexp(llhs, axis=1)[:,np.newaxis]

        llhs = np.exp(llhs - llh)

        out  = np.zeros(llhs.shape[0], dtype=np.bool)
        out[llhs[:,0] < threshold] = True
    except RuntimeWarning:
        logging.info("File contains only silence")
        out=np.zeros(E.shape[0],dtype=np.bool)
    return out

def median_filter(vad, win_length=50, threshold=25):
  stats = np.r_[vad, np.zeros(win_length)].cumsum()-np.r_[np.zeros(win_length), vad].cumsum()
  return stats[win_length/2:win_length/2-win_length] > threshold
  
def frame_labels2start_ends(speech_frames, frame_rate=100.):
  decesions = np.r_[False, speech_frames, False]
  return np.nonzero(decesions[1:] != decesions[:-1])[0].reshape(-1,2) / frame_rate



seg_list = np.atleast_1d(np.loadtxt(seg_list, dtype=object))
utils.mkdir_p(out_vad_dir)
for ii, fn in enumerate(seg_list):
    print ii, '/', len(seg_list), fn
    temp_file = os.path.join(out_vad_dir, fn + '.lab_part.gz')
    vad_file = os.path.join(out_vad_dir, fn + '.lab.gz')
    if (os.path.isfile(vad_file)):
      continue      
    samplerate, sig = scipy.io.wavfile.read(os.path.join(in_wav_dir, fn + '.wav'))
    assert(samplerate == 16000)
    sig=np.r_[np.zeros(8000), sig, np.zeros(8000)] # add half second of "silense" at the beginning and the end
    sig = features.add_dither(sig, 8.0)
    vad = compute_vad(sig, win_length=400, win_overlap=240, n_realignment=5, threshold=0.5)
    vad = median_filter(vad, 50, 5)
    frame_labels2start_ends(vad[50:-50])
    labels = (frame_labels2start_ends(vad[50:-50], 1)*100000).astype(int)

    directory=os.path.dirname(temp_file)
    try:
            os.makedirs(directory)
    except OSError as exception:
            if exception.errno != errno.EEXIST:
                    raise
    print '   saving data to:', vad_file
    #s = StringIO.StringIO()
    f=gzip.GzipFile(temp_file, 'w')
    np.savetxt(f, np.array(labels, dtype=object), fmt='%d %d sp')
    f.close()
    #with gzip.open(temp_file, 'wt') as f:
    #    f.write(s)    

    os.rename(temp_file, vad_file)
    
