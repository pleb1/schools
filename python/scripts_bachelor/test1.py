# usage python test1.py syn_audio_list org_audio_list enrollment_list s|m
# generating testing set from the original audio and synthesized audio, use s for test cosisting of a single test directory. use m only when there are 
# more testing directory that has format test0/ test1/ test2/ ....

import sys
import os
syn = sys.argv[1] #synthesized audio list
org = sys.argv[2] # org audio list
enroll = sys.argv[3] # enrollment list
flag = sys.argv[4] # signle test or multiple test | s | m

def mul_org(opath,out,out1):#use this if there is multiple org files
    for x in opath: # generating orig test set
        x = x.rstrip()
        pom = x.split('/') 
        a = pom[len(pom)-2]
        a = ord(a[len(a)-1]) - 48
        #print(a)
        pom1 = pom[len(pom)-1] # getting the speaker id
        pom1 = pom1[0:pom1.find('-')]
        pom2 = enpath[dch[pom1]-a]# search the speaker in the enrollment segment
        pom3 = pom2.split('/')
        #print(pom1)
        #print( pom3[len(pom3)-3])
        if pom1 == pom3[len(pom3)-3]: # we already know they will always match, but incase if i fucked up something, can skip the if
            out.write(pom2.rstrip()+" "+x.rstrip()+" "+'tgt'+"\n")
            out1.write(pom2.rstrip()+" "+x.rstrip()+" "+'tgt'+"\n") #need some org for the spoofing test too, so the system dont fail

def mul_test(spath,out1): #use this if there is multiple test files
    for x in spath: # generating spoofed test set
        x = x.rstrip()
        pom = x.split('/')
        a = pom[len(pom)-3]
        a = ord(a[4]) - 48
        #print(a)
        pom1 = enpath[dch[pom[len(pom)-1]]- a] # finding the id in the matrix
        pom2 = pom1.split('/')
            #print(pom1)
            #print(pom[len(pom)-1])
            #print( pom2[len(pom2)-3])
        if pom[len(pom)-1] == pom2[len(pom2)-3]: # we already know they will always match,but incase if i fucked up something, can skip the if
            out1.write(pom1.rstrip()+" "+x.rstrip()+" "+'imp'+"\n")
def sing_test(spath,out1):
    for x in spath:
        x = x.rstrip()
        pom = x.split('/')
        #print(pom[len(pom)-1])
        #print(dch[pom[len(pom)-1]])
        #print("hello")
        pom1 = enpath[dch[pom[len(pom)-1]]]
        pom2 = pom1.split('/')
        if pom[len(pom)-1] == pom2[len(pom2)-3]: # we already know they will always match,but incase if i fucked up something, can skip the if
            out1.write(pom1.rstrip()+" "+x.rstrip()+" "+'imp'+"\n")

def sing_org(opath,out,out1):
    for x in opath: # generating orig test set
        pom = x.split('/') 
        pom1 = pom[len(pom)-1]
        #print(pom1)
        pom1 = pom1[0:pom1.find('-')]
        pom2 = enpath[dch[pom1]]
        pom3 = pom2.split('/')
        #print(pom1)
        #print( pom3[len(pom3)-3])
        if pom1 == pom3[len(pom3)-3]: # we already know they will always match, but incase if i fucked up something, can skip the if
            out.write(pom2.rstrip()+" "+x.rstrip()+" "+'tgt'+"\n")
            out1.write(pom2.rstrip()+" "+x.rstrip()+" "+'tgt'+"\n") #need some org for the spoofing test too, so the system dont fail

enpath = []
spath = []
opath = []
dch = {}
with open(enroll,'r') as eng:
    for e in eng:
        path = e.split('=')
        enpath.append(path[0])
with open(syn,'r') as syg:
    for s in syg:
        path = s.split('=')
        spath.append(path[0]) 
with open(org,'r') as ors:
    for o in ors:
        path = o.split('=')
        opath.append(path[0])

old = " "
cnt = 0
for x in enpath:
    pom = x.split('/')
    #print(pom[len(pom)-3])
    if old != pom[len(pom)-3]:
        dch[pom[len(pom)-3]] = cnt
    cnt += 1
with open('test_synthesis.txt','w') as out1, open('org_test_synthesis.txt','w') as out:
    if flag == "s":
        sing_test(spath,out1)
        sing_org(opath,out,out1)
    else:
        mul_test(spath,out1)
        mul_org(opath,out,out1)

with open('org_test_synthesis.txt','w') as out:
    for i in range(0,len(enpath)): # generates about 4*enrolls
        en = enpath[i]
        pom1 = en.split('/')
        pom1 = pom1[len(pom1)-3]
        #print(pom1)
        for x in range(0,4):
            pom = opath[i % len(opath)]
            p1 = pom.split('/')
            p1 = p1[len(p1)-1]
            p1 = p1[0:p1.find('-')]
            #print(p1)
            if p1 != pom1:
                out.write(en.rstrip()+" "+pom.rstrip()+" "+'imp'+"\n")
            #print(p1)
        #print(en)
