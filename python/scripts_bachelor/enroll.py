import sys 
import os

input = sys.argv[1]# path to the x-vector files(where is vad of the enrollment, original, test files stored)
test = sys.argv[2]# name of the enrollment files dir
org = sys.argv[3]# name of to the original files dir
enroll = sys.argv[4]# name of to the enrollment files dir
#input = "/mnt/matylda5/iplchot/synthesis/extraction/xvectors_large_net_40-fb_train40/output"
cwd = os.getcwd()
os.chdir(input)
findorg = "find {0} -name *.i.gz | sed 's/\.i.gz$//' >>{1}/torg.scp".format(org,cwd)
findtest = "find {0} -name *.i.gz | sed 's/\.i.gz$//' >> {1}/ttest.scp".format(test,cwd)
findenroll = "find {0} -name *.i.gz | sed 's/\.i.gz$//' >> {1}/tenroll.scp".format(enroll,cwd)
#print(findorg)
os.system(findorg)
os.system(findtest)
os.system(findenroll)
os.chdir(cwd)

out2 = open('test_synthesis.test.scp','w')
#print(a)
with  open('torg.scp','r') as to, open('ttest.scp','r') as ts, open('tenroll.scp','r') as te, open('org.scp','w') as o, open('test.scp','w') as s, open('enroll.scp','w') as e, open('o_test.scp','w') as oout, open('s_test.scp','w') as sout, open('e_test.scp','w') as eout, open('test_synthesis.test.scp','w') as out2:
    for line in to:
        o.write(line)
        #print(line)
        line = line.rstrip()
        #line = line[1:]
        #print(line)
        oout.write(line+'='+line+'\n')
        out2.write(line+'='+line+'\n')
    for line in ts:
        s.write(line)
        line = line.rstrip()
        #print(line)
        #line = line[1:]
        sout.write(line+'='+line+'\n')
        out2.write(line+'='+line+'\n')
    for line in te:
        e.write(line)
        line = line.rstrip()
        #line = line[1:]
        eout.write(line+'='+line+'\n')
os.system("rm torg.scp")
os.system("rm ttest.scp")
os.system("rm tenroll.scp")


