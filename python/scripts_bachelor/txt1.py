#false alarm  falsely accepted trials / total of nontarget trials
#missed detections falsely rejected trials / number of target trials
#usage python txt1.py txtflie scorefile outfile s|o - s for synthesized audio and o for original audio
import sys
import os

def org_cal(tar,ntar,tgt,ntgt,out,eer_th):
    fal = 0
    for line in tgt:
        pom = line.split()
        a = pom[0].split('/')
        b = pom[1].split('/')
        a = a[len(a)-1]
        #print(a)
        b = b[len(b)-1]
        b = b[0:b.find('-')]
        a = a[0:a.find('-')]
        #print(b)
        if a != b:
            fal += 1

    #CALCULATING MISS for org data
    miss = 0
    for line in ntgt:
        pom = line.split()
        a = pom[0].split('/')
        b = pom[1].split('/')
        a = a[len(a)-1]
        #print(a)
        b = b[len(b)-1]
        b = b[0:b.find('-')]
        a = a[0:a.find('-')]
        #print(b)
        if a == b:
            miss += 1
    misss = miss / tar * 100
    fals = fal / ntar * 100
    out.write("================== NO_KALDI ================ \n")
    out.write("EER_th:" + " "+str(eer_th) + " "+ "MISS:" +" " +str(misss) + " "+ "FALSE:" +" " +str(fals)+'\n')
    out.write("TGT:" + " "+str(tar)+'\n')
    out.write("NTGT:" + " "+str(ntar)+'\n')
def syn_cal(tar,ntar,tgt,ntgt,out,eer_th):
    fal = 0
    for line in tgt:
        if "or" in line:
            continue
        else:
            #print(line)
            pom = line.split()
            a = pom[0].split('/')
            b = pom[1].split('/')
            a = a[len(a)-1]
            a = a[0:a.find('-')]
            b = b[len(b)-1]
            if a == b:
                fal += 1
    #print(fal)
    miss = 0
    for line in ntgt:
        if "or" in line:
            #print(line)
            miss += 1
    misss = miss / tar * 100
    fals = fal / tar * 100 
    out.write("================== NO_KALDI ================ \n")
    out.write("EER_th:" + " "+str(eer_th) + " "+ "MISS:" +" " +str(misss) + " "+ "FALSE:" +" " +str(fals)+'\n')
    out.write("TGT:" + " "+str(tar)+'\n')
    out.write("NTGT:" + " "+str(ntar)+'\n')        

txt = sys.argv[1] #txt file
score = sys.argv[2] #score file
output = sys.argv[3] # output file
flag = sys.argv[4] #flag


out = open(output,'w')
fdp = []

max = 0
min = 0

tgt = []
ntgt = []
with open(txt,'r') as fd:
    for line in fd:
        fdp.append(line)

a = fdp[17]
a = a[a.find('\'EER_th\'')+1:]
a = a[:a.find(',')]
pom = a.split()
eer_th = float(pom[1])
#print(eer_th)

a = fdp[17]
a = a[a.find('\'tar\'')+1:]
a = a[:a.find(',')]
#print(a)
pom = a.split()
tar = pom[1]
#print(tar)

a = fdp[17]
a = a[a.find('\'nontar\'')+1:]
a = a[:a.find(',')]
#print(a)
pom = a.split()
ntar = pom[1]
with open(score,'r') as fs:
    for line in fs:
        #print(line)
        pom = line.split()
        n = float(pom[2])
        #print(n)
        if n >= eer_th:
            tgt.append(line)
        else:
            ntgt.append(line)
tar = float(tar)
ntar = float(ntar)
with open(output,'w') as out:
    if flag == "s":
        syn_cal(tar,ntar,tgt,ntgt,out,eer_th)
    else:
        org_cal(tar,ntar,tgt,ntgt,out,eer_th)

