#function to get all audio and texts to the audio in the librispeech datasets
#returns a dictionary with a list of tuples that contain path and texts of the audio for each of the folders in the datasets
import os 
import sys
def getsyndic(dic)->dict:
    #dic = sys.argv[1]
    syndic = {}
    listtxt = []
    flac = []
    for root, dirs, files in os.walk(dic):
        #print(root)
        #print(dirs)
        #print(files)
        for file in files:
            #print(file)
            if file.endswith('.txt'):
                #print(os.path.join(root, file))
                listtxt.append(os.path.join(root,file))
            if file.endswith('.flac'):
                flac.append(os.path.join(root,file))
    for x in listtxt:
        g = x.split('\\')[-1].split('.')
        #print(dat[::len(dat)])
        #g = dat[-1].split('.')
        #print(g[0])
        path = x.rsplit('\\', 1)[0] # reverse split() rsplit('\\', 1) - split by \, return a list of two of elements, split just once 
        #print(path1)
        with open(x,'r') as fd:
            pom = []
            for s in fd:
                data = s.split()
                #print(data[0])
                data1  = data[1::]
                txts = " ".join(data1)
                path1 = os.path.join(path,"{}.flac".format(data[0]))
                #print(path1)
                tup = (path1,txts)
                #syndic[data[0]] = txts
                pom.append(tup)
        syndic[g[0]] = pom
    #for x, y in syndic.items():
        #print(x)
        #print(y)
    return syndic